<?php

namespace App\Http\Controllers;

use App\ProductSubCategory;

use Illuminate\Http\Request;

use App\Http\Requests\CreateProductRange;

use App\ProductRange;

use Toastr;

use App\Product;

class ProductRangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$product_range = ProductRange::all();
        return view('cms.productrange.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function productRangeData (Request $request){

        $product_range = ProductRange::allProductRange($request);
        return $product_range;
    }


    public function create()
    {
        //$categories = [''=>''] + Category::lists('name', 'id')->all();
        $sub_category = ProductSubCategory::orderBy('sub_cat_name','asc')->pluck('sub_cat_name', 'id')->all();

        return view('cms.productrange.create')->with(compact('sub_category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRange $request)
    {
        $product_range = ProductRange::storeProductRange($request);

        Toastr::success('Product range successfully created!', 'Success', ["positionClass" => "toast-top-right"]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_range = ProductRange::find($id);
        $sub_category = ProductSubCategory::orderBy('sub_cat_name','asc')->pluck('sub_cat_name', 'id')->all();

        return view('cms.productrange.edit')->with(compact('product_range','sub_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateProductRange $request, $id)
    {
        $product_range = ProductRange::updateProductRange($request,$id);

        Toastr::success('Product range successfully updated!', 'Success', ["positionClass" => "toast-top-right"]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productRange = ProductRange::find($id);

        $productRange->forceDelete();

        $product = Product::where('product_range_id','=',$productRange->id);

        $product->delete();

        Toastr::error('Product Range successfully deleted!', 'Deleted', ["positionClass" => "toast-top-right"]);

        return redirect()->back();
    }
}
