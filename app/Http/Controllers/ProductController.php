<?php

namespace App\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;

use Illuminate\Http\Request;

use App\Product;

use App\ProductRange;

use App\Http\Requests\CreateProductRequest;

use Illuminate\Support\Facades\File;

use Illuminate\Support\Facades\URL;

use Illuminate\Support\Facades\Storage;

use Illuminate\Filesystem\Filesystem;

use PDF;
use Dompdf\Dompdf;



class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cms.products.index');
    }

    public function productData (Request $request){

        $product = Product::allProducts($request);
        return $product;
    }

    public function productInfoPdf(Request $request){

        ini_set('memory_limit', '-1');

        $product = Product::find($request->get('product_id'));

        $pdf = new Dompdf();
        $pdf->set_option("isPhpEnabled", true);
        $pdf->set_option('enable-smart-shrinking', true);
//        $pdf->set_option(true);

        $html = view('cms.products.product-info-pdf', ['product' => $product])->render();
        $pdf->loadHtml($html);


        /* Render the HTML as PDF */
        $pdf->render();

        /* Output the generated PDF to Browser */
        //$dompdf->stream();
        return $pdf->stream($product->product_name.'-info.pdf', array("Attachment" => 0));

        //return view('member.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$product_range = ProductRange::orderBy('product_range_name','asc')->pluck('product_range_name', 'id')->all();

        $product_range = ProductRange::all();

        return view('cms.products.create')->with(compact('product_range'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    {
        $product = Product::storeProduct($request);

        Toastr::success('Product successfully created!', 'Success', ["positionClass" => "toast-top-right"]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $product_range = ProductRange::all();

        return view('cms.products.edit')->with(compact('product','product_range'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateProductRequest $request, $id)
    {
        $product_range = Product::updateProduct($request,$id);

        Toastr::success('Product successfully updated!', 'Success', ["positionClass" => "toast-top-right"]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        $pdf_old = $product->sds;
        $product_img_old = $product->product_img;
        File::delete(public_path('uploads/product-sds/'.$pdf_old));
        File::delete(public_path('uploads/product-image/'.$product_img_old));

        foreach (glob(public_path('uploads/product-img-details/'.$product->id.'_*')) as $filename) {
            File::delete($filename);
        }

        $product->forceDelete();

        Toastr::error('Product successfully deleted!', 'Deleted', ["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }

    public function deleteImgDetails(Request $request){

        $img_file = $request->src;

        $file_name = str_replace(URL::to('/'), '', $img_file);

        //$p=parse_url($img_file);

       // dd($file_name);

        File::delete(public_path($file_name));

        return "succesfully deleted";

    }
}
