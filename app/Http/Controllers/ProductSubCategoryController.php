<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ProductSubCategory;

use App\ProductCategory;

use App\ProductRange;

use App\Product;

use App\Http\Requests\CreateProductSubCategoryRequest;

use Toastr;

class ProductSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $product_sub_category = ProductSubCategory::getAllSubCategoryWithProductCategory()
            ->paginate(10);

        return view('cms.subcategories.index')->with(compact('product_sub_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_categories = ProductCategory::getAllProductCategory()
            ->orderBy('category_name','asc')
            ->get();

        return view('cms.subcategories.create')->with(compact('product_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductSubCategoryRequest $request)
    {
        $product_sub_category = ProductSubCategory::storeSubProductCategory($request);

        Toastr::success('Sub category successfully created!', 'Success', ["positionClass" => "toast-top-right"]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_sub_category = ProductSubCategory::find($id);

        $product_categories = ProductCategory::getAllProductCategory()
            ->orderBy('category_name','asc')
            ->get();


        return view('cms.subcategories.edit')->with(compact('product_sub_category','product_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateProductSubCategoryRequest $request, $id)
    {
        $product_sub_category = ProductSubCategory::updateProductCategory($request,$id);

        Toastr::success('Sub-category successfully updated!', 'Success', ["positionClass" => "toast-top-right"]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategory = ProductSubCategory::find($id);

        $subcategory->delete();

        $productRange = ProductRange::where('sub_product_category_id','=',$subcategory->id)->get();
        $productRangeDelete = ProductRange::where('sub_product_category_id','=',$subcategory->id);
        $productRangeDelete->delete();

        foreach($productRange as $pR) {

            $product = Product::where('product_range_id','=',$pR->id);
            $product->delete();
        }

        Toastr::error('Sub Product successfully deleted!', 'Deleted', ["positionClass" => "toast-top-right"]);

        return redirect()->back();
    }
}
