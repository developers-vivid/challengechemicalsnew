<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('guest')->except(['destroy']);
    }


    public function destroy()
    {
        auth()->logout();

        return redirect('/admin/login');
    }
}
