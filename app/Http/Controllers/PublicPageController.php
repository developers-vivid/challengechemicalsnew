<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
use App\ProductSubCategory;
use App\ProductRange;
use App\Product;

class PublicPageController extends Controller
{
    public function index()
    {


        //$product_categories = ProductCategory::get();
        $hospitality = ProductCategory::findOrFail('5b3eed45edd03');
        $automotive = ProductCategory::findOrFail('5b46e60724f9e');
        $toll = ProductCategory::findOrFail('5b46e623f0cf8');

        $water = ProductCategory::findOrFail('5b46e64fbf788');
        $water_sub = ProductSubCategory::findOrFail('5b9f0bc083b76');
        $water_range = ProductRange::findOrFail('5b9f0bd577e58');

        $food = ProductCategory::findOrFail('5b46e66e4a3b6');
       // $gov = ProductCategory::findOrFail('5b46e6d001e15');
        $health = ProductCategory::findOrFail('5b46e700d68ea');

       return view('welcome')->with(compact('hospitality','automotive','toll','water','water_sub','water_range','food','health'));
    }

    public function SubCatList($slug){

        $category = ProductCategory::where('category_slug','=',$slug)->first();

        if ($category === null) {
            abort('404');
        }

        if($category->id=='5b46e700d68ea'){//health care

            //hospitality id category
            //housekeeping id   subcategory
            //product range

            $hospitality = ProductCategory::find('5b3eed45edd03');
            $housekeeping = ProductSubCategory::find('5b46f7cfc5d0f');
            $foodandbeverage = ProductSubCategory::find('5b46f7b8d3c5f');
            $opl = ProductSubCategory::find('5b46f7ec357c1');
            $handBody = ProductSubCategory::find('5b46f80644fee');
            $handBodyRange = ProductRange::find('5b860b4a47630');


            return view('pages.health-care-sub-cat-page')->with(compact('hospitality','housekeeping','foodandbeverage','opl','handBody','handBodyRange'));
        }else{


        $subcategory = ProductSubCategory::where('product_category_id', '=', $category->id)->orderBy('created_at','ASC')
            ->get();

        foreach ($subcategory as $sb){

            if($sb->id=='5b46f80644fee'){//HAND AND BODY

                $productRange = ProductRange::Where('sub_product_category_id','=','5b46f80644fee')->first();
                if($productRange){
                    $productRange_slug_hand = $productRange->product_range_slug;
                }
            }elseif($sb->id=='5b46f82dd51e0'){//CARPET, FLOOR AND GRAFFITTI

                $productRange = ProductRange::Where('sub_product_category_id','=','5b46f82dd51e0')->first();
                if($productRange){
                    $productRange_slug_carpet = $productRange->product_range_slug;

                }

            }elseif($sb->id=='5b46f83f6bddc'){//ONE SHOT

                $productRange = ProductRange::Where('sub_product_category_id','=','5b46f83f6bddc')->first();
                if($productRange){
                    $productRange_slug_one = $productRange->product_range_slug;
                }
            }elseif($sb->id=='5b9f5997c5807'){//Industrial Odour Control
                $productRange = ProductRange::Where('sub_product_category_id','=','5b9f5997c5807')->first();
                if($productRange){
                    $productRange_slug_odor = $productRange->product_range_slug;
                }
            }elseif($sb->id=='5b9f59b4ec617'){//Fleet & Truck Wash Detergents
                $productRange = ProductRange::Where('sub_product_category_id','=','5b9f59b4ec617')->first();
                if($productRange){
                    $productRange_slug_fleet = $productRange->product_range_slug;
                }
            }elseif($sb->id=='5b9f59a542fc7'){//Automatic Car Wash Products
                $productRange = ProductRange::Where('sub_product_category_id','=','5b9f59a542fc7')->first();
                if($productRange){
                    $productRange_slug_carwash = $productRange->product_range_slug;
                }
            }elseif($sb->id=='5b9f59c29d493'){//Car Detailing Products
                $productRange = ProductRange::Where('sub_product_category_id','=','5b9f59c29d493')->first();
                if($productRange){
                    $productRange_slug_detailing = $productRange->product_range_slug;
                }
            }

        }
            return view('pages.showCategory')->with(compact('category','subcategory','productRange_slug_one','productRange_slug_carpet','productRange_slug_hand','productRange_slug_odor','productRange_slug_fleet','productRange_slug_carwash','productRange_slug_detailing'));
        }
    }

    public function landingPage($slug){
        $category = ProductCategory::where('category_slug','=',$slug)->first();

        if ($category === null) {
            abort('404');
        }else{

            if($category->id=='5b46e60724f9e'){//Automotive mining and industrial

                $automotive = ProductCategory::findOrFail('5b46e60724f9e');
                $subcategory = ProductSubCategory::where('product_category_id', '=', $category->id)->get();

                return view('pages.industrial-mining-landing-page')->with(compact('automotive','subcategory'));
            }

            if($category->id=='5b46e623f0cf8'){//toll

                return view('pages.toll-bending-landing-page');
            }
            if($category->id=='5b46e66e4a3b6'){//food processing
                $food = ProductCategory::findOrFail('5b46e66e4a3b6');
                $subcategory = ProductSubCategory::findOrFail('5b960e31ec0d4');//food range
                return view('pages.food-processing-landing-page')->with(compact('food','subcategory'));

            }if($category->id=='5b46e700d68ea'){//healthcare
                $health = ProductCategory::findOrFail('5b46e700d68ea');
                return view('pages.health-care-landing-page')->with(compact('health'));
            }
        }
//        $subcategory = ProductSubCategory::where('product_category_id', '=', $category->id)->get();
//
//        foreach ($subcategory as $sb){
//
//            if($sb->id=='5b46f80644fee'){//HAND AND BODY
//
//                $productRange = ProductRange::Where('sub_product_category_id','=','5b46f80644fee')->first();
//                if($productRange){
//                    $productRange_slug_hand = $productRange->product_range_slug;
//                }
//            }elseif($sb->id=='5b46f82dd51e0'){//CARPET, FLOOR AND GRAFFITTI
//
//                $productRange = ProductRange::Where('sub_product_category_id','=','5b46f82dd51e0')->first();
//                if($productRange){
//                    $productRange_slug_carpet = $productRange->product_range_slug;
//
//                }
//
//            }elseif($sb->id=='5b46f83f6bddc'){//ONE SHOT
//
//                $productRange = ProductRange::Where('sub_product_category_id','=','5b46f83f6bddc')->first();
//                if($productRange){
//                    $productRange_slug_one = $productRange->product_range_slug;
//                }
//            }
//
//        }
//        return view('pages.showCategory')->with(compact('category','subcategory','productRange_slug_one','productRange_slug_carpet','productRange_slug_hand'));

    }


    public function showSubCat($cat_slug,$subcat_slug){

        $category = ProductCategory::where('category_slug','=',$cat_slug)->first();

        if ($category === null) {
            abort('404');
        }

        $subcategory = ProductSubCategory::where('sub_product_slug', '=', $subcat_slug)
                                ->Where('product_category_id','=',$category->id)->first();

        if ($subcategory === null) {
            abort('404');
        }

        if($subcategory->id=='5b46f7b8d3c5f') { //fnb id

            return view('pages.foodandbeverages')->with(compact('subcat_slug','cat_slug'));

        }elseif($subcategory->id=='5b46f7cfc5d0f'){
            return view('pages.housekeeping')->with(compact('subcat_slug','cat_slug'));
        }elseif($subcategory->id=='5b46f7ec357c1'){
            return view('pages.opl-laundry')->with(compact('subcat_slug','cat_slug'));
        }else{
            abort('404');
        }

    }

    public function totalCover($cat_slug,$subcat_slug){

        $category = ProductCategory::where('category_slug','=',$cat_slug)->first();


        if ($category === null) {
            abort('404');
        }

        $subcategory = ProductSubCategory::where('sub_product_slug', '=', $subcat_slug)
            ->Where('product_category_id','=',$category->id)->first();

        if ($subcategory === null) {
            abort('404');
        }

        if($subcategory->id=='5b46f7b8d3c5f') { //fnb id

            return view('pages.total-cover')->with(compact('subcat_slug','cat_slug'));
        }else{
            abort('404');
        }

    }


    public function showEquipment($cat_slug,$subcat_slug){

        $category = ProductCategory::where('category_slug','=',$cat_slug)->first();

        if ($category === null) {
            abort('404');
        }

        $subcategory = ProductSubCategory::where('sub_product_slug', '=', $subcat_slug)
            ->Where('product_category_id','=',$category->id)->first();

        if ($subcategory === null) {
            abort('404');
        }


        if($subcategory->id == '5b46f7b8d3c5f'){//fnb id
            return view('pages.fnb-equipment')->with(compact('subcat_slug','cat_slug'));
        }elseif($subcategory->id == '5b46f7cfc5d0f'){//housekeeping
            return view('pages.hk-equipment')->with(compact('subcat_slug','cat_slug'));
        }elseif($subcategory->id == '5b46f7ec357c1'){//opl
            return view('pages.opl-equipment')->with(compact('subcat_slug','cat_slug'));
        }else{
            abort('404');
        }

    }

    public function showServices($cat_slug,$subcat_slug){

        $category = ProductCategory::where('category_slug','=',$cat_slug)->first();

        if ($category === null) {
            abort('404');
        }

        $subcategory = ProductSubCategory::where('sub_product_slug', '=', $subcat_slug)
            ->Where('product_category_id','=',$category->id)->first();

        if ($subcategory === null) {
            abort('404');
        }

        if($subcategory->id == '5b46f7b8d3c5f'){//fnb id
            return view('pages.services')->with(compact('subcat_slug','cat_slug'));
        }elseif($subcategory->id == '5b46f7cfc5d0f'){
            return view('pages.hk-services')->with(compact('subcat_slug','cat_slug'));
        }elseif($subcategory->id == '5b46f7ec357c1'){
            return view('pages.opl-services')->with(compact('subcat_slug','cat_slug'));
        }else{
            abort('404');
        }

    }

    public function showSafetyTrain($cat_slug,$subcat_slug){

        $category = ProductCategory::where('category_slug','=',$cat_slug)->first();

        if ($category === null) {
            abort('404');
        }

        $subcategory = ProductSubCategory::where('sub_product_slug', '=', $subcat_slug)
            ->Where('product_category_id','=',$category->id)->first();

        if ($subcategory === null) {
            abort('404');
        }


        if($subcategory->id == '5b46f7b8d3c5f'){//fnb id
            return view('pages.fnb-safetytraining')->with(compact('subcat_slug','cat_slug'));
        }elseif($subcategory->id=='5b46f7cfc5d0f'){

            return view('pages.hk-safety-training')->with(compact('subcat_slug','cat_slug'));

        }elseif($subcategory->id=='5b46f7ec357c1'){

        return view('pages.opl-safety-training')->with(compact('subcat_slug','cat_slug'));

        }else{
            abort('404');
        }

    }

    public function newSafetyInitiatives($cat_slug,$subcat_slug){

        $category = ProductCategory::where('category_slug','=',$cat_slug)->first();

        if ($category === null) {
            abort('404');
        }


        $subcategory = ProductSubCategory::where('sub_product_slug', '=', $subcat_slug)
            ->Where('product_category_id','=',$category->id)->first();

        if ($subcategory === null) {
            abort('404');
        }


        if($subcategory->id == '5b46f7b8d3c5f'){//fnb id
            return view('pages.fnb-new-safety-initiatives')->with(compact('subcat_slug','cat_slug'));
        }else{
            abort('404');
        }

    }


    public function chemicalTrainingSafety($cat_slug,$subcat_slug){

        $category = ProductCategory::where('category_slug','=',$cat_slug)->first();

        if ($category === null) {
            abort('404');
        }


        $subcategory = ProductSubCategory::where('sub_product_slug', '=', $subcat_slug)
            ->Where('product_category_id','=',$category->id)->first();


        if ($subcategory === null) {
            abort('404');
        }


//        if($subcategory->id == '5b46f7b8d3c5f'){//fnb id
//            return view('pages.fnb-chemical-training-safety')->with(compact('subcat_slug','cat_slug'));
//        }else{
//            abort('404');
//        }

        return view('pages.fnb-chemical-training-safety')->with(compact('subcat_slug','cat_slug'));

    }


    public function newLabel($cat_slug,$subcat_slug){

        $category = ProductCategory::where('category_slug','=',$cat_slug)->first();

        if ($category === null) {
            abort('404');
        }


        $subcategory = ProductSubCategory::where('sub_product_slug', '=', $subcat_slug)
            ->Where('product_category_id','=',$category->id)->first();


        if ($subcategory === null) {
            abort('404');
        }


        if($subcategory->id == '5b46f7b8d3c5f'){//fnb id
            return view('pages.fnb-new-labels')->with(compact('subcat_slug','cat_slug'));
        }else{
            abort('404');
        }

    }

    public function safetyOpCharts($cat_slug,$subcat_slug){

        $category = ProductCategory::where('category_slug','=',$cat_slug)->first();

        if ($category === null) {
            abort('404');
        }


        $subcategory = ProductSubCategory::where('sub_product_slug', '=', $subcat_slug)
            ->Where('product_category_id','=',$category->id)->first();


        if ($subcategory === null) {
            abort('404');
        }


        if($subcategory->id == '5b46f7b8d3c5f'){//fnb id
            return view('pages.fnb-safety-op-charts')->with(compact('subcat_slug','cat_slug'));
        }else{
            abort('404');
        }

    }


    public function hospitalityPage(){
        return view('pages.hospitality');
    }


    public function productRange($cat_slug,$subcat_slug){

        $category = ProductCategory::where('category_slug','=',$cat_slug)->first();

        if ($category === null) {
            abort('404');
        }


        $subcategory = ProductSubCategory::where('sub_product_slug', '=', $subcat_slug)
            ->Where('product_category_id','=',$category->id)->first();

        if ($subcategory === null) {
            abort('404');
        }


        $productRange = ProductRange::Where('sub_product_category_id','=',$subcategory->id)->get();


        if ($productRange === null) {
            abort('404');
        }


        return view('pages.productrange')->with(compact('productRange','subcategory','category'));
    }

    public function productSdsItems($cat_slug,$subcat_slug,$prod_range_slug){

        $category = ProductCategory::where('category_slug','=',$cat_slug)->first();

        if ($category === null) {
            abort('404');
        }


        $subcategory = ProductSubCategory::where('sub_product_slug', '=', $subcat_slug)
            ->Where('product_category_id','=',$category->id)->first();

        if ($subcategory === null) {
            abort('404');
        }


        $productRange = ProductRange::Where('sub_product_category_id','=',$subcategory->id)
            ->Where('product_range_slug','=',$prod_range_slug)->first();

        if ($productRange === null) {
            abort('404');
        }


        $products = Product::Where('product_range_id','=',$productRange->id)->Orderby('product_name','asc')->get();

        if ($products === null) {
            abort('404');
        }


        return view('pages.productlistsds')->with(compact('productRange','products','category','subcategory'));
    }

    public function productInfo($cat_slug,$subcat_slug,$prod_range_slug,$product_slug){

        $category = ProductCategory::where('category_slug','=',$cat_slug)->first();

        if ($category === null) {
            abort('404');
        }


        $subcategory = ProductSubCategory::where('sub_product_slug', '=', $subcat_slug)
            ->Where('product_category_id','=',$category->id)->first();

        if ($subcategory === null) {
            abort('404');
        }

        $productRange = ProductRange::Where('sub_product_category_id','=',$subcategory->id)
            ->Where('product_range_slug','=',$prod_range_slug)->first();

        if ($productRange === null) {
            abort('404');
        }


        $product = Product::Where('product_slug','=',$product_slug)->Where('product_range_id','=',$productRange->id)->first();

        if ($product === null) {
            abort('404');
        }


        return view('pages.product-info')->with(compact('productRange','product','subcategory','category'));
    }

    public function newsPage(){

        return view('pages.news');
    }

    public function cleanSafeIndustrialPage(){

        return view('pages.cleansafe-industrial-dispensing');
    }

    public function contactUsPage(){

        return view('pages.contactus');
    }

    public function aboutUsPage(){

        return view('pages.aboutus');
    }

    public function servicesPage(){
        return view('pages.services-nav');
    }

    public function serviceReport(){
        return view('pages.service-report');
    }


    public function ourvaluesPage(){
        return view('pages.ourvalues');
    }

    public function companyProfilePage(){
        return view('pages.company-profile');
    }

    public function ourstoryPage(){
        return view('pages.ourstory');
    }



    public function searchProduct(Request $request){

        if($request->search_product==null){

            return redirect()->back();

        }else{
            $products = Product::Where('product_name','LIKE','%'.$request->search_product.'%')->Orderby('product_name','asc')
                ->get();
            return view('pages.search-result')->with(compact('products'));
        }
    }

    /*
     * Return view of Safety Training.
     */
    public function safetyTraining($cat_slug,$subcat_slug) {
        return view('pages.safety-training')->with(compact('subcat_slug','cat_slug'));
    }
}
