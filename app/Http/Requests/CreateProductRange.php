<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRange extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                $rules = [
                    'product_range_name' => 'required|max:100',
                    'product_range_description' => 'required|',
                    'sub_product_category_id' => 'required',
                    'product_range_img' => 'sometimes|image|mimes:jpeg,jpg,png|max:3000',
                ];
                break;

            default:
                $rules = [
                    'product_range_name' => 'required|max:100',
                    'product_range_description' => 'required|',
                    'sub_product_category_id' => 'required',
                    'product_range_img' => 'sometimes|image|mimes:jpeg,jpg,png|max:3000',
                ];
                break;

        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'product_range_name' => 'product range name',
            'product_range_description' => 'product range description|',
            'sub_product_category_id' => 'sub product category',
            'product_range_img' => 'product range image',
        ];
    }
}
