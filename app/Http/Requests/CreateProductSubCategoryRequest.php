<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductSubCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                $rules = [
                    'sub_cat_name' => 'required|max:100',
                    'sub_cat_description' => 'required|',
                    'select_category' => 'required',
                    'sub_cat_img' => 'sometimes|image|mimes:jpeg,jpg,png|max:3000',
                ];
                break;

            default:
                $rules = [
                    'sub_cat_name' => 'required|max:100',
                    'sub_cat_description' => 'required|',
                    'select_category' => 'required',
                    'sub_cat_img' => 'sometimes|image|mimes:jpeg,jpg,png|max:3000',
                ];
                break;

        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'sub_cat_name' => 'sub category  name',
            'sub_cat_description' => 'description',
            'select_category' => 'select category',
            'sub_cat_img' => 'select category image'
        ];
    }
}
