<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                $rules = [
                    'product_name' => 'required|max:100',
                    'product_range_id' => 'required',
                    'pack_size' => 'required',
                    'is_biodegrable' => 'required',
                    'sds' => 'sometimes|mimes:pdf',
                    'product_img' => 'sometimes|image|mimes:jpeg,jpg,png|max:3000',
                ];
                break;

            default:
                $rules = [
                    'product_name' => 'required|max:100',
                    'product_range_id' => 'required',
                    'pack_size' => 'required',
                    'is_biodegrable' => 'required',
                    'sds' => 'sometimes|mimes:pdf',
                    'product_img' => 'sometimes|image|mimes:jpeg,jpg,png|max:3000',
                ];
                break;

        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'product_range_id' => 'product range',
            'is_biodegrable' => 'biodegrable',
            'product_img' => 'product image',
        ];
    }
}
