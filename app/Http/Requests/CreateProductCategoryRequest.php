<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                $rules = [
                    'category_name' => 'required|max:100',
                    'category_description' => 'required|',
                    'category_video' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
                    'page_name' => 'required|max:100',
                    'page_content' => 'required|',
                ];
                break;

            default:
                $rules = [
                    'category_name' => 'required|max:100',
                    'category_description' => 'required|',
                    'category_video' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
                    'page_name' => 'required|max:100',
                    'page_content' => 'required|',
                ];
                break;

        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'category_video' => 'video link',
        ];
    }
}
