<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

use Illuminate\Database\Eloquent\Model;

use DataTables;
use Image;
use Illuminate\Support\Facades\URL;

use Illuminate\Support\Facades\File;

use Illuminate\Support\Facades\Input;



class Product extends Model
{
    use HasSlug;

    public $incrementing = false;

    protected $table = 'products';


    protected $fillable = [
        'id','product_name', 'product_description','product_img','product_range_id','product_range_id','pack_size','sds','product_slug','product_details','is_biodegrable'
    ];

    public function productRange(){
        return $this->belongsTo('App\ProductRange','product_range_id');
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('product_name')
            ->saveSlugsTo('product_slug');
    }

    public static function allProducts($request){

        $products = Product::with('productRange');

        return DataTables::eloquent($products)

            ->addColumn('action', function ($products) {
                $btn_html = '<a href="'.route('products.edit',$products->id).'" class="btn btn-info">Edit</a> ';
                $btn_html.= '| <a href="#delete-modal" data-toggle="modal" data-product_id="'.$products->id.'" class="btn btn-danger">Delete</a>';
                return $btn_html;
            })
            ->addColumn('sds_pdf', function ($products) {
                $sdsFile = '<a href="' . route('productinfo.pdf',['product_id'=>$products->id]) . '" data-toggle="popover" data-content="Product info" target="_blank"><img src="' . asset('fe-content/images/printer.png') . '" width="20px" ></a>';
                if($products->sds!=null) {

                    $sdsFile .= ' | ';
                    $sdsFile .= '<a href="' . asset('/uploads/product-sds/' . $products->sds) . '" data-toggle="popover" data-content="Product SDS" target="_blank"><img src="' . asset('fe-content/images/pdf.png') . '" width="20px" ></a>';
                }
                else{
                    $sdsFile = 'no sds file';
                }

                return $sdsFile;
            })
            ->editColumn('created_at', function ($products) {
                $newDate = date('m-d-Y', strtotime($products->created_at));
                return $newDate;
            })
            ->editColumn('productRange.product_range_name', function ($products) {
                $productRangename = $products->productRange->product_range_name;
                return $productRangename;
            })
            ->rawColumns(['sds_pdf','action'])
            ->toJson();
    }

    public static function storeProduct($request){

        $product = new Product();
        $product->id = uniqid();
        $product->product_name = $request->input('product_name');
        $product->product_description = $request->input('product_description');
        $product->product_range_id = $request->input('product_range_id');
        $product->pack_size = $request->input('pack_size');
        $product->is_biodegrable = $request->input('is_biodegrable');


        if($request->hasFile('product_img')){

            $image = $request->file('product_img');

            $filename = $product->id.'_'.time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/uploads/product-image/'.$filename);

            Image::make($image)->resize(219, 255, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath);

            $product->product_img = $filename;
        }


        if($request->hasFile('sds')){

            $pdfFile = $request->file('sds');
            $filename = $product->product_name.'_SDS_'.time().'.'.$pdfFile->getClientOriginalExtension();
            $request->sds->move(public_path('/uploads/product-sds/'), $filename);
            $product->sds = $filename;

        }

        $product_details = $request->product_details;
        $dom = new \domdocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($product_details, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getelementsbytagname('img');

        foreach($images as $img){

            $src = $img->getAttribute('src');

            // if the img source is 'data-url'
            if(preg_match('/data:image/', $src)){

                // get the mimetype
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];

                // Generating a random filename
                $filename = $product->id.'_'.uniqid();
                $filepath = "/uploads/product-img-details/$filename.$mimetype";

                // @see http://image.intervention.io/api/
                $image = Image::make($src)
                    // resize if required
                    /* ->resize(300, 200) */
                    ->encode($mimetype, 100) 	// encode file to the specified mimetype
                    ->save(public_path($filepath));

                $new_src = asset($filepath);
                $img->removeAttribute('src');
                $img->setAttribute('src', $new_src);
            } // <!--endif
        } // <!--endforeach

        $product->product_details = $dom->saveHTML();
        $product->save();

        return true;
    }

    /**
     * @param $request
     * @param $id
     * @return bool
     */
    public static function updateProduct($request, $id){

        ini_set('memory_limit','256M');
        $product = Product::find($id);
        $product->product_name = $request->input('product_name');
        $product->product_description = $request->input('product_description');
        $product->product_range_id = $request->input('product_range_id');
        $product->pack_size = $request->input('pack_size');
        $product->is_biodegrable = $request->input('is_biodegrable');


        if($request->hasFile('product_img')){

            $image = $request->file('product_img');

            $filename = $id.'_'.time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/uploads/product-image/'.$filename);

            Image::make($image)->resize(219, 255, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath);

            $oldImgFile = $product->product_img;

            $product->product_img = $filename;

            File::delete(public_path('/uploads/product-image/'.$oldImgFile));

        }

        if($request->hasFile('sds')){

            $pdfFile = $request->file('sds');
            $filename = $product->product_name.'_SDS_'.time().'.'.$pdfFile->getClientOriginalExtension();
            $request->sds->move(public_path('/uploads/product-sds/'), $filename);
            $product->sds = $filename;

        }


        $product_details = $request->product_details;
        $dom = new \domdocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($product_details, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getelementsbytagname('img');

        foreach($images as $img){

            $src = $img->getAttribute('src');

            // if the img source is 'data-url'
            if(preg_match('/data:image/', $src)){

                // get the mimetype
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];

                // Generating a random filename
                $filename = $product->id.'_'.uniqid();
                $filepath = "/uploads/product-img-details/$filename.$mimetype";

                // @see http://image.intervention.io/api/
                $image = Image::make($src)
                    // resize if required
                    /* ->resize(300, 200) */
                    ->encode($mimetype, 100) 	// encode file to the specified mimetype
                    ->save(public_path($filepath));

                $new_src = asset($filepath);
                $img->removeAttribute('src');
                $img->setAttribute('src', $new_src);


            } // <!--endif
        } // <!--endforeach

        //remove old image from file directory

        //dd($request->deletedImg);
        if($request->has('deletedImg')){

            $old_img = $request->deletedImg;

            foreach($old_img as $img){

                $file_name = str_replace(URL::to('/'), '', $img);

                File::delete(public_path($file_name));
            }
        }


        $product->product_details = $dom->saveHTML();

        $product->save();

        return true;
    }
}
