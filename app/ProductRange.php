<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

use DataTables;
use Image;
use Illuminate\Support\Facades\URL;

use Illuminate\Support\Facades\File;

use Illuminate\Support\Facades\Input;

class ProductRange extends Model
{

    use HasSlug;

    public $incrementing = false;

    protected $table = 'product_range';

    protected $fillable = [
        'id','product_range_name', 'product_range_description','product_range_img','sub_product_category_id','product_range_slug'
    ];

    public function productSubCategory(){
        return $this->belongsTo('App\ProductSubCategory','sub_product_category_id');
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('product_range_name')
            ->saveSlugsTo('product_range_slug');
    }

    public static function storeProductRange($request){

        $product_range = new ProductRange();
        $product_range->id = uniqid();
        $product_range->product_range_name = $request->input('product_range_name');
        $product_range->product_range_description = $request->input('product_range_description');
        if($request->hasFile('product_range_img')){

            $image = $request->file('product_range_img');

            $filename = $product_range->id.'_'.time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/uploads/product-range-images/'.$filename);

            Image::make($image)->resize(120,null,function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath);

            $product_range->product_range_img = $filename;
        }
        $product_range->sub_product_category_id = $request->input('sub_product_category_id');
        $product_range->save();

        return true;
    }

    public static function updateProductRange($request,$id){

        $product_range = ProductRange::find($id);
        $product_range->product_range_name = $request->input('product_range_name');
        $product_range->product_range_description = $request->input('product_range_description');

        if($request->hasFile('product_range_img')){

            $image = $request->file('product_range_img');

            $filename = $id.'_'.time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/uploads/product-range-images/'.$filename);

            Image::make($image)->resize(120,null,function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath);

            $oldImgFile = $product_range->product_range_img;

            $product_range->product_range_img = $filename;

            File::delete(public_path('/uploads/product-range-images/'.$oldImgFile));

        }
        $product_range->sub_product_category_id = $request->input('sub_product_category_id');
        $product_range->save();

        return true;
    }

    public static function allProductRange($request){

        $product_range = ProductRange::with('productSubCategory');

        return DataTables::eloquent($product_range)

            ->addColumn('action', function ($product_range) {

                    $btn_html = '<a href="'.route('productrange.edit',$product_range->id).'" class="btn btn-info">Edit</a> ';
                    $btn_html .= '| <a href="#delete-modal" data-toggle="modal" data-product_range_id="'.$product_range->id.'" class="btn btn-danger">Delete</a>';
                return $btn_html;
            })
            ->editColumn('created_at', function ($product_range) {
                $newDate = date('m-d-Y', strtotime($product_range->created_at));
                return $newDate;
            })
            ->editColumn('productSubCategory.sub_cat_name', function ($product_range) {
                $sub_category_name = $product_range->productSubCategory->sub_cat_name;
                return $sub_category_name;
            })
            ->toJson();
    }

}
