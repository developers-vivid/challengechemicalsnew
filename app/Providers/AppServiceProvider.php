<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\ProductCategory;
use App\ProductSubCategory;
use App\ProductRange;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

//        view()->composer('*',function ($view){
//            $view->with('product_categories',ProductCategory::getAllProductCategory()->get());
//        });

        view()->composer('*',function ($view){
            $cat_hospitality = ProductCategory::find('5b3eed45edd03');
            $cat_automotive = ProductCategory::find('5b46e60724f9e');
            $cat_toll = ProductCategory::find('5b46e623f0cf8');

            $cat_water = ProductCategory::find('5b46e64fbf788');
            $cat_water_sub = ProductSubCategory::findOrFail('5b9f0bc083b76');
            $cat_water_range = ProductRange::findOrFail('5b9f0bd577e58');

            $cat_food = ProductCategory::find('5b46e66e4a3b6');
            $cat_government  = ProductCategory::find('5b46e6d001e15');
            $cat_agedcare  = ProductCategory::find('5b46e700d68ea');
            $view->with(compact('cat_hospitality','cat_automotive','cat_toll','cat_water','cat_water_sub','cat_water_range','cat_food','cat_government','cat_agedcare'));
        });


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
