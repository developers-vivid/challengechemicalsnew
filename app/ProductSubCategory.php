<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Image;
use Illuminate\Support\Facades\URL;

use Illuminate\Support\Facades\File;

use Illuminate\Support\Facades\Input;


class ProductSubCategory extends Model
{
    use HasSlug;

    public $incrementing = false;

    protected $table = 'sub_product_category';


    protected $fillable = [
        'id','sub_cat_name', 'sub_cat_description','sub_cat_img','product_category_id','sub_product_slug'
    ];

    public function productCategory(){
        return $this->belongsTo('App\ProductCategory','product_category_id');
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('sub_cat_name')
            ->saveSlugsTo('sub_product_slug');
    }


    public static function getAllSubCategoryWithProductCategory(){

        $product_sub_category = ProductSubCategory::with('productCategory')
            ->orderBy('sub_cat_name','asc');

        return $product_sub_category;
    }

    public static function storeSubProductCategory($request){

        $sub_category = new ProductSubCategory();
        $sub_category->id = uniqid();
        $sub_category->sub_cat_name = $request->input('sub_cat_name');
        //$sub_category->sub_cat_img = null;

        if($request->hasFile('sub_cat_img')){

            $image = $request->file('sub_cat_img');

            $filename = $sub_category->id.'_'.time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/uploads/product-sub-category-image/'.$filename);

            Image::make($image)->resize(223, null, function ($constraint) {
//                $constraint->aspectRatio();
            })->save($destinationPath);

            $sub_category->sub_cat_img = $filename;
        }

        $sub_category->sub_cat_description = $request->input('sub_cat_description');
        $sub_category->product_category_id = $request->input('select_category');
        $sub_category->save();

        return true;
    }

    public static function updateProductCategory($request,$id){

        $sub_category = ProductSubCategory::find($id);
        $sub_category->sub_cat_name = $request->input('sub_cat_name');
        //$sub_category->sub_cat_img = null;

        if($request->hasFile('sub_cat_img')){

            $image = $request->file('sub_cat_img');

            $filename = $id.'_'.time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/uploads/product-sub-category-image/'.$filename);

            Image::make($image)->resize(223, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath);

            $oldImgFile = $sub_category->sub_cat_img;

            $sub_category->sub_cat_img = $filename;

            File::delete(public_path('/uploads/product-sub-category-image/'.$oldImgFile));

        }
        $sub_category->sub_cat_description = $request->input('sub_cat_description');
        $sub_category->product_category_id = $request->input('select_category');
        $sub_category->save();

        return true;
    }
}
