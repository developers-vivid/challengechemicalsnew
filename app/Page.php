<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    use HasSlug;

    public $incrementing = false;

    protected $table = 'pages';


    protected $fillable = [
        'id','page_name', 'page_content','page_slug'
    ];

    public function product_category(){
        return $this->hasMany('App\ProductCategory');
    }

    public function getSlugOptions() : SlugOptions
    {

        return SlugOptions::create()
            ->generateSlugsFrom('page_name')
            ->saveSlugsTo('page_slug');
    }


    public static function storePage($request){

        $page = new Page();
        $page->id = uniqid();
        $page->page_name = $request->input('page_name');
        $page->page_content = $request->input('page_content');
        $page->save();

        return true;
    }

    public static function updatePage($request,$id){

        $page = Page::find($id);
        $page->page_name = $request->input('page_name');
        $page->page_content = $request->input('page_content');
        $page->save();

        return true;
    }

}
