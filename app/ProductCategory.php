<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

use Illuminate\Database\Eloquent\Model;


class ProductCategory extends Model
{
    use HasSlug;

    public $incrementing = false;

    protected $table = 'product_category';


    protected $fillable = [
        'id','category_name', 'category_description','category_img','page_id','category_slug','category_video'
    ];

    public function page(){
        return $this->belongsTo('App\Page','page_id');
    }

    public function productSubCategory(){
        return $this->hasMany('App\ProductSubCategory');
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('category_name')
            ->saveSlugsTo('category_slug');
    }

    public static function storeProductCategory($request){


        $page = new Page();
        $page->id = uniqid();
        $page->page_name = $request->input('page_name');
        $page->page_content = $request->input('page_content');
        $page->save();

        $product_category = new ProductCategory();
        $product_category->id = uniqid();
        $product_category->category_name = $request->input('category_name');
        $product_category->category_video = $request->input('category_video');
        $product_category->category_description = $request->input('category_description');
        $product_category->page_id = $page->id;
        $product_category->save();

        return true;
    }

    public static function updateProductCategory($request,$id){

        $product_category = ProductCategory::find($id);

        $product_category->category_name = $request->input('category_name');

        $product_category->category_video = $request->input('category_video');

        $product_category->category_description = $request->input('category_description');
        $product_category->save();

        $page= Page::find($product_category->page_id);
        $page->page_name = $request->input('page_name');

        $page->page_content = $request->input('page_content');
        $page->save();

        return true;
    }

    public static function getProductCatWithPage(){

        $productcategory = ProductCategory::with('page');

        return $productcategory;
    }

    public static function getAllProductCategory(){

        $product_categories = ProductCategory::select('id','category_name','category_description','created_at');

        return $product_categories;
    }
}
