<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductRangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_range', function (Blueprint $table) {
            $table->string('id')->unique()->primary();
            $table->string('product_range_name');
            $table->text('product_range_description')->nullable();
            $table->string('product_range_img')->nullable();
            $table->string('sub_product_category_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_range');
    }
}
