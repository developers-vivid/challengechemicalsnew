<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubProductCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_product_category', function (Blueprint $table) {
            $table->string('id')->unique()->primary();
            $table->string('sub_cat_name');
            $table->text('sub_cat_description')->nullable();
            $table->string('sub_cat_img')->nullable();
            $table->string('product_category_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_sub_product_category');
    }
}
