@extends('layouts.master')

@section('content')
    <div class="row" style="margin-top:-20px !important;">
        <div class="col-lg-12 text-center exploreHeader">
            <h1 class="exploreText">ABOUT US</h1>
        </div>
    </div>


    <div class="row" style="margin-top:80px !important; margin-bottom:80px !important;">
        <div class="col-sm-1">
        </div>

        <div class="col-sm-10">
            <h4><strong><a href="{{route('ourvalues.page')}}">Values</a></strong></h4>
            <br>
            <p>Our Vision</p>
            <p>To be recognised by Australian industries as a leader in cleaning chemicals, valued by our customers for our technical expertise, and respected for our commitment to environmentally sound chemistry.</p>
            <br>
            <p>Our Mission</p>
            <p>Challenge Chemicals’ Mission is to deliver on its four core values with everything we do; guaranteeing high quality cleaning products our customers can rely on</p>
            <p>Click on Values Tab for full document of our Values.</p>
            <br>
            <br>
            <br>
            <h4><strong><a href="{{route('company-profile.page')}}">Company Profile</a></strong></h4>
            <p>Challenge Chemicals Aust. was established in Kwinana Beach over 28 years ago by a group of people who wished to make a difference to the
                chemical industry:</p>
            <br>
            <p>Challenge Chemicals is a privately owned totally Australian business, with distributors throughout Australia.</p>
            <br>
            <p>The business has developed and grown from its humble beginnings to being a major supplier to the mining and general industrial sectors, hospitality, food processing, transport, laundry, water treatments as well as specialty chemicals sold under brand names.</p>
            <br>
            <p>The manufacturing plant, warehouse, distribution and administration are situated at 6 Butcher Street Kwinana Beach</p>
            <br>
            <p>Click on Company Profile Tab for full Company Profile Doc.</p>
            <br>
            <br>
            <br>
            <h4><strong><a href="{{route('ourstory.page')}}">Challenge Chemicals Australia – Over 40 years Experience.</a></strong></h4>
            <br>
            <p>Challenge Chemicals Australia (CCA) is a leading West Australian based chemical manufacturer, with experience in manufacturing dating back to 1975. That over 40 years in manufacturing experience. Starting off with a handful of employees we have grown and developed our business to where we are now a major supplier to the mining, transport, marine and general industrial sectors along with the hospitality, healthcare, textile, food processing and water treatment industries. We also manufacture specialty chemicals sold under brand names suppling products all over Australia and overseas.</p>
            <br>
            <p>Our company was founded on the mantra of “Service with Products of Technology” and today this is
                still the corner stone of our company’s philosophy. Our focus is on developing and testing products
                and equipment while providing our customers with outstanding service and safety.</p>
            <br>
            <br>
            <h4><strong>Management</strong></h4>
            <p>Richard Niven</p>
            <p>Manager</p>
            <br>
            <p>Martin Cattalini</p>
            <p>Sales Manager/Director</p>
            <br>
            <p>Andrew de Bie</p>
            <p>Chemist</p>
            <br>
            <p>Tina Evans</p>
            <p>Financials/Accounts</p>
            <br>
            <p>Ross Green</p>
            <p>Production Manager</p>
        </div>


        <div class="col-sm-1">
        </div>
    </div>
@endsection