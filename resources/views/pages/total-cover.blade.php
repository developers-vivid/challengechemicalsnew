@extends('layouts.master')

@section('page_css')
    <link href="{{ asset('fe-content/css/fnb-main.css') }}" rel="stylesheet">
@endsection
@section('content')


    <div class="row" style="background-image:url('{{asset('fe-content/images/fnb-bg3.jpg')}}'); background-size: 100% 100%;">
        <div class="col-lg-8 text-center circular-bg">

            <nav class="circular-menu container" style="position:relative; left:-40px;">
                <a href="" class="menu-button img-responsive" style="width:220px; margin-top:-30px;"><img style="position:relative; z-index:0;" class="img-responsive" src="{{asset('fe-content/images/total-cover-logo.png')}}" />
                </a>
                <div class="circle img-responsive container">
                    <a href="{{route('category.page',$cat_hospitality->category_slug)}}"></a>
                    <a href="{{route('safety-training.page',[$cat_slug,$subcat_slug])}}"><img style="position:relative; top:10px;" class="img-responsive" src="{{asset('fe-content/images/safety-and-training.png')}}" /></a>
                    <a href="{{route('safety-training.page',[$cat_slug,$subcat_slug])}}"></a>
                    <a href="{{route('contactus.page')}}" style="width:200px;" ><img style="position:relative; right:-20px; top:-15px;" class="img-responsive" src="{{asset('fe-content/images/support.png')}}" /></a>
                    <a href="{{route('contactus.page')}}"></a>
                    <a href="{{route('equipment.page',[$cat_slug,$subcat_slug])}}" style="width:310px;" ><img style="position:relative; bottom:-50px; left:-30px;" class="img-responsive" src="{{asset('fe-content/images/equipment.png')}}" /></a>
                    <a href="{{route('equipment.page',[$cat_slug,$subcat_slug])}}"></a>
                    <a href="{{route('showServices.page',[$cat_slug,$subcat_slug])}}" style="width:190px;" ><img style="position:relative; right:-60px; top:-15px;" class="img-responsive" src="{{asset('fe-content/images/services.png')}}" /></a>
                    <a href="{{route('showServices.page',[$cat_slug,$subcat_slug])}}"></a>
                    <a href="{{route('category.page',$cat_hospitality->category_slug)}}" style="width:285px;" ><img style="position:relative; bottom:-10px;" class="img-responsive" src="{{asset('fe-content/images/products.png')}}" /></a>
                </div>
            </nav>
            <br>
            <br>

        </div>
        <style>
            @media screen and (max-width: 1200px) and (min-width: 290px) {
                .circgg1{
                    position:relative;
                    left:0px !important;
                }
            }
        </style>
        <div class="col-lg-4 text-center circular-bg circgg1" style="position:relative; left:-135px; top:-80px;">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div style="background-color: rgba(255, 255, 255, 0.5); padding:20px; border-radius:1px;">
                <h1 style="color:#0f386c; font-weight:bold; font-size:2em;">CHALLENGE TOTAL COVER</h1>
                <br>
                <p style="width:70%; margin:0 auto; color:#0f386c; font-size:15px;">Total Cover Protection For Your Facility, Staff and Customers While Enabling Your To Control Costs.</p>
                <br>
                <p style="width:70%; margin:0 auto; color:#0f386c; font-size:19px;">Please Click on our 5 pillars of support for further information</p>
            </div>
            <br>
            <br>
        </div>		
    </div>

@endsection

@section('page_js')
    <script src="{{ asset('fe-content/js/fnb.js') }}"></script>
@endsection