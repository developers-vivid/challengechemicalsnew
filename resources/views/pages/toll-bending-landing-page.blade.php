@extends('layouts.master')

@section('page_css')

@endsection
@section('content')
    <div class="row" style="background-image:url('{{asset('fe-content/images/toll-bending-bg1.png')}}'); color:#ffffff;">
        <style>
            .tollText{
                background-color: hsla(69, 14%, 19%, 0.5);
            }

            .tollText h1{
                font-size:2.8em;
                font-weight:bold;
                padding-left:30px;
                padding-right:30px;
                padding-top:20px;
                padding-bottom:20px;
            }

            .tollText p{
                font-size:1.5em;
                padding-left:30px;
                padding-right:30px;
                padding-top:20px;
                padding-bottom:20px;
            }
        </style>
        <div class="col-lg-6 text-left tollText">
            <br>
            <h1>CHALLENGE TOLL BLENDING</h1>
            <br>
            <p>
                With over 40 years of manufacturing
                experience Challenge Chemicals can custom
                formulate a product or manufacture to your specific formulations.
            </p>
            <br>
            <p>
                With management committed to product performance and biological compatibility,
                all products are produced from high quality raw materials and manufactured
                under quality assurance procedures conforming to BSI ISO 9001; 2008
            </p>
            <br>
            <br>
            <br>
            <img style="padding:30px;" src="{{asset('fe-content/images/toll-bcc1.png')}}" />
            <img style="padding:30px;" src="{{asset('fe-content/images/toll-bcc2.png')}}" />
            <br>
            <br>
            <br>
        </div>
        <div class="col-lg-6 text-left">
        </div>
    </div>

@endsection

@section('page_js')

@endsection