@extends('layouts.master')
<style>
    /*.row-alternate:nth-child(even) div.alternate {background-color:#7ec8f9;}*/
    /*.row-alternate:nth-child(odd) div.alternate{background-color:#28aafe;}*/



    .row-alternate:nth-child(8n+1) div.alternate,
    .row-alternate:nth-child(8n+3) div.alternate,
    .row-alternate:nth-child(8n+6) div.alternate,
    .row-alternate:nth-child(8n+8) div.alternate{
        background-color: #7ec8f9;
    }


    .row-alternate:nth-child(8n+2) div.alternate,
    .row-alternate:nth-child(8n+4) div.alternate,
    .row-alternate:nth-child(8n+5) div.alternate,
    .row-alternate:nth-child(8n+7) div.alternate
    {
        background-color:#28aafe;
    }




</style>
@section('content')

    <div class="row" style="margin-top:-20px !important;">
        <div class="col-lg-12 text-center exploreHeader">
            <h1 class="exploreText">{{strtoupper($subcategory->sub_cat_name)}} <br> PRODUCT RANGE</h1>
        </div>
    </div>

    <div class="row"  style="margin-top:-20px !important;">
        @forelse($productRange as $pr)
        <a href="{{route('productsds.page',[$category->category_slug,$subcategory->sub_product_slug,$pr->product_range_slug])}}" class="row-alternate">
            <div class="col-lg-3 text-center alternate" style="height:380px;">
                <br>
                <br>
                <br>
                <img src="{{($pr->product_range_img == '' ) ? asset('uploads/product-range-images/blank.png') : asset('uploads/product-range-images/'.$pr->product_range_img)}}" width="120px" height="" />
                <h3 class="exploreText">{{$pr->product_range_name}}</h3>
            </div>
        </a>
            @empty
            <h3 class="text-center">No product range available</h3>
        @endforelse
    </div>

@endsection