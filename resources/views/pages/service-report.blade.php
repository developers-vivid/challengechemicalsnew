@extends('layouts.master')

@section('page_css')

@endsection

@section('content')
    <div class="row">
        <div class="col-lg-1">
        </div>
        <div class="col-lg-10 text-left">
            <br>
            <h3>KITCHEN AND WAREWASHING SERVICE REPORT</h3>
            <br>
            <style>
                .tableKitchen tbody tr td{
                    padding-top:40px;
                    padding-bottom:40px;
                    text-align:center;
                }
            </style>
            <table class="table table-bordered tableKitchen">
                <tbody>
                <tr>
                    <td style="background-color:#e1e1e1;">Kitchen Contact</td>
                    <td>Surin Balaraman</td>
                    <td style="background-color:#e1e1e1;">Brand of Warewashers</td>
                    <td>Hobart Conveyors</td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">No.of warewashers</td>
                    <td>1</td>
                    <td style="background-color:#e1e1e1;">Dispensers</td>
                    <td>DR35</td>
                </tr>
                </tbody>
            </table>
            <br>
            <h5>WAREWASHING RESULTS</h5>
            <br>
            <h5>MACHINE 1 (main kitchen) Hobart Conveyor</h5>
            <br>
            <table class="table table-bordered tableKitchen">
                <tbody>
                <tr>
                    <td style="background-color:#e1e1e1;">WASH TYPE</td>
                    <td style="background-color:#e1e1e1;">POOR</td>
                    <td style="background-color:#e1e1e1;">FAIR</td>
                    <td style="background-color:#e1e1e1;">GOOD</td>
                    <td style="background-color:#e1e1e1;">EXCELLENT</td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">PLATES</td>
                    <td></td>
                    <td><strong>X</strong></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">GLASSES</td>
                    <td></td>
                    <td><strong>X</strong></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">CUTLERY</td>
                    <td></td>
                    <td><strong>X</strong></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">POTS</td>
                    <td></td>
                    <td><strong>X</strong></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">OTHER</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
            <br>
            <table class="table table-bordered tableKitchen">
                <tbody>
                <tr>
                    <td style="background-color:#e1e1e1;">WASH TEMPERATURES</td>
                    <td>60 DEGREES  (ACCEPTABLE TEMP RANGE 55-65)</td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">RINSE TEMPERATURES</td>
                    <td>82 DEGREES  (ACCEPTABLE TEMP RANGE 82+)</td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">TITRATION LEVEL - DETERGENT</td>
                    <td>7 DROPS</td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">TITRATION LEVEL - RINSE ADDITIVE</td>
                    <td>1MLS</td>
                </tr>
                </tbody>
            </table>
            <br>
            <h5>WAREWASHING VISUAL INSPECTION</h5>
            <br>
            <table class="table table-bordered tableKitchen">
                <tbody>
                <tr>
                    <td style="background-color:#e1e1e1;">MACHINE AREA</td>
                    <td style="background-color:#e1e1e1;">CHECKED</td>
                    <td style="background-color:#e1e1e1;">FINDINGS</td>
                    <td style="background-color:#e1e1e1;">ACTION TAKEN</td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">MACHINE</td>
                    <td>OK</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">ARMS</td>
                    <td>OK</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">JETS</td>
                    <td>OK</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">CURTAINS</td>
                    <td>ISSUE</td>
                    <td>IN POOR CONDITION</td>
                    <td>RECOMMEND REPLACING</td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">DOOR</td>
                    <td>OK</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">DRAIN</td>
                    <td>OK</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">NOZZLES</td>
                    <td>OK</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">SCRAP TRAYS</td>
                    <td>OK</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">WATER</td>
                    <td>OK</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">WATER LEVELS</td>
                    <td>OK</td>
                    <td></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
            <br>
            <h5>OH&S VISUAL CHECK</h5>
            <br>
            <table class="table table-bordered tableKitchen">
                <tbody>
                <tr>
                    <td style="background-color:#e1e1e1;"></td>
                    <td style="background-color:#e1e1e1;"></td>
                    <td style="background-color:#e1e1e1;">COMMENT</td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">SPRAY BOTTLE LABELS</td>
                    <td>OK</td>
                    <td>USING CORRECTLY LABELLED SPRAY BOTTLES</td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">SANITIZER TITRATION</td>
                    <td>OK</td>
                    <td>TESTED AT CORRECT 40 PPM</td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">SAFETY CHARTS</td>
                    <td>OK</td>
                    <td>ALL IN PLACE, HAVE CLEANED</td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">SDS</td>
                    <td>OK</td>
                    <td>ALL IN PLACE AND IN DATE</td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">CHEMICAL DRUM LABELS</td>
                    <td>OK</td>
                    <td>ALL OK</td>
                </tr>
                <tr>
                    <td style="background-color:#e1e1e1;">CHEMICAL STORE ROOM</td>
                    <td>OK</td>
                    <td>ALL OK</td>
                </tr>
                </tbody>
            </table>
            <br>
            <h5>DISPENSERS AND DOSING SYSTEM</h5>
            <br>
            <table class="table table-bordered tableKitchen">
                <tbody>
                <tr>
                    <td style="background-color:#e1e1e1;">TYPE OF DISPENSER</td>
                    <td style="background-color:#e1e1e1;">CHECKED</td>
                    <td style="background-color:#e1e1e1;">COMMENT</td>
                </tr>
                <tr>
                    <td>7X SEKO DETERGENT PUMPS</td>
                    <td>OK</td>
                    <td>CHECKED AND CLEANED</td>
                </tr>
                <tr>
                    <td>1X DR35</td>
                    <td>OK</td>
                    <td>CHECKED AND CLEANED</td>
                </tr>
                <tr>
                    <td>6X SINGLE BOTTLE FILL</td>
                    <td>OK</td>
                    <td>CHECKED AND CLEANED</td>
                </tr>
                <tr>
                    <td>5X SINGE BOTTLE FILL</td>
                    <td>OK</td>
                    <td>CHECKED AND CLEANED</td>
                </tr>
                <tr>
                    <td>2X DEMA LAUNDRY PUMPS</td>
                    <td>OK</td>
                    <td>CHECKED AND CLEANED</td>
                </tr>
                </tbody>
            </table>
            <br>
            <h5>FINAL COMMENTS/ CORRECTIVE ACTIONS</h5>
            <br>
            <div class="card" style="border:2px solid #e1e1e1; padding:20px;">
                <div class="card-body">
                    Have completed my checks on all glasswashers on site, some are not in use at present.
                    <br>
                    <br>
                    I have replaced all squeeze tubes on all dish machine dispensers, this will ensure the correct amount
                    of chemical is dispensed and get better results pushing rating to good and above.
                    <br>
                    <br>
                    The top rinse arms on the restaurant glasswasher are getting rusty.
                    <br>
                    <br>
                    Main dish machine was ok, I have also re-calibrated the probe to ensure the dispenser operates
                    correctly.
                    <br>
                    <br>
                    I have slightly increased the detergent level on the main dish machine, this was to help improve
                    results further. The dish machine racks are in a poor condition, some of these don’t get picked up
                    by the conveyor, would recommend purchasing some new ones.
                </div>
            </div>
            <br>
            <br>
        </div>
        <div class="col-lg-1">
        </div>
    </div>
@endsection

@section('page_js')

@endsection