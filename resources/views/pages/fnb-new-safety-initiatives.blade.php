@extends('layouts.master')

@section('page_css')

@endsection

@section('content')
    <div class="row" style="margin-top:-20px !important;">
        <div class="col-lg-12 text-center exploreHeader">
            <h1 class="exploreText">NEW SAFETY INITIATIVES</h1>
        </div>
    </div>


    <div class="row" style="margin-top:80px !important; margin-bottom:80px !important;">
        <div class="col-sm-1">
        </div>

        <div class="col-sm-10">
            <h5>New Housekeeping and Food & Beverage 5 Litre Colour and Number Coded Labels</h5>
            <br>
            <img class="img-responsive" src="{{asset('fe-content/images/in1.png')}}" />
            <br>
            <br>
            <br>
            <div class="col-sm-6">
                <p class="text-center">
                    <strong>New Operator Safety Wall Charts</strong><br>
                    Identify and Cross Reference Products<br>
                    to Cleaning Task and Safety Requirement<br>
                </p>
                <br>
                <img width="300px" class="img-responsive" style="margin:0 auto;" src="{{asset('fe-content/images/in2.jpg')}}" />
            </div>
            <div class="col-sm-6">
                <p class="text-center">
                    <strong>New Emergency First Aid Wall Charts</strong><br>
                    A reference Chart in case of a Spill, Fire or Chemicals Emergency<br>
                </p>
                <br>
                <img width="300px" class="img-responsive" style="margin:0 auto;" src="{{asset('fe-content/images/in3.jpg')}}" />
            </div>
            <div class="col-sm-12">
                <br>
                <br>
                <p class="text-left" style="width:80%; margin:0 auto;">
                    <strong>Step by Step Cleaning Procedure and Safety Booklet</strong><br>
                    Cleaning procedure Booklets For Housekeeping and Kitchen Cleaning are available with Step By Step Cleaning Instructions,
                    Product Placement and Safety Requirements.<br>
                    <br>
                    <img width="300px" class="img-responsive" src="{{asset('fe-content/images/in4.png')}}" />
                </p>
            </div>
            <div class="col-sm-12">
                <br>
                <br>
                <p class="text-left" style="width:80%; margin:0 auto;">
                    Typical Installation of Seko Promax Dispensing System<br>
                    <br>
                    <img class="img-responsive" src="{{asset('fe-content/images/in5.png')}}" />
                </p>
            </div>
        </div>


        <div class="col-sm-1">
        </div>
    </div>
@endsection

@section('page_js')

@endsection