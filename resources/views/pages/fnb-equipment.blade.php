@extends('layouts.master')

@section('page_css')
    <link href="{{ asset('fe-content/css/fnb.css') }}" rel="stylesheet">
@endsection

@section('content')
    {{--<div class="row" style="background-image:url('{{asset('fe-content/images/eq2-bg.jpg')}}'); background-size:100% 100%;">--}}
        {{--<div class="col-lg-12 text-center circular-bg">--}}
		{{--<br>--}}
		{{--<h1 style="color:#0f386c; font-weight:bold;">EQUIPMENT</h1>--}}
		{{--<p style="color:#0f386c; font-weight:bold;">Total Protection for your facility, Staff and Customers</p>--}}
		{{--<h3 style="color:#0f386c; font-weight:bold;">Challenge Chemicals and SEKO Australia </h3>--}}
		{{--<br>--}}
		{{--<p style="width:70%; margin:0 auto; color:#0f386c;">Challenge Chemicals Australia have partnered with SEKO, Australia , a global leader in pumps and dispensing systems, providing a  TOTAL COVER solution to chemical dosing and dispensing.</p>--}}
		{{--<br>--}}
		{{--<br>--}}

            {{--<nav class="circular-menu container">--}}
                {{--<a href="" class="menu-button img-responsive" style="width:150px; margin-top:5px; margin-left:-5px;"><img style="position:relative; z-index:0;" class="img-responsive" src="{{asset('fe-content/images/total-cover.png')}}" />--}}
                    {{--<h5 style="line-height:20px; margin-top:0px; width:110%; margin-left:-10px; color:#0e376b;"><strong>Total Protection for your facility, Staff and Customers</strong></h5>--}}
                {{--</a>--}}
                {{--<div class="circle img-responsive container">--}}
                    {{--<a href=""></a>--}}
                    {{--<a href="" style="width:305px; position:relative; margin-left:-143px;" data-toggle="popover" data-content="Challenge Chemicals Australia have partnered with SEKO Australia , a global leader in pumps and dispensing systems, providing effective cost controlling and exact dilution rates."><img class="img-responsive" src="{{asset('fe-content/images/global-leading-equipment-main.png')}}" /></a>--}}
                    {{--<a href=""></a>--}}
                    {{--<a href="" style="width:288px; margin-top:-99px;" data-toggle="popover" data-content="CCA will only use certified and industry experienced electricians when installing our warewashing and laundry dispensing systems."><img class="img-responsive" src="{{asset('fe-content/images/quilified-installation.png')}}" /></a>--}}
                    {{--<a href=""></a>--}}
                    {{--<a href="" style="width:283px;" data-toggle="popover" data-content="CCA will only use certified and industry experienced electricians when installing our warewashing and laundry dispensing systems.Our technical staff will set up maintenance programs on regular service visits to ensure correct dilution rates and cost control over the entire time we supply our product ranges."><img class="img-responsive" src="{{asset('fe-content/images/maintenance.png')}}" /></a>--}}
                    {{--<a href=""></a>--}}
                    {{--<a href="" data-toggle="popover" data-content="Correct equipment and machine programing is vital to provide correct dilution rates, best wash results and to ensure continued cost control and limit wastage. Our experience technicians have years of industry experience to provide this service."><img class="img-responsive" src="{{asset('fe-content/images/equipment-machine.png')}}" /></a>--}}
                {{--</div>--}}
            {{--</nav>--}}

		{{--<p style="color:#0f386c; font-weight:bold;">For full SEKO Equipment Range visit <a style="color:#ffffff;" href="http://www.totalds.com.au" target="_blank">www.totalds.com.au</a></p>--}}
		{{--<br>--}}

        {{--</div>--}}
    {{--</div>--}}

    <div class="row" style="background-image:url('{{asset('fe-content')}}/images/eq2-bg.jpg'); background-size:100% 100%;">
        <div class="col-lg-8 text-center circular-bg">

            <nav class="circular-menu container" style="position:relative; left:-40px;">
                <a href="" class="menu-button img-responsive" style="width:150px; margin-top:5px; margin-left:-5px;"><img style="position:relative; z-index:0;" class="img-responsive" src="{{asset('fe-content')}}/images/total-cover.png" />
                    <h5 style="line-height:20px; margin-top:0px; width:110%; margin-left:-10px; color:#0e376b;"><strong>Equipment</strong></h5>
                </a>
                <div class="circle img-responsive container">
                    <a href=""></a>
                    <a href="" style="width:305px; position:relative; margin-left:-143px;" data-toggle="popover" data-content="Challenge Chemicals Australia have partnered with SEKO Australia , a global leader in pumps and dispensing systems, providing effective cost controlling and exact dilution rates."><img class="img-responsive" src="{{asset('fe-content')}}/images/global-leading-equipment-main.png" /></a>
                    <a href=""></a>
                    <a href="" style="width:288px; margin-top:-99px;" data-toggle="popover" data-content="CCA will only use certified and industry experienced electricians when installing our warewashing and laundry dispensing systems."><img class="img-responsive" src="{{asset('fe-content')}}/images/quilified-installation.png" /></a>
                    <a href=""></a>
                    <a href="" style="width:283px;" data-toggle="popover" data-content="CCA will only use certified and industry experienced electricians when installing our warewashing and laundry dispensing systems.Our technical staff will set up maintenance programs on regular service visits to ensure correct dilution rates and cost control over the entire time we supply our product ranges."><img class="img-responsive" src="{{asset('fe-content')}}/images/maintenance.png" /></a>
                    <a href=""></a>
                    <a href="" data-toggle="popover" data-content="Correct equipment and machine programing is vital to provide correct dilution rates, best wash results and to ensure continued cost control and limit wastage. Our experience technicians have years of industry experience to provide this service."><img class="img-responsive" src="{{asset('fe-content')}}/images/equipment-machine.png" /></a>
                </div>
            </nav>

            <p style="color:#0f386c; font-weight:bold;">For full SEKO Equipment Range visit <a style="color:#ffffff;" href="http://www.totalds.com.au" target="_blank">www.totalds.com.au</a></p>
            <br>
        </div>

        <style>
            @media screen and (max-width: 1200px) and (min-width: 290px) {
                .circgg1{
                    position:relative;
                    left:0px !important;
                }
            }
        </style>
        <div class="col-lg-4 text-center circular-bg circgg1" style="position:relative; left:-80px;">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div style="background-color: rgba(255, 255, 255, 0.5); padding:20px; border-radius:1px;">
                <h1 style="color:#0f386c; font-weight:bold;">EQUIPMENT</h1>
                <p style="color:#0f386c; font-weight:bold;">Total Protection for your facility, Staff and Customers</p>
                <h3 style="color:#0f386c; font-weight:bold;">Challenge Chemicals and SEKO Australia </h3>
                <br>
                <p style="width:70%; margin:0 auto; color:#0f386c;">Challenge Chemicals Australia have partnered with SEKO, Australia , a global leader in pumps and dispensing systems, providing a  TOTAL COVER solution to chemical dosing and dispensing.</p>
            </div>
            <br>
            <br>
        </div>
    </div>

@endsection

@section('page_js')
    <script src="{{ asset('fe-content/js/fnb.js') }}"></script>
@endsection