@extends('layouts.master')

@section('page_css')
    <link href="{{ asset('fe-content/css/fnb.css') }}" rel="stylesheet">
@endsection
@section('content')
    {{--<div class="row" style="background-image:url('{{asset('fe-content/images/housekeeping-bg.png')}}'); background-size:100% 100%;">--}}
        {{--<div class="col-lg-12 text-center circular-bg">--}}
            {{--<br>--}}
            {{--<h1 style="color:#0f386c;">EQUIPMENT</h1>--}}
            {{--<br>--}}
            {{--<h3 style="color:#0f386c;">Challenge Chemicals and SEKO Australia </h3>--}}
            {{--<br>--}}
            {{--<p style="width:70%; margin:0 auto; color:#0f386c;">Challenge Chemicals Australia have partnered with SEKO, Australia , a global leader in pumps and dispensing systems, providing a  TOTAL COVER solution to chemical dosing and dispensing.</p>--}}
            {{--<br>--}}
            {{--<p style="width:70%; margin:0 auto; color:#0f386c;">Our experienced technical team offer supply, installation, programming, maintenance and support to provide the safest and most cost effective method of delivering and dosing our Housekeeping, Food & Beverage  and Laundry product ranges.</p>--}}
            {{--<br>--}}
            {{--<br>--}}


            {{--<nav class="circular-menu container">--}}
                {{--<a href="" class="menu-button img-responsive" style="width:150px; margin-top:-20px;"><img style="position:relative; z-index:0;" class="img-responsive" src="{{asset('fe-content/images/total-cover.png')}}" />--}}
                    {{--<h5 style="line-height:20px; margin-top:0px; width:110%; margin-left:-10px; color:#0e376b;"><strong>Total Protection for your facility, Staff and Customers</strong></h5>--}}
                {{--</a>--}}
                {{--<div class="circle img-responsive container">--}}
                    {{--<a href="javascript:void(0)"></a>--}}
                    {{--<a href="javascript:void(0)" style="width:305px; position:relative; margin-left:-143px;"><img class="img-responsive" src="{{asset('fe-content/images/global-leading-st3.png')}}" /></a>--}}
                    {{--<a href="javascript:void(0)"></a>--}}
                    {{--<a href="javascript:void(0)" style="width:288px; margin-top:-99px;"><img class="img-responsive" src="{{asset('fe-content/images/quilified-installation-st3.png')}}" /></a>--}}
                    {{--<a href="javascript:void(0)"></a>--}}
                    {{--<a href="javascript:void(0)" style="width:283px;"><img class="img-responsive" src="{{asset('fe-content/images/maintenance-st3.png')}}" /></a>--}}
                    {{--<a href="javascript:void(0)"></a>--}}
                    {{--<a href="javascript:void(0)"><img class="img-responsive" src="{{asset('fe-content/images/equipment-and-machine-st3.png')}}" /></a>--}}
                {{--</div>--}}
            {{--</nav>--}}


        {{--</div>--}}
    {{--</div>--}}

    <div class="row" style="background-image:url('{{asset('fe-content')}}/images/housekeeping-bg225x.jpg'); background-size:100% 100%;">
        <div class="col-lg-8 text-center circular-bg">

            <nav class="circular-menu container" style="position:relative; left:-40px;">
                <a href="" class="menu-button img-responsive" style="width:150px; margin-top:-20px;"><img style="position:relative; z-index:0;" class="img-responsive" src="{{asset('fe-content')}}/images/total-cover.png" />
                    <h5 style="line-height:20px; margin-top:0px; width:110%; margin-left:-10px; color:#0e376b;"><strong>Total Protection for your facility, Staff and Customers</strong></h5>
                </a>
                <div class="circle img-responsive container">
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:305px; position:relative; margin-left:-143px;"><img class="img-responsive" src="{{asset('fe-content')}}/images/global-leading-st3.png" /></a>
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:288px; margin-top:-99px;"><img class="img-responsive" src="{{asset('fe-content')}}/images/quilified-installation-st3.png" /></a>
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:283px;"><img class="img-responsive" src="{{asset('fe-content')}}/images/maintenance-st3.png" /></a>
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)"><img class="img-responsive" src="{{asset('fe-content')}}/images/equipment-and-machine-st3.png" /></a>
                </div>
            </nav>


        </div>
        <style>
            @media screen and (max-width: 1200px) and (min-width: 290px) {
                .circgg1{
                    position:relative;
                    left:0px !important;
                }
            }
        </style>
        <div class="col-lg-4 text-center circular-bg circgg1" style="position:relative; left:-80px;">
            <br>
            <br>
            <br>
            <br>
            <div style="background-color: rgba(255, 255, 255, 0.5); padding:20px; border-radius:1px;">
                <h1 style="color:#0f386c;">EQUIPMENT</h1>
                <br>
                <h3 style="color:#0f386c;">Challenge Chemicals and SEKO Australia </h3>
                <br>
                <p style="width:70%; margin:0 auto; color:#0f386c;">Challenge Chemicals Australia have partnered with SEKO, Australia , a global leader in pumps and dispensing systems, providing a  TOTAL COVER solution to chemical dosing and dispensing.</p>
                <br>
                <p style="width:70%; margin:0 auto; color:#0f386c;">Our experienced technical team offer supply, installation, programming, maintenance and support to provide the safest and most cost effective method of delivering and dosing our Housekeeping, Food & Beverage  and Laundry product ranges.</p>
            </div>
            <br>
            <br>
        </div>
    </div>


@endsection

@section('page_js')
    <script src="{{ asset('fe-content/js/fnb.js') }}"></script>
@endsection