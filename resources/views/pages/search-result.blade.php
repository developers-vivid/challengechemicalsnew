@extends('layouts.master')

@section('content')

    <div class="row" style="margin-top:-20px !important;">
        <div class="col-lg-12 text-center exploreHeader">
            <h4>Search Result</h4>
        </div>
    </div>

    <div class="row"  style="margin-top:-20px !important; margin-bottom:100px;">
        <table class="table">
            <thead style="background-color:#e1e1e1;">
            <tr>
                <th scope="col" class="text-center" style="padding:20px;">PRODUCT NAME</th>
                <th scope="col" class="text-center" style="padding:20px;">PRODUCT DESCRIPTION</th>
                <th scope="col" class="text-center" style="padding:20px;">PACK SIZE</th>
                <th scope="col" class="text-center" style="padding:20px;">SDS</th>
            </tr>
            </thead>
            <tbody class="text-center">
            @forelse($products as $product)
                <tr>
                    <td class="text-center"><a href="{{route('productInfo.page',[$product->productRange->productSubCategory->productCategory->category_slug,$product->productRange->productSubCategory->sub_product_slug,$product->productRange->product_range_slug,$product->product_slug])}}">{{strtoupper($product->product_name)}}</a></td>
                    <td>{{$product->product_description}}</td>
                    <td>{{$product->pack_size}}</td>
                    <td><a href="{{asset('/uploads/product-sds/'.$product->sds)}}"><img src="{{asset('fe-content/images/printer.png')}}" width="20px" /></a>
                        <a href="{{asset('/uploads/product-sds/'.$product->sds)}}" download="{{$product->sds}}"><img src="{{asset('fe-content/images/pdf.png')}}" width="20px"/></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center">No item found</td>
                </tr>
            @endforelse

            </tbody>
        </table>
    </div>
@endsection