@extends('layouts.master')

@section('page_css')

@endsection
@section('content')
    <div class="row" style="background-image:url('{{asset('fe-content/images/bgfnp1.png')}}'); background-size:100% 100%; background-color: #fdec73; background-blend-mode: multiply;">

        <style>
            .indbtngg {
                background-color: #4CAF50; /* Green */
                border: none;
                color: white;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
            }

            .indbtncolor3{
                background-color: #ffffff;
                color: #000000;
                padding: 2px 50px;
                width:310px;
            }
            .indbtncolor3:hover{
                background-color:transparent;
                color: #ffffff;
                border:2px solid #ffffff;
                padding: 0px 50px;
                width:310px;
                text-decoration:none;
            }

        </style>

        <div class="col-lg-12 text-center">
            <br>
            <h1 style="margin-top:300px; margin-bottom:300px; font-weight:bold; color:#ffffff; line-height:70px; font-size:4em;">
                CHALLENGE FOOD PROCESSING
                <p style="margin-top:-20px; font-size:.5em; font-weight:normal;">Effective Solutions For Processing Plant Sanitation & Cleaning</p>
                <br>
                <br>
                <a href="{{route('productrange.page',[$food->category_slug,$subcategory->sub_product_slug])}}" class="indbtngg indbtncolor3">PRODUCTS</a>

            </h1>
            <br>
        </div>
    </div>

@endsection

@section('page_js')

@endsection