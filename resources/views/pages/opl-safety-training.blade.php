@extends('layouts.master')

@section('page_css')
    <link href="{{ asset('fe-content/css/fnb-main.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="row" style="background-image:url('{{asset('fe-content/images/opl-bg.png')}}');">
        <div class="col-lg-12 text-center circular-bg">
            <br>
            <h1 style="color:#ffffff;">SAFETY AND TRAINING</h1>
            <br>
            <p style="width:70%; margin:0 auto; color:#ffffff;">Challenge Chemicals Australia have made many advances in our safety features and digital training programs providing a safer work place and greater protection for your valued residents, guests and customers, while providing your staff with the correct cleaning procedures to control costs and gain cleaning and sanitising results.</p>
            <br>
            <br>


            <nav class="circular-menu container" style="width:55%; position:relative; left:2%;">
                <a href="" class="menu-button img-responsive" style="width:220px; margin-top:-30px;"><img style="position:relative; z-index:0; right:15px; top:20px;" class="img-responsive" src="{{asset('fe-content/images/total-cover-opl.png')}}" />
                </a>
                <div class="circle img-responsive container">
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:223px; position:absolute; z-index:1;" data-toggle="popover" data-content=""><img style="position:relative; right:-10px; bottom:-30px;" class="img-responsive" src="{{asset('fe-content/images/easy-identification-opl3.png')}}" /></a>
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:170px; position:absolute; z-index:6;" data-toggle="popover" data-content=""><img style="position:relative; right:-25px; top:-15px;" class="img-responsive" src="{{asset('fe-content/images/chemical-training-opl3.png')}}" /></a>
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:260px; position:absolute; z-index:5;" data-toggle="popover" data-content=""><img style="position:relative; bottom:-70px; right:-5px;" class="img-responsive" src="{{asset('fe-content/images/new-safety-initiatives-opl3.png')}}" /></a>
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:245px; position:absolute; z-index:4;" data-toggle="popover" data-content=""><img style="position:relative; right:30px; bottom:-78px;" class="img-responsive" src="{{asset('fe-content/images/wall-charts-opl3.png')}}" /></a>
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:170px; position:absolute; z-index:4;" data-toggle="popover" data-content=""><img style="position:relative; bottom:-25px; right:-55px;" class="img-responsive" src="{{asset('fe-content/images/experienced-opl3.png')}}" /></a>
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:235px; position:absolute; z-index:1;" data-toggle="popover" data-content=""><img style="position:relative; bottom:-30px; left:-15px;" class="img-responsive" src="{{asset('fe-content/images/global-leading-equipment-opl3.png')}}" /></a>
                </div>
            </nav>
            <br>
            <br>
            <br>
            <br>
            <br>


        </div>
    </div>

@endsection

@section('page_js')
    <script src="{{ asset('fe-content/js/fnb.js') }}"></script>
@endsection