@extends('layouts.master')

@section('page_css')
    <link href="{{ asset('fe-content/css/services.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row" style="background-image:url('{{asset('fe-content/images/fnb-bg.png')}}');">
        <div class="col-lg-12 text-center circular-bg">
            <br>
            <h1 style="color:#0f386c;">SERVICE & REPORTING</h1>
            <br>
            <p style="width:70%; margin:0 auto; color:#0f386c;">Our TOTAL COVER service package will ensure a safe working environment, a clean and sanitised facility and a hassle free supply and delivery of cleaning chemicals. Furthermore a correctly and regular serviced account will ensure costs are controlled and overall cleaning costs are kept down.</p>
            <br>
            <p style="width:70%; margin:0 auto; color:#0f386c;">Our service is backed by our comprehensive reporting which is used as a tool to keep communication open with management and staff of our clients providing detailed corrective actions and results of cleaning and sanitising tasks.</p>
            <br>

    <nav class="circular-menu2 container">
        <a href="" class="menu-button2 img-responsive" style="width:150px; margin-top:-20px;"><img style="position:relative; z-index:0;" class="img-responsive" src="{{asset('fe-content/images/total-cover.png')}}" />
            <h5 style="line-height:20px; margin-top:0px; width:110%; margin-left:-10px; color:#0e376b;"><strong>Total Protection for your facility, Staff and Customers</strong></h5>
        </a>
        <div class="circle2 img-responsive container">
            <a href=""></a>
            <a href="" data-toggle="popover" data-content="we provide regular service visits providing corrective actions on site ensuring hassle free supply and total protection of staff, facility and customers.  Emergency Service Visits available on request.">
                <img class="img-responsive" src="{{asset('fe-content/images/service-visits.png')}}" /></a>
            <a href=""></a>
            <a href="" data-toggle="popover" data-content="CCA provide all Safety Documents and Wall Charts to keep staff safe and aware of product dangers, correct product placement and correct cleaning procedures.">
                <img class="img-responsive" src="{{asset('fe-content/images/safety-documents.png')}}" style="position:relative; top:-25px; width:110px; right:-8px; height:185px;" /></a>
            <a href=""></a>
            <a href="" data-toggle="popover" data-content="CCA provide a number of different training modules with a focus on Safe Handling of Chemicals and Procedural Training, ensuring a safer work place, better results and more economical usage.">
                <img class="img-responsive" src="{{asset('fe-content/images/training.png')}}" /></a>
            <a href=""></a>
            <a href="" data-toggle="popover" data-content="we have a team of Local Technicians ready to assist quickly along with Local Management and Warehousing of products.">
                <img class="img-responsive" src="{{asset('fe-content/images/local-support.png')}}" /></a>
            <a href=""></a>
            <a href="" data-toggle="popover" data-content="on every service visit detailed reports are submitted to management highlighting cleaning results, correct machine temperatures and actions, equipment calibrations , training performed, corrective actions taken and much more.">
                <img class="img-responsive" src="{{asset('fe-content/images/detailed-reporting.png')}}" style="position:absolute; top:-10px; right:0px; width:110px;" /></a>
            <a href=""></a>
            <a href="" data-toggle="popover" data-content="all our technical staff are qualified to service and correct all our dispensing equipment on site at each service visit, ensure correct chemical dilutions, correct sanitation levels  and economical usage."><img class="img-responsive" src="{{asset('fe-content/images/equipment-maintenance.png')}}" /></a>
        </div>
    </nav>


    </div>
    </div>
@endsection

@section('page_js')
    <script src="{{ asset('fe-content/js/services.js') }}"></script>
@endsection