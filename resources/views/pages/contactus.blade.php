@extends('layouts.master')

@section('content')
    <div class="row" style="margin-top:-20px !important;">
        <div class="col-lg-12 text-center exploreHeader">
            <h1 class="exploreText">CONTACT US</h1>
        </div>
    </div>

    <div class="row" style="margin-top:0px !important;">
        <div class="col-lg-12 text-center">
            <br>
            <h1 class="exploreText">Challenge Chemicals Office, Manufacturing and Warehousing</h1>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3374.8183736031124!2d115.77939551516836!3d-32.236056581138754!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2a329b0d499d3acf%3A0x2fca9f69e3ecf853!2s6+Butcher+St%2C+Kwinana+Beach+WA+6167%2C+Australia!5e0!3m2!1sen!2sph!4v1533875907628" width="80%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
    <br>
    <br>
    <div class="row" style="margin-top:0px !important;">
        <div class="col-lg-1 text-left">
        </div>
        <div class="col-lg-10 text-left" style="margin-left:10px;">
            <h4 class=""><img src="{{asset('fe-content/images/location.png')}}" /> 6 Butcher Street, Kwinana Beach, Western Australia, 6167</h4>
            <h4 class=""><img src="{{asset('fe-content/images/phone2.png')}}" /> (08) 9419 5577</h4>
            <h4 class=""><img src="{{asset('fe-content/images/fax.png')}}" /> (08) 9419 4958</h4>
            <h4 class=""><img src="{{asset('fe-content/images/email.png')}}" /><a style="color:#333333;" href="mailto:sales@challengechemicals.com.au?Subject=Hello" target="_top"> sales@challengechemicals.com.au</a></h4>
        </div>
        <div class="col-lg-1 text-left">
        </div>
    </div>
    <br>
    <br>
    <br>
@endsection