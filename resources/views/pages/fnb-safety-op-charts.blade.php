@extends('layouts.master')

@section('page_css')

@endsection

@section('content')
    <div class="row" style="margin-top:-20px !important;">
        <div class="col-lg-12 text-center exploreHeader">
            <h1 class="exploreText">SAFETY AND OPERATIONAL CHARTS</h1>
        </div>
    </div>


    <div class="row" style="margin-top:50px !important; margin-bottom:80px !important;">
        <div class="col-sm-1">
        </div>

        <div class="col-sm-10 text-center">
            <h1>New Operator Safety Wall Charts</h1>
            <h4>Identify and cross reference Products to Cleaning Task and Safety Requirement</h4>
            <br>
            <img class="img-responsive" src="{{asset('fe-content/images/saoc-1.jpg')}}" >
            <br>
            <br>
            <h1>New Emergency First Aid Wall Charts</h1>
            <h4>A reference Chart in case of a Spill, Fire or Chemicals Emergency</h4>
            <br>
            <img class="img-responsive" src="{{asset('fe-content/images/saoc-2.jpg')}}" >
        </div>


        <div class="col-sm-1">
        </div>
    </div>
@endsection

@section('page_js')

@endsection