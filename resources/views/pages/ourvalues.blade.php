@extends('layouts.master')

@section('content')
    <div class="row" style="margin-top:-20px !important;">
        <div class="col-lg-12 text-center exploreHeader">
            <h1 class="exploreText">OUR VALUES</h1>
        </div>
    </div>


    <div class="row" style="margin-top:80px !important; margin-bottom:80px !important;">
        <div class="col-sm-1">
        </div>

        <div class="col-sm-10">
            <h4><strong>Our Values</strong></h4>
            <br>
            <h5><strong>Our Vision</strong></h5>
            <p>To be recognised by Australian industries as a leader in cleaning chemicals, valued by our customers for our technical expertise, and respected for our commitment to environmentally sound chemistry.</p>
            <br>
            <h5><strong>Our Mission</strong></h5>
            <p>Challenge Chemicals’ Mission is to deliver on its four core values with everything we do; guaranteeing high quality cleaning products our customers can rely on.</p>
            <br>
            <h5><strong>Customer Service</strong></h5>
            <p>Deliver on our promises<br>
                Take the time to know and understand every customer’s business and their unique requirements. Build long term relationships through consistent, professional and efficient customer service.</p>
            <br>
            <h5><strong>Expertise</strong></h5>
            <p>Be solutions driven<br>
                Provide technical expertise which delivers the best results for our customers. Constantly develop our knowledge and expertise by staying abreast of new product developments and technology.</p>
            <br>
            <h5><strong>Reputation</strong></h5>
            <p>Be a part of our story<br>
                The words of our customers speak the loudest – show integrity in everything we do. Share Challenge Chemicals’ proud history, and in doing so become a part of its future.</p>
            <br>
            <h5><strong>Products</strong></h5>
            <p>Never stand still<br>
                Inspire our customers through product innovation. Create environmentally sound products which meet the changing needs of industry.</p>
        </div>


        <div class="col-sm-1">
        </div>
    </div>
@endsection