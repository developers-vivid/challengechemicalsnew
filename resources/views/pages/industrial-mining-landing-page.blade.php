@extends('layouts.master')

@section('page_css')
    <link href="{{ asset('fe-content/css/fnb-main.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="row" style="background-image:url('{{asset('fe-content/images/ind-bg-1921.jpg')}}'); background-size:100% 100%;">

	<style>
	.indbtn {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
	}	
	
	.indbtncolor0{
	background-color:transparent;
    color: #ffffff;	
	border:2px solid #ffffff;
    padding: 0px 50px;
	width:310px;
	}	
	.indbtncolor0:hover{
	background-color: #ffffff;
    color: #000000;	
    padding: 0px 50px;	
	width:310px;
	text-decoration:none;
	}		
	
	.indbtncolor1{
	background-color: #ffffff;
    color: #000000;	
    padding: 2px 50px;	
	width:310px;	
	}
	.indbtncolor1:hover{
	background-color:transparent;
    color: #ffffff;	
	border:2px solid #ffffff;
    padding: 0px 50px;
	width:310px;
	text-decoration:none;	
	}	
	
	</style>

        <div class="col-lg-12 text-center">
            <br>
            <h1 style="margin-top:300px; margin-bottom:300px; font-weight:bold; color:#ffffff; line-height:70px; font-size:4em;">
                CHALLENGE TRANSPORT, <br> MINING & INDUSTRIAL
                <br>
                {{--@foreach($subcategory as $sb)--}}
                    {{--@if($sb->id=='5b960e31ec0d4')--}}
                     {{--<a href="{{route('productrange.page',[$automotive->category_slug,$sb->sub_product_slug])}}" class="indbtn indbtncolor0">PRODUCTS</a>--}}
                    {{--@endif--}}
                    {{--@if($sb->id=='5b960e4f28512')--}}
                     {{--<a href="{{route('productrange.page',[$automotive->category_slug,$sb->sub_product_slug])}}" class="indbtn indbtncolor1">CLEANSAFE DISPENSING</a>--}}
                    {{--@endif--}}
                {{--@endforeach--}}


                <a href="{{route('category.page',$automotive->category_slug)}}" class="indbtn indbtncolor0">PRODUCTS</a>
                <a href="{{route('cleansafe.page')}}" class="indbtn indbtncolor1">CLEANSAFE DISPENSING</a>

            </h1>
            <br>
        </div>
    </div>

@endsection

@section('page_js')
    <script src="{{ asset('fe-content/js/fnb.js') }}"></script>
@endsection