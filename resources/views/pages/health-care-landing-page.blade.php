@extends('layouts.master')

@section('page_css')

@endsection
@section('content')
    <div class="row" style="background-image:url('{{asset('fe-content/images/agedbg-1.png')}}');">

        <style>
            .indbtngg34 {
                background-color: #4CAF50; /* Green */
                border: none;
                color: white;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
            }

            .indbtncolor34{
                background-color: #310228;
                color: #ffffff;
                padding: 2px 50px;
                width:310px;
            }
            .indbtncolor34:hover{
                background-color:transparent;
                color: #ffffff;
                border:2px solid #310228;
                padding: 0px 50px;
                width:310px;
                text-decoration:none;
            }

            @media screen and (max-width: 600px) {
                .aged-style {
                    margin-left:0px !important;
                }
            }
            @media screen and (max-width: 500px) {
                .aged-style {
                    font-size:20px !important;
                }
            }
        </style>

        <div class="col-lg-8 text-center" style="background-image:url('{{asset('fe-content/images/agedbg-22.png')}}'); background-size:150% 100%;">
            <br>
            <h1 class="aged-style" style="margin-left:-200px; margin-top:100px; margin-bottom:300px; font-weight:bold; color:#ffffff; line-height:70px; font-size:4vw;">
                CHALLENGE AGED & <br> HEALTH CARE
                <br>
                <br>
                <a class="indbtngg34 indbtncolor34" href="{{route('category.page',$health->category_slug)}}">PRODUCTS</a>
                <br>
                <br>
            </h1>
            <br>
        </div>
        <div class="col-lg-4 text-center">
        </div>
    </div>

@endsection

@section('page_js')

@endsection