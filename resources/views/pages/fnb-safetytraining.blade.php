@extends('layouts.master')

@section('page_css')
    <link href="{{ asset('fe-content/css/safety-training.css') }}" rel="stylesheet">
@endsection

@section('content')


    <div class="row" style="background-color:#ccebfd;" >
        <div class="col-lg-12 text-center circular-bg" style="background-image:url('{{asset('fe-content/images/eg2.jpg')}}'); background-repeat: no-repeat; background-size: 100% 110%;">
            <br>
            <h1 style="color:#0f386c; font-weight:bold;">SAFETY & TRAINING</h1>
            <p style="width:70%; margin:0 auto; color:#0f386c;">Total Protection for your facility, Staff and Customers</p>
            <br>
            <nav class="circular-menu3 container">
                <a href="" class="menu-button3 img-responsive" style="width:150px; margin-top:-20px;"><img style="position:relative; z-index:0;" class="img-responsive" src="{{asset('fe-content/images/total-cover.png')}}" />
                    <h5 style="line-height:20px; margin-top:0px; width:110%; margin-left:-10px; color:#0e376b;"><strong>SAFETY & TRAINING</strong></h5>
                </a>
                <div class="circle3 img-responsive container ">
                    <a href=""></a>
                    <a href="{{route('newlabel.page',[$cat_slug,$subcat_slug])}}" style="width:230px; position:absolute; z-index:1;" ><img class="img-responsive" src="{{asset('fe-content/images/easy-identification.png')}}" /></a>
                    <a href="{{route('newlabel.page',[$cat_slug,$subcat_slug])}}"></a>
                    <a href="{{route('chemicaltrainingsafety.page',[$cat_slug,$subcat_slug])}}" style="width:230px; position:absolute; z-index:6;"><img class="img-responsive" src="{{asset('fe-content/images/chemical-safety-training.png')}}" style="position:relative; top:-15px; width:162px; right:-8px; height:225px;" /></a>
                    <a href="{{route('chemicaltrainingsafety.page',[$cat_slug,$subcat_slug])}}"></a>
                    <a href="{{route('newsafetyinitiatives.page',[$cat_slug,$subcat_slug])}}" style="width:220px; position:absolute; z-index:5;"><img class="img-responsive" src="{{asset('fe-content/images/new-safety-initiatives.png')}}" /></a>
                    <a href="{{route('newsafetyinitiatives.page',[$cat_slug,$subcat_slug])}}"></a>
                    <a href="{{route('safetyOpCharts.page',[$cat_slug,$subcat_slug])}}" style="width:225px; position:absolute; z-index:4;"><img class="img-responsive" src="{{asset('fe-content/images/wall-charts.png')}}" style="position:relative; left:10px;" /></a>
                    <a href="{{route('safetyOpCharts.page',[$cat_slug,$subcat_slug])}}"></a>
                    <a href="" style="width:230px; position:absolute; z-index:4;" data-toggle="popover" data-content="Our staff is our best asset and have the industry experience, qualifications and the support to provide your facility, staff and customers with a “Total Cover” protection."><img class="img-responsive" src="{{asset('fe-content/images/experienced.png')}}" style="position:relative; top:-5px; right:-75px; width:153px; transform: rotate(5deg);" /></a>
                    <a href=""></a>
                    <a href="" style="width:250px; position:absolute; z-index:1;" data-toggle="popover" data-content="Challenge Chemicals Australia have partnered with SEKO, Australia , a global leader in pumps and dispensing systems, providing a  TOTAL COVER solution to chemical dosing and dispensing. Our experienced technical team offer supply, installation, programming, maintenance and support to provide the safest and most cost effective method of delivering and dosing our Housekeeping, Food & Beverage  and Laundry product ranges."><img class="img-responsive" src="{{asset('fe-content/images/global-leading-equipment.png')}}" style="position:relative; left:2px; top:0px;" /></a>
                </div>
            </nav>

            <p style="width:70%; margin:0 auto; color:#0f386c; padding: 15px 10px; border-radius:4px; background-color: rgba(255, 255, 255, 0.5);">Challenge Chemicals Australia have made many advances in our safety features and digital training programs providing a safer work place and greater protection for your valued residents, guests and customers, while providing your staff with the correct cleaning procedures to control costs and gain cleaning and sanitising results.</p>
            <br>

        </div>
    </div>

@endsection

@section('page_js')
    <script src="{{ asset('fe-content/js/safety-training.js') }}"></script>
@endsection