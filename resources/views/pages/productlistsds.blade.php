@extends('layouts.master')

@section('content')

    <div class="row" style="margin-top:-20px !important;">
        <div class="col-lg-12 text-center exploreHeader">
            <h1 class="exploreText">{{strtoupper($productRange->product_range_name)}}</h1>
        </div>
    </div>

    <div class="row"  style="margin-top:-20px !important; margin-bottom:100px;">
        <table class="table">
            <thead style="background-color:#e1e1e1;">
            <tr>
                <th scope="col" class="text-center" style="padding:20px;">PRODUCT NAME</th>
                <th scope="col" class="text-center" style="padding:20px;">PRODUCT DESCRIPTION</th>
                <th scope="col" class="text-center" style="padding:20px;">PACK SIZE</th>
                <th scope="col" class="text-center" style="padding:20px;">SDS</th>
            </tr>
            </thead>
            <tbody class="text-center">
            @forelse($products as $product)
            <tr>
                <td class="text-center"><a href="{{route('productInfo.page',[$category->category_slug,$subcategory->sub_product_slug,$productRange->product_range_slug,$product->product_slug])}}">{{strtoupper($product->product_name)}}</a></td>
                <td>{{$product->product_description}}</td>
                <td>{{$product->pack_size}}</td>
                <td><a href="{{route('productinfo.pdf',['product_id'=>$product->id]) }}"  data-toggle="popover" data-content="Product info" target="_blank"><img src="{{asset('fe-content/images/printer.png')}}" width="20px" /></a>
                    <a href="{{asset('/uploads/product-sds/'.$product->sds)}}" data-toggle="popover" data-content="Product SDS" target="_blank"><img src="{{asset('fe-content/images/pdf.png')}}" width="20px"/></a>
                </td>
            </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center">No item available</td>
                </tr>
            @endforelse

            </tbody>
        </table>
    </div>
@endsection