@extends('layouts.master')

@section('page_css')
    <link href="{{ asset('fe-content/css/fnb-main.css') }}" rel="stylesheet">
@endsection
@section('content')
    {{--<div class="row" style="background-image:url('{{asset('fe-content/images/opl-bg.png')}}');">--}}
        {{--<div class="col-lg-12 text-center circular-bg">--}}
            {{--<br>--}}
            {{--<h1 style="color:#ffffff; font-weight:bold; font-size:3em;">CHALLENGE OPL</h1>--}}
            {{--<br>--}}
            {{--<p style="width:70%; margin:0 auto; color:#ffffff; font-size:20px;">Total Cover Protection</p>--}}
            {{--<br>--}}
            {{--<p style="width:70%; margin:0 auto; color:#ffffff; font-size:20px; margin-bottom:-40px;">Click onto our Total Cover Products and Services Below</p>--}}
            {{--<br>--}}
            {{--<br>--}}


            {{--<nav class="circular-menu container">--}}
                {{--<a href="" class="menu-button img-responsive" style="width:220px; margin-top:-30px;"><img style="position:relative; z-index:0;" class="img-responsive" src="{{asset('fe-content/images/total-cover-opl.png')}}" />--}}
                {{--</a>--}}
                {{--<div class="circle img-responsive container">--}}
                    {{--<a href="{{route('productrange.page',[$cat_slug,$subcat_slug])}}"></a>--}}
                    {{--<a href="{{route('safety-training.page',[$cat_slug,$subcat_slug])}}" style="position:absolute; z-index:1;" data-toggle="popover" data-content="Challenge Chemicals Australia have made many advances in our safety features and digital training programs  providing a safer work place and greater protection for your valued residents, guests and customers."><img style="position:relative; top:10px;" class="img-responsive" src="{{asset('fe-content/images/safety-and-training-opl.png')}}" /></a>--}}
                    {{--<a href="{{route('safety-training.page',[$cat_slug,$subcat_slug])}}"></a>--}}
                    {{--<a href="{{route('contactus.page')}}" style="width:200px; position:absolute; z-index:6;" data-toggle="popover" data-content="CCA provide local support with experience technicians and local management team for quick reply and response to clients requests."><img style="position:relative; right:-20px; top:-15px;" class="img-responsive" src="{{asset('fe-content/images/support-opl.png')}}"/></a>--}}
                    {{--<a href="{{route('contactus.page')}}"></a>--}}
                    {{--<a href="{{route('equipment.page',[$cat_slug,$subcat_slug])}}" style="width:310px; position:absolute; z-index:5;" data-toggle="popover" data-content="Challenge Chemicals Australia have partnered with SEKO, Australia , a global leader in pumps and dispensing systems, providing a  TOTAL COVER solution to chemical dosing and dispensing."><img style="position:relative; bottom:-50px; left:-30px;" class="img-responsive" src="{{asset('fe-content/images/equipment-opl.png')}}"  /></a>--}}
                    {{--<a href="{{route('equipment.page',[$cat_slug,$subcat_slug])}}"></a>--}}
                    {{--<a href="{{route('showServices.page',[$cat_slug,$subcat_slug])}}" style="width:190px; position:absolute; z-index:4;" data-toggle="popover" data-content="Our TOTAL COVER service package will ensure a safe working environment, a clean and sanitised facility and a hassle free supply and delivery of cleaning chemicals."><img style="position:relative; right:-60px; top:-15px;" class="img-responsive"  src="{{asset('fe-content/images/service-reporting-opl.png')}}" /></a>--}}
                    {{--<a href="{{route('showServices.page',[$cat_slug,$subcat_slug])}}"></a>--}}
                    {{--<a href="{{route('productrange.page',[$cat_slug,$subcat_slug])}}" style="width:285px; position:absolute; z-index:1;" data-toggle="popover" data-content="CCA have a huge range of products to suit user requirements, safety needs and environmental restrictions."><img style="position:relative; bottom:-10px;" class="img-responsive" src="{{asset('fe-content/images/products-opl.png')}}"/></a>--}}
                {{--</div>--}}
            {{--</nav>--}}
            {{--<br>--}}
            {{--<br>--}}


        {{--</div>--}}
    {{--</div>--}}

    <div class="row" style="background-image:url('{{asset('fe-content')}}/images/opl-bg.png'); background-size:100% 100%;">
        <div class="col-lg-8 text-center circular-bg">

            <nav class="circular-menu container" style="position:relative; left:-40px;">
                <a href="" class="menu-button img-responsive" style="width:220px; margin-top:-30px;"><img style="position:relative; z-index:0;" class="img-responsive" src="{{asset('fe-content')}}/images/total-cover-opl.png" />
                </a>
                <div class="circle img-responsive container">
                    <a href="{{route('productrange.page',[$cat_slug,$subcat_slug])}}"></a>
                    <a href="{{route('chemicaltrainingsafety.page',[$cat_slug,$subcat_slug])}}" style="position:absolute; z-index:1;" data-toggle="popover" data-content="Challenge Chemicals Australia have made many advances in our safety features and digital training programs  providing a safer work place and greater protection for your valued residents, guests and customers."><img style="position:relative; top:10px;" class="img-responsive" src="{{asset('fe-content')}}/images/safety-and-training-opl.png" /></a>
                    <a href="{{route('chemicaltrainingsafety.page',[$cat_slug,$subcat_slug])}}"></a>
                    <a href="{{route('contactus.page')}}" style="width:200px; position:absolute; z-index:6;" data-toggle="popover" data-content="CCA provide local support with experience technicians and local management team for quick reply and response to clients requests."><img style="position:relative; right:-20px; top:-15px;" class="img-responsive" src="{{asset('fe-content')}}/images/support-opl.png" /></a>
                    <a href="{{route('contactus.page')}}"></a>
                    <a href="{{route('equipment.page',[$cat_slug,$subcat_slug])}}" style="width:310px; position:absolute; z-index:5;" data-toggle="popover" data-content="Challenge Chemicals Australia have partnered with SEKO, Australia , a global leader in pumps and dispensing systems, providing a  TOTAL COVER solution to chemical dosing and dispensing."><img style="position:relative; bottom:-50px; left:-30px;" class="img-responsive" src="{{asset('fe-content')}}/images/equipment-opl.png" /></a>
                    <a href="{{route('equipment.page',[$cat_slug,$subcat_slug])}}"></a>
                    <a href="{{route('showServices.page',[$cat_slug,$subcat_slug])}}" style="width:190px; position:absolute; z-index:4;" data-toggle="popover" data-content="Our TOTAL COVER service package will ensure a safe working environment, a clean and sanitised facility and a hassle free supply and delivery of cleaning chemicals."><img style="position:relative; right:-60px; top:-15px;" class="img-responsive" src="{{asset('fe-content')}}/images/service-reporting-opl.png" /></a>
                    <a href="{{route('showServices.page',[$cat_slug,$subcat_slug])}}"></a>
                    <a href="{{route('productrange.page',[$cat_slug,$subcat_slug])}}" style="width:285px; position:absolute; z-index:1;" data-toggle="popover" data-content="CCA have a huge range of products to suit user requirements, safety needs and environmental restrictions."><img style="position:relative; bottom:-10px;" class="img-responsive" src="{{asset('fe-content')}}/images/products-opl.png" /></a>
                </div>
            </nav>
            <br>
            <br>


        </div>
        <style>
            @media screen and (max-width: 1200px) and (min-width: 290px) {
                .circgg1{
                    position:relative;
                    left:0px !important;
                }
            }
        </style>
        <div class="col-lg-4 text-center circular-bg circgg1" style="position:relative; left:-80px;">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div style="background-color: rgba(255, 255, 255, 0.5); padding:20px; border-radius:1px;">
                <h1 style="color:#006400; font-weight:bold; font-size:3em;">CHALLENGE OPL</h1>
                <br>
                <p style="width:70%; margin:0 auto; color:#006400; font-size:20px;">Total Cover Protection</p>
                <br>
                <p style="width:70%; margin:0 auto; color:#006400; font-size:20px; margin-bottom:20px;">Click unto our Total Cover Products and Services</p>
            </div>
            <br>
            <br>
        </div>
    </div>

@endsection

@section('page_js')
    <script src="{{ asset('fe-content/js/fnb.js') }}"></script>
@endsection