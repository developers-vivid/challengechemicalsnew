@extends('layouts.master')

@section('page_css')
    <link href="{{ asset('fe-content/css/services-nav.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row" style="background-image:url('{{asset('fe-content/images/fnb-bg.png')}}');">
        <div class="col-lg-12 text-center circular-bg">
            <br>
            <h1 style="color:#0f386c;">SERVICES</h1>
            <br>


            <nav class="circular-menu4 container">
                <a href="" class="menu-button4 img-responsive" style="width:150px; margin-top:-20px;"><img style="position:relative; z-index:0;" class="img-responsive" src="{{asset('fe-content/images/total-cover.png')}}" />
                    <h5 style="line-height:20px; margin-top:0px; width:110%; margin-left:-10px; color:#0e376b;"><strong>Total Protection for your facility, Staff and Customers</strong></h5>
                </a>
                <div class="circle4 img-responsive container">
                    <a href="{{route('servicesreport.page')}}"></a>
                    <a href=""><img class="img-responsive" src="{{asset('fe-content/images/wall-charts-nav.png')}}" /></a>
                    <a href=""></a>
                    <a href=""><img class="img-responsive" src="{{asset('fe-content/images/inventory-reporting.png')}}" /></a>
                    <a href=""></a>
                    <a href=""><img class="img-responsive" src="{{asset('fe-content/images/safety-training.png')}}" style="width:130px;" /></a>
                    <a href=""></a>
                    <a href=""><img class="img-responsive" src="{{asset('fe-content/images/procedural-training.png')}}" /></a>
                    <a href=""></a>
                    <a href=""><img class="img-responsive" src="{{asset('fe-content/images/emergency-call-outs.png')}}" /></a>
                    <a href=""></a>
                    <a href=""><img class="img-responsive" src="{{asset('fe-content/images/local-mgt-support.png')}}" /></a>
                    <a href=""></a>
                    <a href=""><img class="img-responsive" src="{{asset('fe-content/images/local-technical-team.png')}}" style="width:125px; position:relative; left:20px;" /></a>
                    <a href=""></a>
                    <a href=""><img class="img-responsive" src="{{asset('fe-content/images/detailed-report.png')}}" /></a>
                    <a href=""></a>
                    <a href="{{route('servicesreport.page')}}"><img class="img-responsive" src="{{asset('fe-content/images/regular-service-visit.png')}}" style="position:relative; left:-20px; bottom:-10px;" /></a>
                </div>

            </nav>


        </div>
    </div>

@endsection

@section('page_js')
    <script src="{{ asset('fe-content/js/services-nav.js') }}"></script>
@endsection