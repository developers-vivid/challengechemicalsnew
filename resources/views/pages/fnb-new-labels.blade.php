@extends('layouts.master')

@section('page_css')

@endsection

@section('content')
    <div class="row" style="margin-top:-20px !important; display:none;">
        <div class="col-lg-12 text-center exploreHeader">
            <h1 class="exploreText">SAFETY AND OPERATIONAL CHARTS</h1>
        </div>
    </div>


    <div class="row" style="background-image: linear-gradient(#ffffff, #46b957, #066e93);">
        <div class="col-sm-1">
        </div>

        <div class="col-sm-10 text-center">
            <br>
            <img class="img-responsive" src="{{asset('fe-content/images/logo2.png')}}" />
            <h1 style="color:#00679a; font-weight:bold;">Introducing Our New Labels</h1>
            <br>
            <p>Challenge Chemicals Australia introduces our 5 litre label designs for new product ranges CHALLENGE FOOD & BEVERAGE and CHALLENGE HOUSEKEEPING.</p>
            <br>
            <p>The names have not changed just our label design to meet new GHS requirements and increase and further enhance our safety commitment.</p>
            <br>
            <p>Label changes will start to take effect over the next 3 to 4 weeks. This will be the start of many enhancement programs <br> Challenge Chemicals will introduce over the next couple of months.</p>
            <br>
            <p>Our Sales Team will be in contact over the next couple of weeks to answer any questions and provide further details. <br> Alternatively feel free to call our sales line on <strong>08 9419 5577</strong></p>
            <br>
            <p><strong>Martin Cattalini</strong><br>Director</p>

            <br>
            <br>

            <div class="row">
                <div class="col-sm-6 text-center" style="padding:10px;">
                    <h3 style="color:#00679a; font-weight:bold;">CHALLENGE 5L CLEANING SOLUTIONS</h3>
                    <br>
                    <img class="img-responsive" src="{{asset('fe-content/images/ionl-2.png')}}" />
                </div>
                <div class="col-sm-6 text-center" style="padding:10px;">
                    <h3 style="color:#00679a; font-weight:bold;">CHALLENGE CLEANING STATION</h3>
                    <br>
                    <img class="img-responsive" src="{{asset('fe-content/images/ionl-1.jpg')}}" style="border: 4px solid #010a79; border-radius:15px;" />
                </div>
            </div>
            <br>
            <br>
        </div>


        <div class="col-sm-1">
        </div>
    </div>
@endsection

@section('page_js')

@endsection