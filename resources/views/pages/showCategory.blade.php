@extends('layouts.master')

@section('content')

    <div class="row" style="margin-top:-20px !important;">
        <div class="col-lg-12 text-center exploreHeader">
            <h1 class="exploreText">{{$category->page->page_name}}</h1>
        </div>
    </div>
    @if($category->id != '5b46e60724f9e')
    <div class="row">
        <div class="col-lg-12 text-center">
            <div style="padding:20px; padding-left:40px; padding-right:40px;">
                <br>
                <h1>{{$category->page->page_content}}</h1>
                <br>
            </div>
        </div>
    </div>
    @endif
    @if($category->category_video != null)
        @if($category->id != '5b46e60724f9e')
    <div class="row">
        <div class="CCvideoWrapper">

            <iframe width="560" height="315" src="{{$category->category_video}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

        </div>
    </div>
        @endif
    @else
        <div class="row text-center">
           <h2>No Video Available</h2>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-12 text-center">
            <div style="padding:20px; padding-left:40px; padding-right:40px;">
                <br>
                <h1>Explore our {{$category->category_name}} Ranges and Services Below</h1>
                <br>
            </div>
        </div>
    </div>
    {{--HOSPITALITY--}}
@if($category->id=='5b3eed45edd03')
    <div class="row"  style="margin-top:60px !important;">
        @forelse($subcategory as $sb)

            @if($sb->id =='5b46f80644fee' || $sb->id=='5b46f82dd51e0' || $sb->id=='5b46f83f6bddc'){{--hand and body--}}
            <?php
            if($sb->id=='5b46f80644fee'){//HAND AND BODY
                ?>

             <a href="{{route('productsds.page',[$category->category_slug,$sb->sub_product_slug,$productRange_slug_hand])}}">

                <?php
            }elseif($sb->id=='5b46f82dd51e0'){//CARPET, FLOOR AND GRAFFITTI
                ?>
                    <a href="{{route('productrange.page',[$category->category_slug,$sb->sub_product_slug])}}">
                <?php
            }elseif($sb->id=='5b46f83f6bddc'){//ONE SHOT
                ?>
                    <a href="{{route('productrange.page',[$category->category_slug,$sb->sub_product_slug])}}">
         <?php
            }
            ?>

                <div class="col-lg-4 text-center" style="margin-top:-20px !important; height:380px;">
                    <br>
                    <br>
                    <br>
                    <img src="{{asset('uploads/product-sub-category-image/'.$sb->sub_cat_img)}}" width="" height=""/>
                    <h3 style="color:#464646;" class="exploreText">{{strtoupper($sb->sub_cat_name)}}</h3>
                </div>
            </a>
            @else
            <a href="{{route('subcat.page',[$category->category_slug,$sb->sub_product_slug])}}">
                <div class="col-lg-4 text-center" style="margin-top:-20px !important; height:380px;">
                    <br>
                    <br>
                    <br>
                    <img src="{{asset('uploads/product-sub-category-image/'.$sb->sub_cat_img)}}" width="" height=""/>
                    <h3 style="color:#464646;" class="exploreText">{{strtoupper($sb->sub_cat_name)}}</h3>
                </div>
            </a>
            @endif
        @empty
           <h2 class="text-center">No available</h2>
        @endforelse


    </div>
    {{--HOSPITALITY END--}}

    {{--Automotive Mining and Industrial--}}
@elseif($category->id=='5b46e60724f9e')
    <div class="row"  style="margin-top:60px !important;">
        @forelse($subcategory as $sb)
            @if($sb->id=='5b9f5997c5807'){{--nidustrial-odour-control--}}

                <a href="{{route('productsds.page',[$category->category_slug,$sb->sub_product_slug,$productRange_slug_odor])}}">

            @elseif($sb->id=='5b9f59b4ec617'){{--Fleet & Truck Wash Detergents--}}

                    <a href="{{route('productsds.page',[$category->category_slug,$sb->sub_product_slug,$productRange_slug_fleet])}}">

            @elseif($sb->id=='5b9f59a542fc7'){{--Automatic Car Wash Products--}}
                    <a href="{{route('productsds.page',[$category->category_slug,$sb->sub_product_slug,$productRange_slug_carwash])}}">

            @elseif($sb->id=='5b9f59c29d493'){{--Car Detailing Products--}}
                   <a href="{{route('productsds.page',[$category->category_slug,$sb->sub_product_slug,$productRange_slug_detailing])}}">
             @else
            <a href="{{route('productrange.page',[$category->category_slug,$sb->sub_product_slug])}}">
             @endif
                <div class="col-lg-4 text-center" style="margin-top:-20px !important; height:380px;">
                    <br>
                    <br>
                    <br>
                    <img src="{{($sb->sub_cat_img == '' ) ? asset('uploads/product-sub-category-image/blank.png') : asset('uploads/product-sub-category-image/'.$sb->sub_cat_img)}}" width="" height=""/>
                    <h3 style="color:#464646;" class="exploreText">{{strtoupper($sb->sub_cat_name)}}</h3>
                </div>
            </a>
        @empty
                        <h2 class="text-center">No available</h2>
        @endforelse

    </div>
    {{--HOSPITALITY END--}}

@else
    <div class="row"  style="margin-top:60px !important;">
        @forelse($subcategory as $sb)
                        <a href="{{route('productrange.page',[$category->category_slug,$sb->sub_product_slug])}}">
                            <div class="col-lg-4 text-center" style="margin-top:-20px !important; height:380px;">
                                <br>
                                <br>
                                <br>
                                <img src="{{($sb->sub_cat_img == '' ) ? asset('uploads/product-sub-category-image/blank.png') : asset('uploads/product-sub-category-image/'.$sb->sub_cat_img)}}" width="" height=""/>
                                <h3 style="color:#464646;" class="exploreText">{{strtoupper($sb->sub_cat_name)}}</h3>
                            </div>
                        </a>
                    @empty
                        <h2 class="text-center">No available</h2>
            @endforelse
    </div>
@endif
@endsection