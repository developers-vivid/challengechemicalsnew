@extends('layouts.master')

@section('page_css')

@endsection
@section('content')
    <div class="row">

        <div class="col-lg-12 text-center">
            <br>
            <br>
            <h1 style="font-weight:bold;">Explore our Aged & Health Care Ranges Below</h1>
            <br>
            <br>
        </div>
    </div>

    <div class="row">
        <style>
            .agChange1 {
                background: url("{{asset('fe-content/images/ag1.png')}}") !important;
                width:300px;
                height:400px;
                transition: .7s;
            }
            .agChange1:hover {
                background: url("{{asset('fe-content/images/ag1-hover.png')}}") !important;
            }
            .agChange2 {
                background: url("{{asset('fe-content/images/ag2.png')}}") !important;
                width:300px;
                height:400px;
                transition: .7s;
            }
            .agChange2:hover {
                background: url("{{asset('fe-content/images/ag2-hover.png')}}") !important;
            }
            .agChange3 {
                background: url("{{asset('fe-content/images/ag3.png')}}") !important;
                width:300px;
                height:400px;
                transition: .7s;
            }
            .agChange3:hover {
                background: url("{{asset('fe-content/images/ag3-hover.png')}}") !important;
            }
            .agChange4 {
                background: url("{{asset('fe-content/images/ag4.png')}}") !important;
                width:300px;
                height:400px;
                transition: .7s;
            }
            .agChange4:hover {
                background: url("{{asset('fe-content/images/ag4-hover.png')}}") !important;
            }
        </style>
        <a href="{{route('productrange.page',[$hospitality->category_slug,$housekeeping->sub_product_slug])}}">
        <div class="col-lg-3 text-center">
            <br>
            <div class="img-responsive agChange1">
            </div>
            <h4>Facility Cleaning and Sanitising</h4>
            <br>
        </div>
        </a>

        <a href="{{route('productrange.page',[$hospitality->category_slug,$foodandbeverage->sub_product_slug])}}">
        <div class="col-lg-3 text-center">
            <br>
            <div class="img-responsive agChange2">
            </div>
            <h4>Warewashing and Kitchen Products</h4>
            <br>
        </div>
        </a>
        <a href="{{route('productrange.page',[$hospitality->category_slug,$opl->sub_product_slug])}}">
        <div class="col-lg-3 text-center">
            <br>
            <div class="img-responsive agChange3">
            </div>
            <h4>Laundry</h4>
            <br>
        </div>
        </a>

        <a href="{{route('productsds.page',[$hospitality->category_slug,$handBody->sub_product_slug,$handBodyRange->product_range_slug])}}">
        <div class="col-lg-3 text-center">
            <br>
            <div class="img-responsive agChange4">
            </div>
            <h4>Hand and Body</h4>
            <br>
        </div>
        </a>
    </div>

    <br>
    <br>


@endsection

@section('page_js')

@endsection