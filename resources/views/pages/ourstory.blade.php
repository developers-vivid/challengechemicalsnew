@extends('layouts.master')
@section('page_css')
    <link href="{{asset('fe-content/css/timeline.css')}}" rel="stylesheet">
    <link href="{{asset('fe-content/css/our-story-animation.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="row" style="margin-top:-20px !important;">
        <div class="col-lg-12 text-center exploreHeader">
            <h1 class="exploreText">OUR STORY</h1>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <br>
                <h4><strong>Challenge Chemicals Australia – Over 40 years Experience.</strong></h4>
                <br>
                <p>Challenge Chemicals Australia (CCA) is a leading West Australian based chemical manufacturer, with experience in manufacturing dating back to 1975. That over 40 years in manufacturing experience. Starting off with a handful of employees we have grown and developed our business to where we are now a major supplier to the mining, transport, marine and general industrial sectors along with the hospitality, healthcare, textile, food processing and water treatment industries. We also manufacture specialty chemicals sold under brand names suppling products all over Australia and overseas.</p>
                <br>
                <p>Our company was founded on the mantra of “Service with Products of Technology” and today this is
                    still the corner stone of our company’s philosophy. Our focus is on developing and testing products
                    and equipment while providing our customers with outstanding service and safety.</p>
                <br>
                <div class="col-lg-12 text-center">
                    <br><br>
                    <h1 style="color:#3ab54a;">OUR STORY – More Than A Challenge!!</h1>
                    <br><br>
                </div>
                    <h2 class="text-center"><div class="timeline-badge primary" ><a><i class="dot-timeline invert" id=""></i></a></div></h2>

                <ul class="timeline">

                    <li>
                        <div class="timeline-panel storyRotateLeft">
                            <div class="timeline-body">
                                <img class="img-responsive storyImg" src="{{asset('fe-content/images/story1.png')}}" />
                                <div style="background-color:#86eda3;" class="text-right storyLeft">
                                    <strong class="storyLeftStrong">1975</strong>
                                </div>
                                <p class="text-right">FIRST MANUFACTURING PLANT Started our first chemical manufacturing plant in Yeates Road Kwinana Beach – Manufacturing Allum Sulphate for the Water Corp</p>
                            </div>
                        </div>
                    </li>

                    <li  class="timeline-inverted storyRotateRight">
                        <div class="timeline-panel">
                            <div class="timeline-body">
                                <img class="img-responsive storyImgRight" src="{{asset('fe-content/images/story2.png')}}" width="325px" />
                                <div style="background-color:#86daed;" class="text-left storyRight">
                                    <strong class="storyRightStrong">1980</strong>
                                </div>
                                <p class="text-left">PROCESS CHEMICALS FORMED Specilising in piping services and rental of equipment for
                                    the growing needs of Oil and Gas, Mining and Power Generation Industries. Key services include Pressure Testing,
                                    Pipeline Services, Chemical Cleaning, Hot Oil and Fluid Flushing and other associated  services for piping systems,
                                    vessels and  installation </p>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="timeline-panel storyRotateLeft">
                            <div class="timeline-body">
                                <img class="img-responsive storyImg" src="{{asset('fe-content/images/story3.png')}}" />
                                <div style="background-color:#86daed;" class="text-right storyLeft">
                                    <strong class="storyLeftStrong">1982</strong>
                                </div>
                                <p class="text-right">BUTCHER ST<br>
                                    Purchased Butcher St housing Process<br>
                                    Chemicals and soon to be Challenge
                                    Chemicals.</p>
                            </div>
                        </div>
                    </li>

                    <li  class="timeline-inverted storyRotateRight">
                        <div class="timeline-panel">
                            <div class="timeline-body">
                                <img class="img-responsive storyImgRight" src="{{asset('fe-content/images/story4.png')}}" />
                                <div style="background-color:#edb686;" class="text-left storyRight">
                                    <strong class="storyRightStrong">1985</strong>
                                </div>
                                <p class="text-left">FOUNDED NEWLAND CHEMICALS
                                    Started Newland Chemicals In Western Australia manufacturing a range of commercial grade hospitality cleaning products</p>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="timeline-panel storyRotateLeft">
                            <div class="timeline-body">
                                <img class="img-responsive storyImg" src="{{asset('fe-content/images/story5.png')}}" />
                                <div style="background-color:#edb686;" class="text-right storyLeft">
                                    <strong class="storyLeftStrong">1988</strong>
                                </div>
                                <p class="text-right">NAME CHANGE to CHALLENGE CHEMICALS AUSTRALIA
                                    Changed names from Newland Chemicals to Challenge Chemicals Australia.</p>
                            </div>
                        </div>
                    </li>

                    <li  class="timeline-inverted">
                        <div class="timeline-panel">
                            <div class="timeline-body storyRotateRight">
                                <img class="img-responsive storyImgRight" src="{{asset('fe-content/images/story6.png')}}" />
                                <div style="background-color:#edb686;" class="text-left storyRight">
                                    <strong class="storyRightStrong">1990</strong>
                                </div>
                                <p class="text-left">REGINAL DISTRUBUTORS
                                    Introduction and development of Western Australian regional distributors selling our Challenge Chemicals range of cleaning
                                    products.
                                </p>
                            </div>

                            <div class="timeline-body storyRotateRight">
                                <img class="img-responsive storyImgRight" src="{{asset('fe-content/images/story7.png')}}" />
                                <div style="background-color:#86eda3;" class="text-left storyRight">
                                    <strong class="storyRightStrong">1990</strong>
                                </div>
                                <p class="text-left">FORMATION OF IRON CHEMICALS
                                    Redesigned and rebuilt Yeates rd To
                                    house Iron Chemicals – manufacturing
                                    Ferric Sulphate and Ferric Chloride for
                                    water treatment.
                                </p>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="timeline-panel">
                            <div class="timeline-body storyRotateLeft">
                                <img class="img-responsive storyImg" src="{{asset('fe-content/images/story8.png')}}" />
                                <div style="background-color:#86daed;" class="text-right storyLeft">
                                    <strong class="storyLeftStrong">1995</strong>
                                </div>
                                <p class="text-right">NEW LOCATION FOR
                                    PROCESS CHEMICALS
                                    Challenge Chemicals sole resident of
                                    Butcher St.
                                    <br>
                                    <br>
                                    PROCESS CHEMICALS
                                    Process continue to grow and support
                                    most of our countrys biggest oil and gas
                                    contrustion operations. Now based
                                    at Head Office in Henderson.</p>
                            </div>

                            <div class="timeline-body storyRotateLeft">
                                <img class="img-responsive storyImg" src="{{asset('fe-content/images/story9.png')}}" />
                                <div style="background-color:#edb686;" class="text-right storyLeft">
                                    <strong class="storyLeftStrong">1995</strong>
                                </div>
                                <p class="text-right">INTRODUCTION of NEW MINING
                                    & INDUSTRIAL RANGE Challenge Chemicals formulated a new range of Mining and
                                    Industrial products to meet the increasing demand from the Mining Industry</p>
                            </div>

                            <div class="timeline-body storyRotateLeft">
                                <img class="img-responsive storyImg" src="{{asset('fe-content/images/story10.png')}}" />
                                <div style="background-color:#edb686;" class="text-right storyLeft">
                                    <strong class="storyLeftStrong">1995</strong>
                                </div>
                                <p class="text-right">FIRST MINING CONTRACT
                                    Western Coalaries (Now Premier Coal)
                                    start using our Mining Product Range -
                                    still a valued customers today.
                                    Many more to follow.</p>
                            </div>
                        </div>
                    </li>

                    <li  class="timeline-inverted storyRotateRight">
                        <div class="timeline-panel">
                            <div class="timeline-body">
                                <img class="img-responsive storyImgRight" src="{{asset('fe-content/images/story11.png')}}" />
                                <div style="background-color:#edb686;" class="text-left storyRight">
                                    <strong class="storyRightStrong">1996</strong>
                                </div>
                                <p class="text-left">FIRST NATION ACCOUNT<br>
                                    Challenge goes Australia wide supplying Red Rooster with cleaning and sanitising products
                                </p>
                            </div>
                        </div>
                    </li>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>

                </ul>
                <img class="img-responsive storyCenterImgLeft" width="150px" src="{{asset('fe-content/images/story12left.png')}}" />
                <img class="img-responsive storyCenterImgRight" width="150px" src="{{asset('fe-content/images/story12right.png')}}">
                <div class="storyCenter text-center">
                    <strong>1996 - 2000</strong>
                    <p style="position:relative; top:-70px; font-size:18px;">HUGE GROWTH and DEVELOPMENT<br>
                        Challenge Chemicals Australia grows into a leading<br>
                        Australian chemical manufacturer with distribution growing through out Australia
                    </p>
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <ul class="timeline">

                    <li>
                        <div class="timeline-panel storyRotateLeft">
                            <div class="timeline-body">
                                <img class="img-responsive storyImg" src="{{asset('fe-content/images/story13.png')}}" />
                                <div style="background-color:#86eda3;" class="text-right storyLeft">
                                    <strong class="storyLeftStrong">2008</strong>
                                </div>
                                <p class="text-right">IRON CHEMICALS SOLD<br>
                                    Iron Chemicals sold to Orica Australia</p>
                            </div>
                        </div>
                    </li>

                    <li  class="timeline-inverted storyRotateRight">
                        <div class="timeline-panel">
                            <div class="timeline-body">
                                <img class="img-responsive storyImgRight" src="{{asset('fe-content/images/story14.png')}}" />
                                <div style="background-color:#edb686;" class="text-left storyRight">
                                    <strong class="storyRightStrong">2010</strong>
                                </div>
                                <p class="text-left">NEW GENERATION<br>
                                    Retirement of Bill Cromedy and Peter
                                    Cattalini brings in a second generation of
                                    family members</p>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="timeline-panel storyRotateLeft">
                            <div class="timeline-body">
                                <img class="img-responsive storyImg" src="{{asset('fe-content/images/story15.png')}}" />
                                <div style="background-color:#edb686;" class="text-right storyLeft">
                                    <strong class="storyLeftStrong">2014</strong>
                                </div>
                                <p class="text-right">CHALLENGE CHEMICALS
                                    GOES INTERNATIONAL
                                    Our first distributor opens in The Maldives servicing the growing tourism industry.
                                    Further international distribution follows.</p>
                            </div>
                        </div>
                    </li>

                    <li  class="timeline-inverted storyRotateRight">
                        <div class="timeline-panel">
                            <div class="timeline-body">
                                <img class="img-responsive storyImgRight" src="{{asset('fe-content/images/story16.png')}}" />
                                <div style="background-color:#edb686;" class="text-left storyRight">
                                    <strong class="storyRightStrong textPresent" style="font-size:70px;">PRESENT DAY</strong>
                                </div>
                                <p class="text-left">Challenge Chemicals Australia continue to grow, with a focus on product development and superior service and support for our valued clients.</p>
                            </div>
                        </div>
                    </li>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </ul>

                <h2 class="text-center"><div class="timeline-badge primary" ><a><i class="dot-timeline invert" id=""></i></a></div></h2>
            </div>
        </div>
    </div>
@endsection
@section('page_js')

@endsection