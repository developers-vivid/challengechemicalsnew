@extends('layouts.master')

@section('page_css')

@endsection

@section('content')
    <div class="row" style="margin-top:-20px !important;">
        <div class="col-lg-12 text-center exploreHeader">
            <h1 class="exploreText">TRAINING & SAFETY</h1>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-1">
        </div>
        <div class="col-lg-10" style="margin-left:15px;">
            <br>
            <br>
            <br>
            <p><strong>Challenge Chemicals Australia provides educational training programs formatted to easily follow and understand.<br>
                    Training programs include group training sessions and digital refresher documents.</strong></p>
            <br>
            <h4>Contact our Sales Team to access Training Modules:</h4>
            <br>
        </div>
        <div class="col-lg-1">
        </div>
    </div>


    <div class="row">
        <div class="col-lg-1">
        </div>
        <div class="col-lg-10">
            <br>
            <div class="col-lg-6">
                    <h5><strong>KITCHEN SAFETY & PROCEDURE</strong></h5>
                    <img class="img-responsive zoom" src="{{asset('fe-content/images/1-1c.jpg')}}" />
            </div>
            <div class="col-lg-6">
                    <h5><strong>CHEMICAL HANDLING & SAFETY</strong></h5>
                    <img class="img-responsive zoom" src="{{asset('fe-content/images/challenge-chemica-handling-99x.jpg')}}" />
            </div>
        </div>
        <div class="col-lg-1">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1">
        </div>
        <div class="col-lg-10">
            <br>
            <div class="col-lg-6">
                    <h5><strong>HOUSEKEEPING SAFETY & PROCEDURE</strong></h5>
                    <img class="img-responsive zoom" src="{{asset('fe-content/images/3.jpg')}}" />
            </div>
            <div class="col-lg-6">
                    <h5><strong>LAUNDRY SAFETY & PROCEDURE</strong></h5>
                    <img class="img-responsive zoom" src="{{asset('fe-content/images/4.jpg')}}" />
            </div>
        </div>
        <div class="col-lg-1">
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-lg-1">
        </div>
        <div class="col-lg-10" style="margin-left:15px;">
            <br>
            <h5>On completion of educational modules, your staff will be compliant in;</h5>
            <br>
            <ul>
                <li>Safe Handling of Chemicals</li>
                <li>Cleaning Procedures focussing on best results at lowest costs</li>
                <li>Reduced risk of infection toward residents, guests and staff</li>
                <li>Factors that effect cleaning.</li>
                <li>Hazard identification</li>
            </ul>
            <br>
        </div>
        <div class="col-lg-1">
        </div>
    </div>

  <br>
  
  <div class="row">
        <div class="col-lg-1">
        </div>  
        <div class="col-lg-10" style="margin-left:15px;">
    <h3 style="font-weight:bold;">New Safety Inititives</h3>
    <br>
    <br>    
    <br>    
    <p>New Housekeeping and Food & Beverage 5 Litre Colour and Number Coded Labels</p>
    <img class="img-responsive" style="margin-top:-70px;" src="{{asset('fe-content/images/product-label-desc-99x.jpg')}}" />
    <br>
    <h3 style="font-weight:bold;">New Challenge Chemicals installation and safety initiatives for a safer work place.</h3>    
    <br>
    <h4 style="font-weight:bold;">INSTALLATION & SAFETY INITIATIVES</h4>
    <img class="img-responsive" src="{{asset('fe-content/images/ionl-3.jpg')}}" />      
    <br>
        </div>  
        <div class="col-lg-1">
        </div>  
  </div>  
  
    <br>
@endsection

@section('page_js')

@endsection