@extends('layouts.master')

@section('content')
    <div class="row" style="margin-top:-20px !important;">
        <div class="col-lg-12 text-center exploreHeader">
            <h1 class="exploreText">NEWS</h1>
        </div>
    </div>

    <div class="row" style="text-align:center; width:80%; margin:0 auto;">
        <div class="col-sm-4">

            <div style="width:300px; margin-top:50px; background-color:#e7f8f8; text-align:left;">
                <img class="img-responsive" src="{{asset('fe-content')}}/images/news.png" />
                <div style="padding:20px;">
                    <h5><strong>MEDIA RELEASE – CCA LAUNCH MALDIVES 9 May 2014</strong></h5>
                    <br>
                    <p>In April 2014, Challenge Chemicals Australia launched their extensive product range to assist with all cleaning and maintenance requirements within the resorts and commercial businesses of the Maldives at the Traders Hotel Male’.</p>
                    <a href="#">Read More</a>
                </div>
            </div>

        </div>
        <div class="col-sm-4">

            <div style="width:300px; margin-top:50px; background-color:#e7f8f8; text-align:left;">
                <img class="img-responsive" src="{{asset('fe-content')}}/images/news.png" />
                <div style="padding:20px;">
                    <h5><strong>New Product: White Hot! Clean, Shine and Protect 17 Aug 2017</strong></h5>
                    <br>
                    <p>Challenge Chemicals have formulated WHITE HOT to be a concentrated heavy duty remover of mineral salts and staining. It will brighten and clean all metals and oxidised painted surfaces.</p>
                    <a href="#">Read More</a>
                </div>
            </div>

        </div>
        <div class="col-sm-4">

            <div style="width:300px; margin-top:50px; background-color:#e7f8f8; text-align:left;">
                <img class="img-responsive" src="{{asset('fe-content')}}/images/news.png" />
                <div style="padding:20px;">
                    <h5><strong>MEDIA RELEASE – CCA LAUNCH MALDIVES 9 May 2014</strong></h5>
                    <br>
                    <p>In April 2014, Challenge Chemicals Australia launched their extensive product range to assist with all cleaning and maintenance requirements within the resorts and commercial businesses of the Maldives at the Traders Hotel Male’.</p>
                    <a href="#">Read More</a>
                </div>
            </div>

        </div>
    </div>


    <div class="row" style="margin-top:80px !important; margin-bottom:80px !important;">
        <div class="col-sm-2">
        </div>

        <div class="col-sm-8">
            <h4><strong>Archive</strong></h4>

            <div>
                <h5 data-toggle="collapse" data-target="#demo" style="background-color:#00679a; padding:5px; color:#ffffff; cursor:pointer;">2017</h5>
                <div id="demo" class="collapse" style="background-color:#d8dbdd; margin-top:-10px; padding:5px;">
                    -August
                </div>
            </div>

            <div style="margin-top:-10px;">
                <h5 data-toggle="collapse" data-target="#demo2" style="background-color:#95c4db; padding:5px; color:#ffffff; cursor:pointer;">2016</h5>
                <div id="demo2" class="collapse" style="background-color:#d8dbdd; margin-top:-10px; padding:5px;">
                    -August
                </div>
            </div>

            <div style="margin-top:-10px;">
                <h5 data-toggle="collapse" data-target="#demo3" style="background-color:#00679a; padding:5px; color:#ffffff; cursor:pointer;">2015</h5>
                <div id="demo3" class="collapse" style="background-color:#d8dbdd; margin-top:-10px; padding:5px;">
                    -August
                </div>
            </div>

            <div style="margin-top:-10px;">
                <h5 data-toggle="collapse" data-target="#demo4" style="background-color:#95c4db; padding:5px; color:#ffffff; cursor:pointer;">2014</h5>
                <div id="demo4" class="collapse" style="background-color:#d8dbdd; margin-top:-10px; padding:5px;">
                    -May
                </div>
            </div>

        </div>


        <div class="col-sm-2">
        </div>
    </div>

@endsection