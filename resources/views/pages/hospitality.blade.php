@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12 text-center">
            <div style="padding:20px; padding-left:40px; padding-right:40px;">
                <br>
                <br>
                <h1>Hospitality Products</h1>
                <br>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diem nonummy
                    nibh euismod tincidunt ut lacreet dolore magna aliguam erat volutpat.
                    Ut wisis enim ad minim veniam, quis nostrud exerci tution ullam corper suscipit
                    lobortis nisi ut aliquip ex ea commodo consequat. Duis te feugi facilisi.
                    Duis autem dolor in hendrerit in vulputate velit esse molestie consequat, </p>
                <br>
                <br>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 text-center">
            <img class="img-responsive" src="{{asset('fe-content/images/hosp-video.png')}}" />
        </div>
    </div>

    <div class="row"  style="margin-top:60px !important;">
        <a href="#">
            <div class="col-lg-4 text-center" style="margin-top:-20px !important; height:380px;">
                <br>
                <br>
                <br>
                <img src="{{asset('fe-content/images/food.png')}}" width="" height=""/>
                <h3 style="color:#464646;" class="exploreText">FOOD AND BEVERAGES</h3>
            </div>
        </a>
        <a href="#">
            <div class="col-lg-4 text-center" style="margin-top:-20px !important; height:380px;">
                <br>
                <br>
                <br>
                <img src="{{asset('fe-content/images/housekeeping.png')}}" width="160px" height="" />
                <h3 style="color:#464646;" class="exploreText">HOUSEKEEPING</h3>
            </div>
        </a>
        <a href="#">
            <div class="col-lg-4 text-center" style="margin-top:-20px !important; height:380px;">
                <br>
                <br>
                <br>
                <img src="{{asset('fe-content/images/laundry.png')}}" width="145px" height=""/>
                <h3 style="color:#464646;" class="exploreText">OPL(Laundry)</h3>
            </div>
        </a>
    </div>

    <div class="row"  style="margin-top:0px !important;">
        <a href="#">
            <div class="col-lg-4 text-center" style="margin-top:-20px !important; height:380px;">
                <br>
                <br>
                <br>
                <img src="{{asset('fe-content/images/hand.png')}}" width="195px" height=""/>
                <h3 style="color:#464646;" class="exploreText">HAND AND BODY</h3>
            </div>
        </a>
        <a href="#">
            <div class="col-lg-4 text-center" style="margin-top:-20px !important; height:380px;">
                <br>
                <br>
                <br>
                <img src="{{asset('fe-content/images/carpet.png')}}" width="160px" height="" />
                <h3 style="color:#464646;" class="exploreText">CARPET,FLOOR AND GRAFFITTI</h3>
            </div>
        </a>
        <a href="#">
            <div class="col-lg-4 text-center" style="margin-top:-20px !important; height:380px;">
                <br>
                <br>
                <br>
                <img src="{{asset('fe-content/images/one-shot-soon.png')}}" width="235px" height=""/>
                <h3 style="color:#464646;" class="exploreText">ONE SHOT</h3>
            </div>
        </a>
    </div>

@endsection