@extends('layouts.master')

@section('page_css')
    <link href="{{ asset('fe-content/css/fnb-main.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="row" style="background-image:url('{{asset('fe-content/images/opl-bg.png')}}')">
        <div class="col-lg-12 text-center circular-bg">
            <br>
            <h1 style="color:#ffffff;">SERVICE & REPORTING</h1>
            <br>
            <p style="width:70%; margin:0 auto; color:#ffffff;">Our TOTAL COVER service package will ensure a safe working environment, a clean and sanitised facility and a hassle free supply and delivery of cleaning chemicals. Furthermore a correctly and regular serviced account will ensure costs are controlled and overall cleaning costs are kept down.</p>
            <br>
            <p style="width:70%; margin:0 auto; color:#ffffff;">Our service is backed by our comprehensive reporting which is used as a tool to keep communication open with management and staff of our clients providing detailed corrective actions and results of cleaning and sanitising tasks.</p>
            <br>
            <br>


            <nav class="circular-menu container" style="width:55%; position:relative; left:2%;">
                <a href="" class="menu-button img-responsive" style="width:220px; margin-top:-30px;"><img style="position:relative; z-index:0; right:15px; top:20px;" class="img-responsive" src="{{asset('fe-content/images/total-cover-opl.png')}}" />
                </a>
                <div class="circle img-responsive container">
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:213px; position:absolute; z-index:1;" data-toggle="popover" data-content="we provide regular service visits providing corrective actions on site ensuring hassle free supply and total protection of staff, facility and customers.  Emergency Service Visits available on request."><img style="position:relative; right:-2px; bottom:-30px;" class="img-responsive" src="{{asset('fe-content/images/service-visit-opl5.png')}}" /></a>
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:145px; position:absolute; z-index:6;" data-toggle="popover" data-content="CCA provide all Safety Documents and Wall Charts to keep staff safe and aware of product dangers, correct product placement and correct cleaning procedures."><img style="position:relative; right:-25px; top:-5px;" class="img-responsive" src="{{asset('fe-content/images/safety-docs-opl5.png')}}" /></a>
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:235px; position:absolute; z-index:5;" data-toggle="popover" data-content="CCA provide a number of different training modules with a focus on Safe Handling of Chemicals and Procedural Training, ensuring a safer work place, better results and more economical usage."><img style="position:relative; bottom:-70px; right:-5px;" class="img-responsive" src="{{asset('fe-content/images/traning-opl5.png')}}" /></a>
                    <a href="ajavascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:240px; position:absolute; z-index:4;" data-toggle="popover" data-content="we have a team of Local Technicians ready to assist quickly along with Local Management and Warehousing of products."><img style="position:relative; right:20px; bottom:-74px;" class="img-responsive" src="{{asset('fe-content/images/local-support-opl5.png')}}" /></a>
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:150px; position:absolute; z-index:5;" data-toggle="popover" data-content="on every service visit detailed reports are submitted to management highlighting cleaning results, correct machine temperatures and actions, equipment calibrations , training performed, corrective actions taken and much more."><img style="position:relative; bottom:-35px; right:-55px;" class="img-responsive" src="{{asset('fe-content/images/detailed-reporting-opl5.png')}}" /></a>
                    <a href="javascript:void(0)"></a>
                    <a href="javascript:void(0)" style="width:235px; position:absolute; z-index:1;" data-toggle="popover" data-content="all our technical staff are qualified to service and correct all our dispensing equipment on site at each service visit, ensure correct chemical dilutions, correct sanitation levels  and economical usage."><img style="position:relative; bottom:-30px; left:-15px;" class="img-responsive" src="{{asset('fe-content/images/equipment-and-training-opl5.png')}}" /></a>
                </div>
            </nav>
            <br>
            <br>
            <br>
            <br>
            <br>


        </div>
    </div>
@endsection

@section('page_js')
    <script src="{{ asset('fe-content/js/fnb.js') }}"></script>
@endsection