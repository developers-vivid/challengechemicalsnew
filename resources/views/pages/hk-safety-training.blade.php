@extends('layouts.master')

@section('page_css')
    <link href="{{ asset('fe-content/css/fnb-main.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="row" style="background-image:url('{{asset('fe-content/images/housekeeping-bg.png')}}'); background-size:100% 100%;">
        <div class="col-lg-12 text-center circular-bg">
            <br>
            <h1 style="color:#0f386c;">SAFETY AND TRAINING</h1>
            <br>
            <p style="width:70%; margin:0 auto; color:#0f386c;">Challenge Chemicals Australia have made many advances in our safety features and digital training programs providing a safer work place and greater protection for your valued residents, guests and customers, while providing your staff with the correct cleaning procedures to control costs and gain cleaning and sanitising results.</p>
            <br>
            <br>


            <nav class="circular-menu container" style="width:55%; position:relative; left:2%;">
                <a href="" class="menu-button img-responsive" style="width:220px; margin-top:-30px;"><img style="position:relative; z-index:0; right:15px; top:20px;" class="img-responsive" src="{{asset('fe-content/images/total-cover-logo.png')}}" />
                </a>
                <div class="circle img-responsive container">
                    <a href="qwerty.com"></a>
                    <a href="google.com" style="width:223px; position:absolute; z-index:1;" data-toggle="popover" data-content=""><img style="position:relative; right:-10px; bottom:-30px;" class="img-responsive" src="{{asset('fe-content/images/easy-identification-st.png')}}" /></a>
                    <a href="google.com"></a>
                    <a href="pokemon.com" style="width:170px; position:absolute; z-index:6;" data-toggle="popover" data-content=""><img style="position:relative; right:-25px; top:-15px;" class="img-responsive" src="{{asset('fe-content/images/chemical-training-safety-st.png')}}" /></a>
                    <a href="pokemon.com"></a>
                    <a href="abc.com" style="width:260px; position:absolute; z-index:5;" data-toggle="popover" data-content=""><img style="position:relative; bottom:-70px; right:-5px;" class="img-responsive" src="{{asset('fe-content/images/new-safety-initiatives-st.png')}}" /></a>
                    <a href="abc.com"></a>
                    <a href="123.com" style="width:245px; position:absolute; z-index:4;" data-toggle="popover" data-content=""><img style="position:relative; right:30px; bottom:-78px;" class="img-responsive" src="{{asset('fe-content/images/wall-charts-st.png')}}" /></a>
                    <a href="123.com"></a>
                    <a href="ll.com" style="width:170px; position:absolute; z-index:1;" data-toggle="popover" data-content=""><img style="position:relative; bottom:-25px; right:-55px;" class="img-responsive" src="{{asset('fe-content/images/experienced-st.png')}}" /></a>
                    <a href="ll.com"></a>
                    <a href="qwerty.com" style="width:235px; position:absolute; z-index:1;" data-toggle="popover" data-content=""><img style="position:relative; bottom:-30px; left:-15px;" class="img-responsive" src="{{asset('fe-content/images/global-st.png')}}" /></a>
                </div>
            </nav>
            <br>
            <br>
            <br>
            <br>
            <br>


        </div>
    </div>

@endsection

@section('page_js')
    <script src="{{ asset('fe-content/js/fnb.js') }}"></script>
@endsection