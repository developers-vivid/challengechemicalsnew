@extends('layouts.master')

@section('content')

    <div class="row" style="margin-top:-20px !important;">
        <div class="col-lg-12 text-center exploreHeader">
            <h1 class="exploreText" style="margin-top:5px;">{{strtoupper($product->product_name)}}</h1>
            <h3 class="exploreText" style="margin-top:-45px; margin-bottom:-20px;">{{strtoupper($product->product_description)}}</h3>
        </div>
    </div>

    <div class="row" style="margin-top:20px !important; margin-bottom:80px !important;">
        <div class="col-sm-8">
            @if($product->product_details ==null)
                <p>No Product info</p>
            @else
                {!! $product->product_details !!}
            @endif
        </div>

        <div class="col-sm-4 text-center">
            <div style="margin-left:35%; text-align:left;">
                @if($product->is_biodegrable=='Yes')
                    <a><img src="{{asset('fe-content/images/leaf.png')}}" style="margin:5px;" />Biodegradable Product</a>
                @endif
                <br>
                    @if($product->id=='5b986981bb87d'){{--galaxy--}}
                        <a href="https://youtu.be/aY3mGiC83dE" target="_blank"><img src="{{asset('fe-content/images/youtube-icon2.png')}}" width="20px" style="margin:5px;" />Play Video Presentation</a>
                    <br>
                    @elseif($product->id=='5b99dea7ee200'){{--white hot--}}
                    <a href="https://youtu.be/YtVDbrgAJtc" target="_blank"><img src="{{asset('fe-content/images/youtube-icon2.png')}}" width="20px" style="margin:5px;" />Play Video Presentation</a>
                    <br>
                    @endif

                <a href="{{asset('/uploads/product-sds/'.$product->sds)}}" download="{{$product->sds}}"><img src="{{asset('fe-content/images/pdf.png')}}" width="20px" style="margin:5px;" />Download SDS</a>
                <br>
                <a href="{{ route('productinfo.pdf',['product_id'=>$product->id]) }}" download="{{$product->product_name}}"><img src="{{asset('fe-content/images/pdf.png')}}" width="20px" style="margin:5px;" />Download Product Info</a>
                <br>
                <a href="{{ route('productinfo.pdf',['product_id'=>$product->id]) }}" target="_blank"><img src="{{asset('fe-content/images/print.png')}}"  width="20px" style="margin:5px;" />Print Product Info</a>
            </div>
            <br>
            <br>
            <a style="padding: 15px 70px; background-color: #4CAF50; color:#ffffff; cursor:pointer;">ADD TO ENQUIRY</a>
            <br>
            <br>
            <br>
            <br>
            <img class="img-responsive" style="margin:0 auto;" src="{{($product->product_img == '' ) ? asset('uploads/product-image/blank.png') : asset('uploads/product-image/'.$product->product_img)}}" />
            <br>
            <br>
            <p><strong>Pack Size</strong>: {{$product->pack_size}}</p>
            <br>
            <a href="{{route('productrange.page',[$category->category_slug,$subcategory->sub_product_slug])}}" style="padding: 15px 20px; background-color: #00679a; color:#ffffff; cursor:pointer;">{{strtoupper('view all '.$subcategory->sub_cat_name)}}</a>

        </div>
    </div>

@endsection