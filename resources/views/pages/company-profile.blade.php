@extends('layouts.master')

@section('content')
    <div class="row" style="margin-top:-20px !important;">
        <div class="col-lg-12 text-center exploreHeader">
            <h1 class="exploreText">COMPANY PROFILE</h1>
        </div>
    </div>


    <div class="row" style="margin-top:80px !important; margin-bottom:80px !important;">
        <div class="col-sm-1">
        </div>

        <div class="col-sm-10">
            <h4><strong>Company Profile</strong></h4>
            <br>
            <p>Challenge Chemicals Office Challenge Chemicals Aust. was established
                in Kwinana Beach over 28 years ago by a group of people who wished to make a difference to the chemical industry:</p>
            <br>
            <p>Challenge Chemicals is a privately owned totally Australian business, with distributors throughout Australia.</p>
            <br>
            <p>The business has developed and grown from its humble beginnings to being a major supplier to the mining and general
                industrial sectors, hospitality, food processing, transport, laundry, water treatments as well as specialty chemicals sold under brand names.</p>
            <br>
            <p>The manufacturing plant, warehouse, distribution and administration are situated at 6 Butcher Street Kwinana Beach.</p>
            <br>
            <p>Quality<br>
                Quality and continuous improvements are hallmarks of Challenge Chemicals Aust operations, and great pride is held in the fact that we are certified to
                <br>ISO 9001: 2000</p>
            <br>
            <p>Technical Experience<br>
                Challenge Chemicals provide technical expertise which delivers the best result for our customer through the knowledge of our experienced people.
                We constantly research and develop our products to stay abreast of new product developments and technology.</p>
            <br>
            <p>Environment<br>
                Challenge Chemicals Aust consider the environment in which we live and work as a priority, and are able to provide businesses with local
                support along with the latest technology and the changes associated with the methods and use of chemicals.</p>
            <br>
            <p>Customer Service<br>
                By taking time to know and understand every customers business and their unique requirements Challenge Chemicals have built
                long term relationships through consistent professional and effective customer service..</p>
            <br>
            <p>Challenge Chemicals Aust is committed to working to provide the most innovative and environmentally sound chemistry,
                together with the best people who know how to apply it.</p>
        </div>


        <div class="col-sm-1">
        </div>
    </div>
@endsection