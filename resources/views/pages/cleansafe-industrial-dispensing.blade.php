@extends('layouts.master')

@section('page_css')
    <link href="{{ asset('fe-content/css/fnb-main.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="row" style="background-image:url('{{asset('fe-content/images/cleansafe-bg.jpg')}}'); background-size:100% 100%;">
        <div class="col-lg-12 text-center">
            <br>
            <br>
            <h1 style="font-weight:bold;">SAFER, FASTER & MORE ECONOMICAL</h1>
            <br>
            <br>
            <br>
        </div>
        <div class="col-lg-6 text-left"  style="font-weight:bold;">
            <style>
                .checked1t {
                    list-style: none;
                    line-height:55px;
                }

                .checked1t li:before {
                    content: '✓ ';
                }
            </style>
            <ul class="checked1t" style="font-size:25px;">
                <h1 style="font-weight:bold; font-size:50px;">BENEFITS</h1>
                <li>Produces a rich foam allowing product to "stick" to vertical surfaces.</li>
                <li>Less product used.</li>
                <li>Less time equipment is held in cleaning phase.</li>
                <li>Less wastage</li>
                <li>Reduction of man hours</li>
                <li>Foam stream reach's up to heights of 7m.</li>
                <li>Proven results over numerous customers</li>
            </ul>
        </div>
        <div class="col-lg-6 text-center">
            <img class="img-responsive" src="{{asset('fe-content/images/cleansafe-img1.png')}}" />
        </div>
        <div class="col-lg-12 text-center">
            <br>
            <br>
            <br>
        </div>
        <div class="col-lg-6 text-center">
            <h4 style="background-color:#00679a; color:#ffffff; font-weight:bold;">
                <div style="padding-top:20px; padding-bottom:20px;">Covers and Adheres To Large Surface Areas Faster</div>
                <img class="img-responsive" width="100%" style="margin:0 auto;" src="{{asset('fe-content/images/cleansafe-img2.jpg')}}" />
            </h4>
        </div>
        <div class="col-lg-6 text-center">
            <h4 style="background-color:#00679a; color:#ffffff; font-weight:bold;">
                <div style="padding-top:20px; padding-bottom:20px;">Foam Action Assists For A Better Cleaning Result</div>
                <img class="img-responsive" width="100%" style="margin:0 auto;" src="{{asset('fe-content/images/cleansafe-img3.jpg')}}" />
            </h4>
        </div>
        <div class="col-lg-12 text-center">
            <br>
            <br>
            <br>
            <h1 style="font-weight:bold;">CLEANSAFE RANGE</h1>
            <br>
            <br>
        </div>
        <div class="col-lg-4 text-center">
            <img class="img-responsive" width="100%" style="width:300px; height:300px; margin:0 auto;" src="{{asset('fe-content/images/cleansafe-img4.jpg')}}" />
            <h4 style="font-weight:bold;">CLEANSAFE COMPACT</h4>
        </div>
        <div class="col-lg-4 text-center">
            <img class="img-responsive" width="100%" style="width:300px; height:300px; margin:0 auto;" src="{{asset('fe-content/images/cleansafe-img5.jpg')}}" />
            <h4 style="font-weight:bold;">CLEANSAFE SKID MOUNT</h4>
        </div>
        <div class="col-lg-4 text-center">
            <img class="img-responsive" style="width:300px; height:300px; margin:0 auto;" src="{{asset('fe-content/images/cleansafe-img6.jpg')}}" />
            <h4 style="font-weight:bold;">CLEANSAFE WALL MOUNT</h4>
        </div>
        <div class="col-lg-12 text-center">
            <div class="CCvideoWrapper">

                <iframe style="padding:80px;" width="560" height="315" src="https://www.youtube.com/embed/VFNjZ2jQgJg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

            </div>
        </div>
    </div>

@endsection

@section('page_js')
    <script src="{{ asset('fe-content/js/fnb.js') }}"></script>
@endsection