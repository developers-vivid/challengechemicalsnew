@if(isset($product_range))
    {{ Form::model($product_range, ['route' => ['productrange.update', $product_range->id], 'method' => 'put','id'=>'product-range-form','files'=>'true']) }}
@else
    {{ Form::open(['route' => 'productrange.store','id'=>'product-range-form','files'=>'true']) }}
@endif

<div class="form-group  {{ $errors->has('product_range_img') ? 'has-error' : '' }}">
    {{Form::label('product_range_img', 'Product image')}}
    {{ Form::file('product_range_img', old('product_range_img'),['class' => 'form-control']) }}
    {!! $errors->first('product_range_img', '<p class="help-block">:message</p>') !!}
</div>

@if(isset($product_range))
    <div class="col-md-12">
        <div class="row">
            <img src="{{($product_range->product_range_img == '' ) ? asset('uploads/product-range-images/blank.png') : asset('uploads/product-range-images/'.$product_range->product_range_img)}}" width="120px" >
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <label class="" for="description">Current image</label>
        </div>
    </div>
@endif

<div class="form-group  {{ $errors->has('sub_cat_name') ? 'has-error' : '' }}">
    {{Form::label('product_range_name', 'Name')}}
    {{ Form::text('product_range_name', old('product_range_name'),['class' => 'form-control','placeholder'=>'input product range name']) }}
    {!! $errors->first('product_range_name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('product_range_description') ? 'has-error' : '' }}">
    {{Form::label('product_range_description', 'Description')}}
    {{ Form::textarea('product_range_description', old('product_range_description'),['class' => 'form-control','placeholder'=>'input description']) }}
    {!! $errors->first('product_range_description', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('sub_product_category_id') ? 'has-error' : '' }}">
    {{Form::label('sub_product_category_id', 'Select sub category')}}
    {{Form::select('sub_product_category_id', $sub_category, null, ['class'=>'form-control','placeholder' => 'choose one'])}}
    {!! $errors->first('sub_product_category_id', '<p class="help-block">:message</p>') !!}
</div>

{{ Form::submit('Save', ['class'=>'btn btn-primary']) }}

{{ Form::close() }}

