@extends('cms.layouts.master')

@section('page_css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

@endsection

@section('content')
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        @endif
        <div class="col-lg-12">
            <h1 class="page-header">Product Range
                {{--<a class="btn btn-primary" href="{{route('productrange.create')}}"><i class="fa fa-plus"></i> Add product range</a>--}}
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Product range data
                </div>

                <div class="panel-body">
                    <table class="table table-bordered" id="product_range_table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Sub category</th>
                            <th>Created at</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    {{-- {{ $product_sub_category->links() }}--}}
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->

            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

@section('modal')
    @include('cms.modal.delete-modal')
@endsection

@section('page_js')
    <!-- Datatable Js -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    {!! Toastr::message() !!}

    <script type="text/javascript">

        $(document).ready(function ($) {


            var productRangeTable;

            productRangeData();


            $('#delete-modal').on('show.bs.modal', function (event) {

                var button = $(event.relatedTarget); // Button that triggered the modal
                var product_range_id = button.data('product_range_id'); // Extract info from data-* attributes

                var modal = $(this);

                var url = '{{ route("productrange.destroy", ":product_range_id") }}';
                url = url.replace(':product_range_id', product_range_id);
                $('#delete-form').attr('action',url);
            });


        });

        function productRangeData(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            productRangeTable = $('#product_range_table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax:{
                    "url": "{!! route('productrange.get') !!}",
                    "type": "POST",
                },
                columns: [
                    { data: 'product_range_name', name: 'product_range_name' },
                    { data: 'product_range_description', name: 'product_range_description' },
                    { data: 'productSubCategory.sub_cat_name', name: 'productSubCategory.sub_cat_name'},
                    { data: 'created_at', name: 'created_at'},
                    { data: 'action', name: 'action' },

                ],
                columnDefs: [
                    { searchable: true, targets: '_all'},
                    { searchable: false, targets: [0,4]},
                    { orderable: false, targets: [0,4]},

                ],
            });
        }

    </script>

@endsection