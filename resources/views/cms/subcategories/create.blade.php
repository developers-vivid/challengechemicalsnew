@extends('cms.layouts.master')

@section('page_css')
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
@endsection

@section('content')
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        @endif
        <div class="col-lg-12">
            <h1 class="page-header">Create sub-category</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="{{route('subcategories.store')}}" method="post" role="form" id="subcategories-create-form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('sub_cat_name') ? 'has-error' : '' }}">
                            <label for="">Sub category name</label>
                            <input type="text" class="form-control" name="sub_cat_name"  id=""  value="{{ old('sub_cat_name') }}" placeholder="Input sub category name">
                            {!! $errors->first('page_name', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group  {{ $errors->has('sub_cat_img') ? 'has-error' : '' }}">
                            {{Form::label('sub_cat_img', 'Sub category image')}}
                            {{ Form::file('sub_cat_img', old('sub_cat_img'),['class' => 'form-control']) }}
                            {!! $errors->first('sub_cat_img', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('sub_cat_description') ? 'has-error' : '' }}">
                            <label for="">Sub category description</label>
                            <textarea type="text" class="form-control" name="sub_cat_description" id="description" placeholder="Input description" style="resize:vertical;
height:200px;
min-height:200px;
max-height:200px;">{{ old('sub_cat_description') }}</textarea>
                            {!! $errors->first('sub_cat_description', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('select_category') ? 'has-error' : '' }}">
                            <label for="select_category">Select category</label>
                            <select name="select_category" id="select_category" class="form-control">
                                @forelse($product_categories as $product_category)
                                    <option value="{{$product_category->id}}">{{$product_category->category_name}}</option>
                                @empty
                                    <option value="">No data available</option>
                                @endforelse
                            </select>
                            {!! $errors->first('select_category', '<p class="help-block">:message</p>') !!}
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

@section('modal')

@endsection

@section('page_js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CreateProductSubCategoryRequest','#subcategories-create-form') !!}

    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    {!! Toastr::message() !!}
@endsection