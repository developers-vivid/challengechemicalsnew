@extends('cms.layouts.master')

@section('page_css')

@endsection

@section('content')
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        @endif
        <div class="col-lg-12">
            <h1 class="page-header">Sub-categories
                {{--<a class="btn btn-primary" href="{{route('subcategories.create')}}"><i class="fa fa-plus"></i> Add sub-category</a>--}}
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Sub categories data
                </div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Sub-category name</th>
                            <th>Category</th>
                            <th>Created at</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($product_sub_category as $sub_category)
                            <tr>
                                <td>{{$sub_category->sub_cat_name}}</td>
                                <td>{{$sub_category->productCategory->category_name}}</td>
                                <td>{{Carbon\Carbon::parse($sub_category->created_at)->format('d/m/Y')}}</td>
                                <td>
                                    <a href="{{route('subcategories.edit',$sub_category->id)}}" class="btn btn-info" role="button">Edit</a>
                                    <a href="#delete-modal" class="btn btn-danger" data-toggle="modal" data-sub_category="{{$sub_category->id}}" >Delete</a>
                                </td>
                            </tr>
                        @empty
                            no page available
                        @endforelse
                        </tbody>
                    </table>
                    {{ $product_sub_category->links() }}
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->

            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

@section('modal')
    @include('cms.modal.delete-modal')
@endsection

@section('page_js')
    <script type="text/javascript">

        $(document).ready(function ($) {


            $('#delete-modal').on('show.bs.modal', function (event) {

                var button = $(event.relatedTarget); // Button that triggered the modal
                var sub_category = button.data('sub_category'); // Extract info from data-* attributes

                var modal = $(this);

                var url = '{{ route("subcategories.destroy", ":sub_category") }}';
                url = url.replace(':sub_category', sub_category);
                $('#delete-form').attr('action',url);
            });


        });


    </script>
@endsection