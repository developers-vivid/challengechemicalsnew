@extends('cms.layouts.master')

@section('page_css')

@endsection

@section('content')
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        @endif
        <div class="col-lg-12">
            <h1 class="page-header">Categories
                {{--<a class="btn btn-primary" href="{{route('categories.create')}}"><i class="fa fa-plus"></i> Add category</a>--}}
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Category data
                </div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Category name</th>
                            <th>Created at</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($pro_cat as $pc)
                            <tr>
                                <td>{{$pc->category_name}}</td>
                                <td>{{Carbon\Carbon::parse($pc->created_at)->format('d/m/Y')}}</td>
                                <td>
                                    <a href="{{route('categories.edit',$pc->id)}}" class="btn btn-info" role="button">Edit</a>{{-- | <a href="#" class="btn btn-danger" role="button">Delete</a>--}}
                                </td>
                            </tr>
                        @empty
                            no category available
                        @endforelse
                        </tbody>
                    </table>
                    {{ $pro_cat->links() }}
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->

            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

@section('modal')

@endsection

@section('page_js')

@endsection