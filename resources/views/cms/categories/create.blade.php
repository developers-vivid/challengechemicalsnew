@extends('cms.layouts.master')

@section('page_css')
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
@endsection

@section('content')
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        @endif
        <div class="col-lg-12">
            <h1 class="page-header">Create category</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <legend>Category details</legend>
                    <form action="{{route('categories.store')}}" method="post" role="form" id="category-create-form">
                        {{ csrf_field() }}
                        <div class="form-group  {{ $errors->has('category_name') ? 'has-error' : '' }}">
                            <label for="">Category name</label>
                            <input type="text" class="form-control" name="category_name"  id=""  value="{{ old('category_name') }}" placeholder="Input category name">
                            {!! $errors->first('category_name', '<p class="help-block">:message</p>') !!}

                        </div>

                        <div class="form-group  {{ $errors->has('category_video') ? 'has-error' : '' }}">
                            <label for="">Category video link</label>
                            <input type="text" class="form-control" name="category_video"  id=""  value="{{ old('category_video') }}" placeholder="Input category video link">
                            {!! $errors->first('category_video', '<p class="help-block">:message</p>') !!}

                        </div>

                        <div class="form-group {{ $errors->has('category_description') ? 'has-error' : '' }}">
                            <label for="">Category Description</label>
                            <textarea type="text" class="form-control" name="category_description" id="description" value="{{ old('category_description') }}" placeholder="Input category description" style="resize:vertical;
height:200px;
min-height:200px;
max-height:200px;"></textarea>
                            {!! $errors->first('category_description', '<p class="help-block">:message</p>') !!}
                        </div>

                        <legend>Page details</legend>

                        <div class="form-group  {{ $errors->has('page_name') ? 'has-error' : '' }}">
                            <label for="">Page name / title</label>
                            <input type="text" class="form-control" name="page_name"  id=""  value="{{ old('page_name') }}" placeholder="Input page name">
                            {!! $errors->first('page_name', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('page_content') ? 'has-error' : '' }}">
                            <label for="">Page Content</label>
                            <textarea type="text" class="form-control" name="page_content" id="description" value="{{ old('page_content') }}" placeholder="Input page content" style="resize:vertical;
height:200px;
min-height:200px;
max-height:200px;"></textarea>
                            {!! $errors->first('page_content', '<p class="help-block">:message</p>') !!}
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

@section('modal')

@endsection

@section('page_js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CreateProductCategoryRequest','#category-create-form') !!}

    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    {!! Toastr::message() !!}
@endsection