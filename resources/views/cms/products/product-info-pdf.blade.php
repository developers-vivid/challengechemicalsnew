<!DOCTYPE HTML>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{asset('fe-content/product-pdf-info/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('fe-content/product-pdf-info/css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('fe-content/product-pdf-info/css/header-footer.css')}}" rel="stylesheet">
    <title>Product info</title>
</head>

<style type="text/css">

    @page {margin-top: 50px; margin-bottom: 50px; margin-left:0px; margin-right:0px}

    /** Define now the real margins of every page in the PDF **/
    body {
        margin-top: 3cm;
        margin-left: 2cm;
        margin-right: 2cm;
        margin-bottom: 2cm;
    }

    p{
        font-size:13.5px !important;
        padding:1px !important;
    }

    h1{
        font-size:13.5px !important;
        padding:1px !important;
    }

    li{
        font-size:13.5px !important;
        padding:1px !important;
    }

    /** Define the header rules **/
    header {
        position: fixed; top: -90px; left: 0px; right: 0px; height: 50px;
    }

    /** Define the footer rules **/
    /*footer {*/
    /*bottom: -50px; position: fixed;*/
    /*}*/
    footer .pagenum:before {
        content: counter(page);
    }

    footer {
        position: fixed;
        bottom: -60px;
        left: 0px;
        right: 0px;
        height: 50px;

        /** Extra personal styles **/
        /*background-color: #3f903f;*/
        /*color: white;*/
        text-align: right;
        /*line-height: 35px;*/
    }
</style>

<body>
<script type="text/php">
    if (isset($pdf)) {
        $x = 56;
        $y = 765;
        $date = date('m-d-Y');
        $text = "Page {PAGE_NUM} of {PAGE_COUNT}" ;
        $font = null;
        $size = 10;
        $color = array(0,0,0);
        $word_space = 0.0;  //  default
        $char_space = 0.0;  //  default
        $angle = 0.0;   //  default
        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
    }
</script>
<header>
    <img src="fe-content/product-pdf-info/images/header-pdf-info.png" width="100%">

</header>
{{--<div class="header">--}}
{{--<img style="width:100%;" src="fe-content/product-pdf-info/images/wordheader.png" />--}}
{{--</div>--}}
<footer>
    <p style="margin-right: 50px;">Rev: {{ \Carbon\Carbon::parse($product->updated_at)->format('m')}}/{{ \Carbon\Carbon::parse($product->updated_at)->format('y')}} </p>
    <p class="num text-center" style="position:relative; top:-30px;">+61 8 9419 5577</p>
</footer>
<!-- Page Content -->

<div style="width:90%; margin:0 auto; text-align:center; clear:both;">
    <div style="float: left; width: 50%; text-align:center;">
        <br>
        <br>
        <div style="float:right; position:relative; top:-25px; text-align:left;">
            <h2 style="font-size: 3.2em; font-weight:bold; position:relative; right:65px !important; left:-65px !important;">{{$product->product_name}}</h2>
            <h2 style="font-size: 2.2em; font-weight:normal; position:relative; right:65px !important; left:-65px !important;">{{$product->product_description}}</h2>
        </div>
        <br>
        <br>
        <br>
        <br>
    </div>
    <div style="float: left; width: 50%;">
        <br>
        <br>
        <img style="width:260px !important; height:260px !important; position:relative; right:-20px; top:-20px;" src="{{($product->product_img == '' ) ? 'uploads/product-image/blank.png' : 'uploads/product-image/'.$product->product_img}}" />
        <br>
        <br>
        <br>
        <br>
    </div>
</div>
<br>
<div style="clear:both;"></div>
<div style="width:90%; margin:0 auto; text-align:left; position:relative; top:-10px;">
    @if($product->product_details ==null)
        <p>No Product info</p>
    @else
        {!! $product->product_details !!}
    @endif
</div>



<!-- /.row -->