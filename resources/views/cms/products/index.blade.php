@extends('cms.layouts.master')

@section('page_css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

@endsection

@section('content')
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        @endif
        <div class="col-lg-12">
            <h1 class="page-header">Products
                <a class="btn btn-primary" href="{{route('products.create')}}"><i class="fa fa-plus"></i> Add product </a>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Products data
                </div>

                <div class="panel-body">
                    <table class="table table-bordered" id="product_table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Product Range</th>
                            <th>Pack Size</th>
                            <th>Downloads</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->

            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

@section('modal')

@include('cms.modal.delete-modal')

@endsection

@section('page_js')
    <!-- Datatable Js -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    {!! Toastr::message() !!}

    <script type="text/javascript">

        $(document).ready(function ($) {


            var productTable;

            productData();



            $('#delete-modal').on('show.bs.modal', function (event) {

                var button = $(event.relatedTarget); // Button that triggered the modal
                var product_id = button.data('product_id'); // Extract info from data-* attributes

                var modal = $(this);

                var url = '{{ route("products.destroy", ":product_id") }}';
                url = url.replace(':product_id', product_id);
                $('#delete-form').attr('action',url);
            });


        });

        function productData(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            productTable = $('#product_table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax:{
                    "url": "{!! route('products.get') !!}",
                    "type": "POST",
                },
                columns: [
                    { data: 'product_name', name: 'product_name' },
                    { data: 'product_description', name: 'product_description' },
                    { data: 'productRange.product_range_name', name: 'productRange.product_range_name'},
                    { data: 'pack_size', name: 'pack_size'},
                    { data: 'sds_pdf', name: 'sds_pdf'},
                    { data: 'action', name: 'action' },

                ],
                columnDefs: [
                    { searchable: true, targets: '_all'},
                    { searchable: false, targets: [5]},
                    { orderable: false, targets: [5]},
                    { width: '20%', targets: 3 },
                    { width: '20%', targets: 1 }

                ],
                order: [
                    [ 0, "asc" ]
                ],
                "fnDrawCallback": function(){
                    $('[data-toggle="popover"]').popover({
                        placement: 'top',
                        trigger: 'hover'
                    });
                }
            })


        }

    </script>

@endsection