@if(isset($product))
    {{ Form::model($product, ['route' => ['products.update', $product->id], 'method' => 'put','id'=>'product-form','files'=>'true']) }}
@else
    {{ Form::open(['route' => 'products.store','id'=>'product-form','files'=>'true']) }}
@endif

<div class="form-group  {{ $errors->has('product_img') ? 'has-error' : '' }}">
    {{Form::label('product_img', 'Product image')}}
    {{ Form::file('product_img', old('product_img'),['class' => 'form-control']) }}
    {!! $errors->first('product_img', '<p class="help-block">:message</p>') !!}
</div>

@if(isset($product))
<div class="col-md-12">
    <div class="row">
    <img src="{{($product->product_img == '' ) ? asset('uploads/product-image/blank.png') : asset('uploads/product-image/'.$product->product_img)}}" class="img-responsive img-thumbnail" width="219" height="255">
    </div>
</div>

<div class="col-md-12">
    <div class="row">
    <label class="" for="description">Current image</label>
    </div>
</div>
@endif


<div class="form-group  {{ $errors->has('product_name') ? 'has-error' : '' }}">
    {{Form::label('product_name', 'Name')}}
    {{ Form::text('product_name', old('product_name'),['class' => 'form-control','placeholder'=>'input product name']) }}
    {!! $errors->first('product_name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group  {{ $errors->has('pack_size') ? 'has-error' : '' }}">
    {{Form::label('pack_size', 'Pack size')}}
    {{ Form::text('pack_size', old('pack_size'),['class' => 'form-control','placeholder'=>'input pack size']) }}
    {!! $errors->first('pack_size', '<p class="help-block">:message</p>') !!}
</div>
@if(isset($product))
<div class="form-group">
    {!! Form::label('Downloads') !!}<br>
    <a href="{{ route('productinfo.pdf',['product_id'=>$product->id]) }}"  target="_blank" data-toggle="popover" data-content="Product info"><img src="{{asset('fe-content/images/printer.png')}}" width="20px" ></a>
    @if($product->sds==null)
        <p>No current sds file</p>
    @else
        | <a href="{{asset('/uploads/product-sds/'.$product->sds)}}" target="_blank" data-toggle="popover" data-content="Product SDS"><img src="{{asset('fe-content/images/pdf.png')}}"  width="20px"></a>
    @endif

</div>

<input  id="field1"  type="hidden" />
@endif
<div class="form-group {{ $errors->has('is_biodegrable') ? 'has-error' : '' }}">
    {{Form::label('is_biodegrable', 'Biodegrable')}}
    {{Form::select('is_biodegrable', array('Yes' => 'Yes', 'No' => 'No','Not Applicable'=>'Not Applicable'), old('is_biodegrable'), ['class'=>'form-control','placeholder' => 'choose one'])}}
    {!! $errors->first('is_biodegrable', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('sds') ? 'has-error' : '' }}">
    {!! Form::label('Product sds') !!}
    {!! Form::file('sds', old('sds'),['class' => 'form-control','placeholder'=>'upload sds']) !!}
    {!! $errors->first('sds', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('product_description') ? 'has-error' : '' }}">
    {{Form::label('product_description', 'Short description')}}
    {{ Form::textarea('product_description', old('product_description'),['class' => 'form-control','placeholder'=>'input description','rows'=>'3']) }}
    {!! $errors->first('product_description', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('product_details') ? 'has-error' : '' }}">
    {{Form::label('product_details', 'Product Info')}}
    {{ Form::textarea('product_details', old('product_details'),['class' => 'form-control','placeholder'=>'input details','rows'=>'100']) }}
    {!! $errors->first('product_details', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('product_range_id') ? 'has-error' : '' }}">
    {{--{{Form::label('product_range_id', 'Select product range')}}--}}
    {{--{{Form::select('product_range_id', $product_range, null, ['class'=>'form-control','placeholder' => 'choose one'])}}--}}

    <label>Select product range</label>
    <select class="form-control" name="product_range_id">
        <option value=""> please select one</option>
        @forelse($product_range as $pr)
        <option value="{{$pr->id}}" @if(isset($product)) @if($product->product_range_id === $pr->id) selected @endif @endif >{{$pr->productSubCategory->sub_cat_name}} - {{$pr->product_range_name}}</option>
        @empty
        @endforelse
    </select>
    {!! $errors->first('product_range_id', '<p class="help-block">:message</p>') !!}
</div>

{{ Form::submit('Save', ['class'=>'btn btn-primary']) }}

{{ Form::close() }}

