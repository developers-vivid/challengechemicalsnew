@extends('cms.layouts.master')

@section('page_css')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
@endsection

@section('content')
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        @endif
        <div class="col-lg-12">
            <h1 class="page-header">

                <div class="col-lg-12">
                    <h1 class="page-header">Edit Product</h1>
                </div>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('cms.products.form')
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

@section('modal')

@endsection

@section('page_js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CreateProductRequest','#product-form') !!}

    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    {!! Toastr::message() !!}

    <script>
        $(document).ready(function() {
            $('textarea[name="product_details"]').summernote({
                height: 480,
                placeholder: 'Type product info here...',
                maximumImageFileSize: 2097152,
                callbacks: {
                    onMediaDelete : function(target) {
                       // deleteFile(target[0].src);
                        var next = 1;
                            var addto = "#field" + next;
                            var addRemove = "#field" + (next);
                            next = next + 1;
                            var newIn = '<input id="field' + next + '" name="deletedImg[]" type="hidden" value="'+target[0].src+'">';
                            var newInput = $(newIn);
                            $(addto).after(newInput);
                            $("#field" + next).attr('data-source',$(addto).attr('data-source'));
                            $("#count").val(next);
                    },
                    onImageUploadError: function(){
                        toastr.warning('Error image attachment. Check image format and image size (max size allowed 2MB only)')
                    }
                }
            });
        });

        {{--function deleteFile(src) {--}}

            {{--$.ajaxSetup({--}}
                {{--headers: {--}}
                    {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
                {{--}--}}
            {{--});--}}

            {{--$.ajax({--}}
                {{--data: {src : src},--}}
                {{--type: "POST",--}}
                {{--url: "{{route('deleteFile.get')}}", // replace with your url--}}
                {{--cache: false,--}}
                {{--success: function(resp) {--}}
                    {{--console.log(resp);--}}
                {{--}--}}
            {{--});--}}
        {{--}--}}
    </script>
@endsection