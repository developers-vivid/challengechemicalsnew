@extends('cms.layouts.master')

@section('page_css')
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
@endsection

@section('content')
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        @endif
        <div class="col-lg-12">
            <h1 class="page-header">Create page</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="{{route('pages.create')}}" method="post" role="form" id="page-create-form">
                        {{ csrf_field() }}
                        <div class="form-group  {{ $errors->has('page_name') ? 'has-error' : '' }}">
                            <label for="">Page name</label>
                            <input type="text" class="form-control" name="page_name"  id=""  value="{{ old('page_name') }}" placeholder="Input page name">
                            {!! $errors->first('page_name', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('page_content') ? 'has-error' : '' }}">
                            <label for="">Page Content</label>
                            <textarea type="text" class="form-control" name="page_content" id="description" value="{{ old('page_content') }}" placeholder="Input page content" style="resize:vertical;
height:200px;
min-height:200px;
max-height:200px;"></textarea>
                            {!! $errors->first('page_content', '<p class="help-block">:message</p>') !!}
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

@section('modal')

@endsection

@section('page_js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CreatePageRequest','#page-create-form') !!}

    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    {!! Toastr::message() !!}
@endsection