@extends('cms.layouts.master')

@section('page_css')

@endsection

@section('content')
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        @endif
        <div class="col-lg-12">
            <h1 class="page-header">Product Pages
                {{--<a class="btn btn-primary" href="pages/create"><i class="fa fa-plus"></i> Add page</a>--}}
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Page data
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Page name</th>
                                <th>Page for</th>
                                <th>Created at</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($productcategory as $page)
                                <tr>
                                    <td>{{$page->page->page_name}}</td>
                                    <td>{{$page->category_name}}</td>
                                    <td>{{Carbon\Carbon::parse($page->page->created_at)->format('d/m/Y')}}</td>
                                    <td>
                                        <a href="{{route('pages.edit',$page->id)}}" class="btn btn-info" role="button">Edit</a>
                                       {{-- <a href="#" class="btn btn-danger" role="button">Delete</a>--}}
                                    </td>
                                </tr>
                            @empty
                                no page available
                            @endforelse
                            </tbody>
                        </table>
                        {{ $productcategory->links() }}
                    </div>
                    <!-- /.panel-body -->
                </div>
            <!-- /.panel -->

        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection

@section('modal')

@endsection

@section('page_js')

@endsection