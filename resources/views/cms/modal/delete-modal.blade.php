
<div class="modal fade" id="delete-modal">
    <div class="modal-dialog">
        {{ Form::open(['id'=>'delete-form','method'=>'DELETE']) }}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <strong>Are you sure you want to delete this item?</strong>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                {{ Form::submit('Yes', ['class'=>'btn btn-danger']) }}
            </div>
        </div><!-- /.modal-content -->
        {{ Form::close() }}
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->