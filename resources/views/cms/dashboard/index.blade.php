
@extends('cms.layouts.master')

@section('page_css')
    <link href="{{ asset('theme-content/vendor/morrisjs/morris.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        @endif
        <div class="col-lg-12">
                <h1 class="page-header">Welcome to admin dashboard</h1>
        </div>
    </div>
        <!-- /.col-lg-12 -->
        <div class="row">
            <div class="col-lg-12 well">
                <h3 class="page-header"><a href="{{route('home')}}">Visit site</a></h3>
            </div>
        </div>


@endsection

@section('page_js')
    <!-- Morris Charts JavaScript -->
    <script href="{{ asset('theme-content/vendor/raphael/raphael.min.js') }}"></script>
    <script href="{{ asset('theme-content/vendor/morrisjs/morris.min.js') }}"></script>
    <script href="{{ asset('theme-content/data/morris-data.js') }}"></script>

@endsection