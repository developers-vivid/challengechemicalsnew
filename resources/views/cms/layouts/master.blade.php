<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Challenge Chemical Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('theme-content/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">


    <!-- MetisMenu CSS -->
    <link href="{{ asset('theme-content/vendor/metisMenu/metisMenu.min.css') }}" rel="stylesheet">
    <!-- Custom CSS -->

    <link href="{{ asset('theme-content/dist/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
        @yield('page_css')

    <!-- Custom Fonts -->
    
    <link href="{{ asset('theme-content/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        @include('cms.layouts.navbar-top')

        @include('cms.layouts.navbar-side')

        <div id="page-wrapper">
            @yield('content')
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    @yield('modal')

    <!-- jQuery -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script href="{{ asset('theme-content/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
   
    <script href="{{ asset('theme-content/vendor/metisMenu/metisMenu.min.js') }}"></script>

    @yield('page_js')

    <!-- Custom Theme JavaScript -->

    <script href="{{ asset('theme-content/dist/js/sb-admin-2.js') }}"></script>
    <script>

        $(document).ready(function() {
            $('[data-toggle="popover"]').popover({
                placement: 'top',
                trigger: 'hover'
            });
        });
    </script>
</body>

</html>
