<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                </div>
                <!-- /input-group -->
            </li>
            <li class="">
                <a href="{{route('dashboard.index')}}" class="{{ Request::is('admin') ? 'active' : '' }} {{ Request::is('admin/dashboard*') ? 'active' : '' }}" ><i class="fa fa-dashboard fa-fw "></i> Dashboard</a>
            </li>
            <li class="">
                {{--<a href="/admin/pages" class="{{ Request::is('admin/pages*') ? 'active' : '' }}" ><i class="fa fa-newspaper-o "></i> Product Pages</a>--}}
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="{{route('products.index')}}"><i class="fa fa-files-o fa-fw"></i> Product<span class="fa arrow"></span></a>
                <style>
                    .menu-admin123 {
                        width:100%;
                        min-width: 160px;
                        box-shadow:none;
                        border:none;
                        background:none !important;
                    }
                    .menu-admin123 li a{
                        color:#4680b7 !important;
                    }
                    .dropdown .dropdown-menu {
                        -webkit-transition: all 0.3s;
                        -moz-transition: all 0.3s;
                        -ms-transition: all 0.3s;
                        -o-transition: all 0.3s;
                        transition: all 0.3s;

                        max-height: 0;
                        display: block;
                        overflow: hidden;
                        opacity: 0;
                    }

                    .dropdown.open .dropdown-menu {
                        max-height: 300px;
                        opacity: 1;
                    }
                </style>
                <ul class="nav nav-second-level dropdown-menu menu-admin123">
                    <li>
                        <a href="{{route('products.index')}}" class="{{ Request::is('admin/products*') ? 'active' : '' }}">Products</a>
                    </li>
                    <li>
                        <a href="{{route('productrange.index')}}" class="{{ Request::is('admin/productrange*') ? 'active' : '' }}">Product Range</a>
                    </li>
                    <li>
                        <a href="{{route('subcategories.index')}}" class="{{ Request::is('admin/subcategories*') ? 'active' : '' }}">Sub-categories</a>
                    </li>
                    <li>
                        <a href="{{route('categories.index')}}" class="{{ Request::is('admin/categories*') ? 'active' : '' }}">Categories</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>