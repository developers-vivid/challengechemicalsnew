<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Challenge Chemicals</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('fe-content/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('fe-content/css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('fe-content/css/errorCustom.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <style>
        body {
            padding-top: 10px;
            /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
        }

        .search {
            padding-top:50px;
            background: linear-gradient(to right, #0c386c , #0bd0d1);
            color:#ffffff;
        }

        .searchButtonAndHolder{
            padding-bottom:70px;
        }

        .searchBox{
            width:40%;
            height:40px;
            color:#464646;
        }

        .searchButton{
            height:40px;
            padding-left:20px;
            padding-right:20px;
            color:grey;
        }

        .carousel h1{
            color:#000000;
            margin-top:-56%;
        }

        .exploreHeader{
            background-color:#006095;
            color:#ffffff;
        }

        .exploreText{
            padding-top:10px;
            padding-bottom:40px;
        }

        .footer1stColumn{
            padding-top:20px;
            padding-bottom:20px;
            font-weight:bold;
            line-height: 1.7;
        }

        .footer1stColumn a{
            color:#464646;
        }

        .footer2ndColumn img{
            margin-top:50px;
        }

        .footer3rdColumn img{
            margin-top:30px;
        }

        .footer4thColumn{
            padding-top:20px;
            padding-bottom:20px;
            font-weight:bold;
            line-height: 1.7;
        }

        .footer4thColumn img{
            width:250px;
        }

        .footer{
            background-color:#2d3e50;
            color:#d0d2d4;
        }

        .footerTop{
            padding-top:20px;
            padding-bottom:20px;
        }

        .exploreTop a{
            color:#464646;
        }

        .exploreBottom a{
            color:#464646;
        }

        .navright1{
            color:#00679a;
        }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Page Content -->
<div class="container-fluid">


    <link href='https://fonts.googleapis.com/css?family=Anton|Passion+One|PT+Sans+Caption' rel='stylesheet' type='text/css'>
    <body>

    <!-- Error Page -->
    <div class="error">
        <div class="container-floud">
            <div class="col-xs-12 ground-color text-center">
                <div class="container-error-404">
                    <div class="clip"><div class="shadow"><span class="digit thirdDigit"></span></div></div>
                    <div class="clip"><div class="shadow"><span class="digit secondDigit"></span></div></div>
                    <div class="clip"><div class="shadow"><span class="digit firstDigit"></span></div></div>
                    <div class="msg" style="color:#ffffff; font-size:25px;">OPS!<span class="triangle"></span></div>
                </div>
                <h2 class="h1">Sorry! Page not found</h2>
            </div>
        </div>
    </div>
    <!-- Error Page -->
    </body>


</div>
<!-- /.container -->

<!-- jQuery Version 1.11.1 -->
<script src="{{asset('fe-content/js/jquery.js')}}js/jquery.js"></script>
<script src="{{asset('fe-content/js/custom.js')}}"></script>
<script src="{{asset('fe-content/js/errorCustom.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{asset('fe-content/js/bootstrap.min.js')}}js/bootstrap.min.js"></script>

</body>

</html>
