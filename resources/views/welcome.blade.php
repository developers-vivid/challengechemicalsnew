@extends('layouts.master')

@section('content')
<div class="row carouselBody">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>
            <li data-target="#myCarousel" data-slide-to="5"></li>
            <li data-target="#myCarousel" data-slide-to="6"></li>
            <li data-target="#myCarousel" data-slide-to="7"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="{{asset('fe-content/images/slider1.jpg')}}" style="width:100%;">
            </div>
            <div class="item ">
                <img src="{{asset('fe-content/images/slider2.jpg')}}" style="width:100%;">
            </div>
            <div class="item ">
                <img src="{{asset('fe-content/images/slider3.jpg')}}" style="width:100%;">
            </div>
            <div class="item ">
                <img src="{{asset('fe-content/images/slider4.jpg')}}" style="width:100%;">
            </div>
            <div class="item ">
                <img src="{{asset('fe-content/images/slider5.jpg')}}" style="width:100%;">
            </div>
            <div class="item ">
                <img src="{{asset('fe-content/images/slider6.jpg')}}" style="width:100%;">
            </div>
            <div class="item ">
                <img src="{{asset('fe-content/images/slider7.jpg')}}" style="width:100%;">
            </div>
            <div class="item ">
                <img src="{{asset('fe-content/images/slider8.jpg')}}" style="width:100%;">
            </div>
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<div class="row" style="margin-top:-20px !important;">
    <div class="col-lg-12 text-center exploreHeader">
        <h1 class="exploreText">Explore our Ranges</h1>
    </div>
</div>


<div class="row exploreTop"  style="margin-top:-20px !important;">
    <a href="{{route('category.page',$hospitality->category_slug)}}">
        <div class="col-lg-4 text-center zoom2" style="background-color:#fc72fe; margin-top:-20px !important; height:380px;">
            <br>
            <br>
            <br>
            <img src="{{asset('fe-content/images/hospitality.png')}}" width="280px" height="200px"/>
            <h3 class="exploreText">{{strtoupper($hospitality->category_name)}}</h3>
        </div>
    </a>
    <a href="{{route('landing.page',$automotive->category_slug)}}">
        <div class="col-lg-4 text-center zoom2" style="background-color:#12ebb1; margin-top:-20px !important; height:380px;">
            <br>
            <br>
            <br>
            <img src="{{asset('fe-content/images/auto.png')}}" width="280px" height="" />
            <h3 class="exploreText">{{strtoupper($automotive->category_name)}}</h3>
        </div>
    </a>
    <a href="{{route('landing.page',$toll->category_slug)}}">
        <div class="col-lg-4 text-center zoom2" style="background-color:#c4f0fe; margin-top:-20px !important; height:380px;">
            <br>
            <br>
            <br>
            <img src="{{asset('fe-content/images/toll.png')}}" width="200px" height=""/>
            <h3 class="exploreText">{{strtoupper($toll->category_name)}}</h3>
        </div>
    </a>
</div>

<div class="row exploreBottom"  style="margin-top:-20px !important;">
    {{--<a href="{{route('category.page',$water->category_slug)}}">--}}
    {{--product/{category_slug}/{sub_category_slug}/{product_range_slug}/productsds--}}


    <a href="{{route('productsds.page',[$water->category_slug,$water_sub->sub_product_slug,$water_range->product_range_slug])}}">
        <div class="col-lg-4 text-center zoom2" style="background-color:#4fcafb; margin-top:-20px !important; height:380px;">
            <br>
            <br>
            <br>
            <img src="{{asset('fe-content/images/water.png')}}" width="" height=""/>
            <h3 class="exploreText">{{strtoupper($water->category_name)}}</h3>
        </div>
    </a>

    <a href="{{route('landing.page',$food->category_slug)}}">
        <div class="col-lg-4 text-center zoom2" style="background-color:#70ebea; margin-top:-20px !important; height:380px;">
            <br>
            <br>
            <br>
            <img src="{{asset('fe-content/images/food-home.png')}}" width="195px" height="" />
            <h3 class="exploreText">{{strtoupper($food->category_name)}}</h3>
        </div>
    </a>
    <a href="{{route('landing.page',$health->category_slug)}}">
        <div class="col-lg-4 text-center zoom2" style="background-color:#fec4f1; margin-top:-20px !important; height:380px;">
            <br>
            <br>
            <br>
            <img src="{{asset('fe-content/images/health.png')}}" width="200px" height=""/>
            <h3 class="exploreText">{{strtoupper($health->category_name)}}</h3>
        </div>
    </a>
</div>

@endsection