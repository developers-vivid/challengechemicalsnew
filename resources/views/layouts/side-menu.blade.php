{{--<div id="ccSidenav" class="sidenav">--}}
    {{--<img src="{{asset('fe-content/images/logo-nav.png')}}" width="200px" style="position:relative; left:30px; top:-40px;" /><span id="navClose" style="display:none; top:4px;" href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</span>--}}
    {{--<hr>--}}
    {{--<a href="{{route('home')}}"><strong>&nbsp; &nbsp; <span style="font-size:15px; position:relative; bottom:-5px;">Home</span></strong></a>--}}
    {{--<hr>--}}
    {{--<a href="{{route('services.page')}}"><strong>&nbsp; &nbsp; <span style="font-size:15px; position:relative; bottom:-5px;">Services</span></strong></a>--}}
    {{--<hr>--}}
    {{--<a href="{{route('category.page',$cat_hospitality->category_slug)}}"><strong>&nbsp; &nbsp; <span style="font-size:15px; position:relative; bottom:-5px;">{{$cat_hospitality->category_name}}</span></strong></a>--}}
    {{--<hr>--}}
    {{--<a href="{{route('category.page',$cat_automotive->category_slug)}}"><strong>&nbsp; &nbsp; <span style="font-size:15px; position:relative; bottom:-5px;">{{ str_limit(($cat_automotive->category_name),18) }}</span></strong></a>--}}
    {{--<hr>--}}
    {{--<a href="{{route('aboutus.page')}}"><strong>&nbsp; &nbsp; <span style="font-size:15px; position:relative; bottom:-5px;">About Us</span></strong></a>--}}
    {{--<hr>--}}
    {{--<a href="{{route('news.page')}}"><strong>&nbsp; &nbsp; <span style="font-size:15px; position:relative; bottom:-3px;">News</span></strong></a>--}}
    {{--<hr>--}}
    {{--<a href="{{route('contactus.page')}}"><strong>&nbsp; &nbsp; <span style="font-size:15px; position:relative; bottom:-3px;">Contact</span></strong></a>--}}
    {{--<hr>--}}
{{--</div>--}}

<div id="ccSidenav" class="sidenav">
    <a href="{{route('home')}}">
    <img src="{{asset('fe-content/images/logo-nav.png')}}" width="200px" style="position:relative; left:30px; top:-40px;" /><span id="navClose" style="display:none; top:4px;" href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</span>
    </a>
    <hr>
    <div class="dropdown">
        <a href="#" class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown"><strong>&nbsp; &nbsp; <span style="font-size:15px; position:relative; bottom:-5px;">Products</span></strong><img class="caret" width="20px" style="margin-left:10px;margin-top:5px;" /></a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('category.page',$cat_hospitality->category_slug)}}">{{str_limit(($cat_hospitality->category_name),18)}}</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('landing.page',$cat_automotive->category_slug)}}">{{str_limit(($cat_automotive->category_name),18)}}</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('productsds.page',[$cat_water->category_slug,$cat_water_sub->sub_product_slug,$cat_water_range->product_range_slug])}}">{{str_limit(($cat_water->category_name),18)}}</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('landing.page',$cat_food->category_slug)}}">{{str_limit(($cat_food->category_name),18)}}</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('landing.page',$cat_agedcare->category_slug)}}">{{str_limit(($cat_agedcare->category_name),18)}}</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('landing.page',$cat_toll->category_slug)}}">{{str_limit(($cat_toll->category_name),18)}}</a></li>
        <hr>
        </ul>

    </div>
    <hr>
    <a href="{{route('totalcover.page',['hospitality','food-and-beverages'])}}"><strong>&nbsp; &nbsp; <span style="font-size:15px; position:relative; bottom:-3px;">Challenge Total Cover</span></strong></a>
    <hr>
    <a href="{{route('cleansafe.page')}}"><strong>&nbsp; &nbsp; <span style="font-size:15px; position:relative; bottom:-3px;">Cleansafe Industrial Dispensing</span></strong></a>
    <hr>
    <a href="{{route('ourstory.page')}}"><strong>&nbsp; &nbsp; <span style="font-size:15px; position:relative; bottom:-3px;">Our Story</span></strong></a>
    <hr>
    {{--{{route('news.page')}}--}}
    <a href="#"><strong>&nbsp; &nbsp; <span style="font-size:15px; position:relative; bottom:-3px;">News</span></strong></a>
    <hr>
    <a href="{{route('contactus.page')}}"><strong>&nbsp; &nbsp; <span style="font-size:15px; position:relative; bottom:-3px;">Contact</span></strong></a>
    <hr>


</div>
