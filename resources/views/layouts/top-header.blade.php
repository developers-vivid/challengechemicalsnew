

<nav class="navbar" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">

            <a href="{{route('home')}}"><img class="img-responsive" src="{{asset('fe-content/images/logo2.png')}}" width="550px" style="padding-left:1px; padding-right:1px;"/></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="navbar-right">
            <h4><img class="nav-burger" onclick="openNav()" src="{{asset('fe-content/images/navburger.png')}}" width="50px" style="margin-top:13px; margin-right:20px; cursor:pointer" /></h4>
        </div>
        <div class="navbar-right navright1">
            <h4><img src="{{ asset('fe-content/images/phone.png') }}" /> +61 8 9419 5577</h4>
            <h4 style="margin-right:30px;"><a href="mailto:sales@challengechemicals.com.au?Subject=Hello"><img src="{{asset('fe-content/images/mail.png')}}" /> sales@challengechemicals.com.au</a></h4>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>