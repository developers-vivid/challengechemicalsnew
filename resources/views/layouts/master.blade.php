<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Challenge Chemicals</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('fe-content/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('fe-content/css/header-footer.css') }}" rel="stylesheet">
    <link href="{{ asset('fe-content/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('fe-content/css/custom-total.css') }}" rel="stylesheet">
	<link rel="shortcut icon" type="image/png" href="{{asset('fe-content/images/cc_ico.ico')}}"/>		


@yield('page_css')

    <!-- Custom CSS -->
    <style>
        <style>
        @media only screen and (min-width: 300px) and (max-width: 1200px){
            .footerBottom div{
                text-align:center;
            }
        }


        body {
            padding-top: 10px;
            /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
        }

        .search {
            padding-top:50px;
            background: linear-gradient(to right, #0c386c , #0bd0d1);
            color:#ffffff;
        }

        .searchButtonAndHolder{
            padding-bottom:70px;
        }

        .searchBox{
            width:40%;
            height:40px;
            color:#464646;
        }

        .searchButton{
            height:40px;
            padding-left:20px;
            padding-right:20px;
            color:grey;
        }

        .carousel h1{
            color:#000000;
            margin-top:-56%;
        }

        .exploreHeader{
            background-color:#006095;
            color:#ffffff;
        }

        .exploreText{
            padding-top:10px;
            padding-bottom:40px;
        }

        .footer1stColumn{
            padding-top:20px;
            padding-bottom:20px;
            font-weight:bold;
            line-height: 1.7;
        }

        .footer1stColumn a{
            color:#464646;
        }

        .footer2ndColumn img{
            margin-top:50px;
        }

        .footer3rdColumn img{
            margin-top:30px;
        }

        .footer4thColumn{
            padding-top:20px;
            padding-bottom:20px;
            font-weight:bold;
            line-height: 1.7;
        }

        .footer4thColumn img{
            width:250px;
        }

        .footer{
            background-color:#2d3e50;
            color:#d0d2d4;
        }

        .footerTop{
            padding-top:20px;
            padding-bottom:20px;
        }

        .exploreTop a{
            color:#464646;
        }

        .exploreBottom a{
            color:#464646;
        }

        .navright1{
            color:#00679a;
        }

        .zoom2 {
            transition: transform .7s; /* Animation */
        }

        .zoom2:hover {
            -ms-transform: scale(1.1); /* IE 9 */
            -webkit-transform: scale(1.1); /* Safari 3-8 */
            -moz-transform: scale(1.1);
            -o-transform: scale(1.1);
            transform: scale(1.1);
            position:relative;
            z-index:10;
        }

        /* actual dropdown animation */
        .dropdown .dropdown-menu {
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            -ms-transition: all 0.3s;
            -o-transition: all 0.3s;
            transition: all 0.3s;

            max-height: 0;
            display: block;
            overflow: hidden;
            opacity: 0;
        }

        .dropdown.open .dropdown-menu {
            max-height: 200px;
            opacity: 1;
        }

        .dropdown-menu li a{
            padding-top:10px;
            padding-bottom:10px;
            padding-left:10px;
            padding-right:10px;
            color:#ffffff;
            font-weight:bold;
            margin-left:55px;
        }

        .dropdown-menu{
            background-color:transparent;
            box-shadow:none;
            border:none;
            position:relative;
            display:block;
            width:100%;
        }

        .productDropbtn {
            background-color:transparent;
            color: #686363;
            border: none;
        }

        .prodDropdown {
            position: relative;
            display: inline-block;
        }

        .prodDropdown-content {
            display: none;
            position: absolute;
            left:65px;
            top:0px;
            background-color: #f1f1f1;
            min-width: 200px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
            padding-top:5px;
            padding-bottom:5px;
        }

        .prodDropdown-content a {
            color: black;
            padding: 3px 5px;
            text-decoration: none;
            display: block;
        }

        .prodDropdown-content a:hover {background-color: #ddd;}

        .prodDropdown:hover .prodDropdown-content {display: block;}

        .dropCaret{
            position:relative;
            top:-1px;
            left:2px;
        }

        .dropdown.open .dropdown-menu {
            max-height: 600px;
            opacity: 1;
        }

        .CCvideoWrapper {
            position: relative;
            padding-bottom: 56.25%; /* 16:9 */
            padding-top: 25px;
            height: 0;
        }
        .CCvideoWrapper iframe {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

@include('layouts.side-menu')

@include('layouts.top-header')

<!-- Page Content -->
<div class="container-fluid">

    <div class="row">
        <div class="col-lg-12 text-center search">
            <h1>Search for Product or SDS</h1>
            <br>
            {{ Form::open(['route' => 'searchProduct.page','method'=>'GET']) }}
            <div class="searchButtonAndHolder">
                {{ Form::text('search_product', old('search_product'),['class' => 'searchBox','placeholder'=>'   Enter Product Name OR SDS*']) }}
                {{ Form::submit('Search', ['class'=>'searchButton']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /.row -->
    @yield('content')
    {{--footer--}}

<div class="row footerTop">
    <div class="col-lg-3 text-left footer1stColumn">
        <p>
        <div class="prodDropdown">
            <a href="javascript:void(0)" class="productDropbtn">Products<img src="#" class="caret dropCaret" /></a><br>
            <div class="prodDropdown-content">

                <a role="menuitem" tabindex="-1" href="{{route('category.page',$cat_hospitality->category_slug)}}">{{str_limit(($cat_hospitality->category_name),18)}}</a>
                <a role="menuitem" tabindex="-1" href="{{route('landing.page',$cat_automotive->category_slug)}}">{{str_limit(($cat_automotive->category_name),18)}}</a>
                <a role="menuitem" tabindex="-1" href="{{route('productsds.page',[$cat_water->category_slug,$cat_water_sub->sub_product_slug,$cat_water_range->product_range_slug])}}">{{str_limit(($cat_water->category_name),18)}}</a>
                <a role="menuitem" tabindex="-1" href="{{route('landing.page',$cat_food->category_slug)}}">{{str_limit(($cat_food->category_name),18)}}</a>
                <a role="menuitem" tabindex="-1" href="{{route('landing.page',$cat_agedcare->category_slug)}}">{{str_limit(($cat_agedcare->category_name),18)}}</a>
                <a role="menuitem" tabindex="-1" href="{{route('landing.page',$cat_toll->category_slug)}}">{{str_limit(($cat_toll->category_name),18)}}</a>
            </div>

        </div>
        <br>
        <a href="{{route('totalcover.page',['hospitality','food-and-beverages'])}}">Challenge Total Cover</a><br>
        <a href="{{route('cleansafe.page')}}">Cleansafe Industrial Dispensing</a><br>
        <a href="{{route('ourstory.page')}}">Our Story</a><br>
        {{--{{route('news.page')}}--}}
        <a href="#">News</a><br>
        <a href="{{route('contactus.page')}}">Contact</a><br>
        </p>
    </div>
    <div class="col-lg-3 text-center footer2ndColumn">
        <img src="{{asset('fe-content/images/logo2.png')}}"  width="350px" style="margin-top:20px;" />
        <img src="{{asset('fe-content/images/iso.png')}}"  width="200px" style="margin-top:20px;" />
    </div>
    <div class="col-lg-3 text-center footer3rdColumn">
        <img src="{{asset('fe-content/images/map.png')}}"  width="150px" />
    </div>
    <div class="col-lg-3 text-left footer4thColumn">
        <p style="margin-top:10px;">
            6 Butcher Street, Kwinana Beach W.A. 6167<br>
            P: (08) 9419 5577<br>
            F: (08) 9419 4958<br>
            E: <a style="color:#464646;" href="mailto:sales@challengechemicals.com.au?Subject=Hello">sales@challengechemicals.com.au</a><br>
            <img src="{{asset('fe-content/images/logo.png')}}" style="display:none;"/>
        </p>
    </div>
</div>


<div class="row footerBottom">
    <div class="col-lg-6 text-left footer" style="margin-top:-10px !important; padding-top:5px; padding-bottom:5px;">
        <h4>Copyright © 2018 . Challenge Chemicals . All Rights Reserved</h4>
    </div>
    <div class="col-lg-6 text-right footer" style="margin-top:-10px !important; padding-top:5px; padding-bottom:5px;">
        <h4>Web Design & Development by Sushi Digital, Perth</h4>
    </div>
</div>

</div>
<!-- /.container -->

<!-- jQuery Version 1.11.1 -->

<script src="{{ asset('fe-content/js/jquery.js') }}"></script>


<script src="{{ asset('fe-content/js/custom.js') }}"></script>

<!-- Bootstrap Core JavaScript -->

<script src="{{ asset('fe-content/js/bootstrap.min.js') }}"></script>

@yield('page_js')

<script>
    $(document).ready(function() {
        $('[data-toggle="popover"]').popover({
            placement: 'top',
            trigger: 'hover'
        });
    });
</script>
</body>

</html>
