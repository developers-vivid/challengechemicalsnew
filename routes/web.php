<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
})->name('home');*/

Route::get('/','PublicPageController@index')->name('home');

Route::get('product/{category_slug}','PublicPageController@SubCatList')->name('category.page');

Route::get('products/{category_slug}','PublicPageController@landingPage')->name('landing.page');

Route::get('product/{category_slug}/{sub_category_slug}','PublicPageController@showSubCat')->name('subcat.page');

Route::get('product/{category_slug}/{sub_category_slug}/total-cover','PublicPageController@totalCover')->name('totalcover.page');


//Route::get('/food-beverages/{subcategory_id}','PublicPageController@foodAndBeverages')->name('foodbeverages.page');

Route::get('hospitality','PublicPageController@hospitalityPage')->name('hospitality.page');

/*Route::get('/foodandproductrange','PublicPageController@foodAndProductRange')->name('foodandproductrange.page');*/



Route::get('product/{category_slug}/{sub_category_slug}/product-range','PublicPageController@productRange')->name('productrange.page');

Route::get('product/{category_slug}/{sub_category_slug}/equipment','PublicPageController@showEquipment')->name('equipment.page');

Route::get('product/{category_slug}/{sub_category_slug}/services','PublicPageController@showServices')->name('showServices.page');

Route::get('product/{category_slug}/{sub_category_slug}/safety-training','PublicPageController@safetyTraining')->name('safety-training.page');

Route::get('product/{category_slug}/{sub_category_slug}/{product_range_slug}/productsds','PublicPageController@productSdsItems')->name('productsds.page');

Route::get('product/{category_slug}/{sub_category_slug}/{product_range_slug}/productsds/{product_slug}','PublicPageController@productInfo')->name('productInfo.page');

Route::get('product/{category_slug}/{sub_category_slug}/safety-training/chemical-training-safety','PublicPageController@chemicalTrainingSafety')->name('chemicaltrainingsafety.page');

Route::get('product/{category_slug}/{sub_category_slug}/safety-training/new-safety-initiatives','PublicPageController@newSafetyInitiatives')->name('newsafetyinitiatives.page');

Route::get('product/{category_slug}/{sub_category_slug}/safety-training/new-label','PublicPageController@newLabel')->name('newlabel.page');

Route::get('product/{category_slug}/{sub_category_slug}/safety-training/safety-and-operational-charts','PublicPageController@safetyOpCharts')->name('safetyOpCharts.page');

//Route::get('news','PublicPageController@newsPage')->name('news.page');

Route::get('contactus','PublicPageController@contactUsPage')->name('contactus.page');

//Route::get('aboutus','PublicPageController@aboutUsPage')->name('aboutus.page');
//
//Route::get('aboutus/ourvalues','PublicPageController@ourvaluesPage')->name('ourvalues.page');
//
//Route::get('aboutus/company-profile','PublicPageController@companyProfilePage')->name('company-profile.page');

Route::get('ourstory','PublicPageController@ourstoryPage')->name('ourstory.page');

Route::get('cleansafe-industrial-dispensing','PublicPageController@cleanSafeIndustrialPage')->name('cleansafe.page');

Route::get('services','PublicPageController@servicesPage')->name('services.page');

Route::get('service-report','PublicPageController@serviceReport')->name('servicesreport.page');





Route::get('search','PublicPageController@searchProduct')->name('searchProduct.page');


Route::get('/productinfopdf','ProductController@productInfoPdf')->name('productinfo.pdf');
//login
//Auth::routes();
/*Route::get('/home', 'HomeController@index')->name('home');*/


//Dashboard

Route::group(['prefix' => 'admin'], function() {


    Route::group(['middleware' => ['auth']], function() {

        Route::get('/','DashboardController@index');

        Route::get('/logout','LoginController@destroy')->name('logout.user');

        Route::resource('/dashboard','DashboardController');

        Route::resource('/pages','PageController');

        Route::resource('/categories','ProductCategoryController');

        Route::resource('/subcategories','ProductSubCategoryController');

        Route::resource('/productrange','ProductRangeController');

        Route::any('/productrange/index/productrangeData','ProductRangeController@productrangeData')->name('productrange.get');

        Route::resource('/products','ProductController');

        Route::any('/products/index/productsData','ProductController@productData')->name('products.get');

        Route::any('/products/index/deleteFile','ProductController@deleteImgDetails')->name('deleteFile.get');

    });

    Route::group(['middleware' => ['guest']], function() {

        Route::post('/login','Auth\LoginController@login');
        Route::get('/login','Auth\LoginController@showLoginForm')->name('login');
        Route::post('/register','Auth\RegisterController@register');
        Route::get('/register','Auth\RegisterController@showRegistrationForm')->name('register');

        //logout

    });

});



